#include "socket_if.h"
void Socket_LlenarDireccion(struct sockaddr_in* direccion, char* ip, uint32_t puerto);
void Socket_SetFlags(int descriptor);

SckServidor* Socket_CrearServidor(char* ip, uint32_t puerto){
	//SOCK_STREAM = TCP | SOCK_DGRAM = UDP
	SckServidor* server = malloc(sizeof(SckServidor));
	server->socket = malloc(sizeof(Sck));
	server->socket->direccion = malloc(sizeof(struct sockaddr_in));

	server->socket->descriptor = socket(AF_INET, SOCK_STREAM, 0);
	Socket_SetFlags(server->socket->descriptor);
	Socket_LlenarDireccion(server->socket->direccion, ip, puerto);
	server->maxConexiones = MAX_CONEXIONES;

	return server;
}

SckCliente* Socket_CrearCliente(char* ip, int32_t puerto){
	//SOCK_STREAM = TCP | SOCK_DGRAM = UDP
	//ip = NULL => Conecta con la IP local predeterminada
	//puerto = 0 => Bindea con cualquier puerto
	//puerto = -1 => NO solicitar un descriptor, ya que lo estamos creando desde accept()

	SckCliente* cliente = malloc(sizeof(SckCliente));
	cliente->socket = malloc(sizeof(Sck));
	cliente->socket->direccion = malloc(sizeof(struct sockaddr_in));
	cliente->direcServer = NULL;

	if (puerto != -1)
		cliente->socket->descriptor = socket(AF_INET, SOCK_STREAM, 0);
	Socket_SetFlags(cliente->socket->descriptor);
	Socket_LlenarDireccion(cliente->socket->direccion, ip, puerto);
	if (puerto != 0 && puerto != -1)
		bind(cliente->socket->descriptor, (struct sockaddr*) cliente->socket->direccion, sizeof(struct sockaddr_in));
	cliente->estado = DESCONECTADO;
	cliente->handShaked = false;
	cliente->usando = false;

	return cliente;
}

void Socket_CerrarServidor(SckServidor* server){
	//SHUT_RDWR = No more receptions or transmissions.
	shutdown(server->socket->descriptor, SHUT_RDWR);
	close(server->socket->descriptor);
	free(server->socket->direccion);
	free(server);
}

void Socket_CerrarCliente(SckCliente* cliente){
	//SHUT_RDWR = No more receptions or transmissions.
	shutdown(cliente->socket->descriptor, SHUT_RDWR);
	close(cliente->socket->descriptor);
	free(cliente->socket->direccion);
	if (cliente->direcServer != NULL) free(cliente->direcServer);
	free(cliente);
}

int Socket_Conectar(SckCliente* cliente, char* serverIP, uint32_t serverPuerto){
	if (cliente->estado == CONECTADO) return 1;
	uint32_t descriptorClient = cliente->socket->descriptor;

	cliente->direcServer = malloc(sizeof(struct sockaddr_in));
	Socket_LlenarDireccion(cliente->direcServer, serverIP, serverPuerto);

	int res = connect(descriptorClient, (struct sockaddr*) cliente->direcServer, sizeof(struct sockaddr_in));
	if (res != -1) cliente->estado = CONECTADO;
	return res;
}

int Socket_Enviar(SckCliente* cliente, SckPaquete* buffer){
	int res = send(cliente->socket->descriptor, buffer, sizeof(SckHeader) + buffer->header.length, 0);
	return res;
}

int Socket_EscucharConexiones(SckServidor* server){
	int res = bind(server->socket->descriptor, (struct sockaddr*) server->socket->direccion, sizeof(struct sockaddr_in));
	if (res == -1) return res;
	return listen(server->socket->descriptor, server->maxConexiones);
}

SckCliente* Socket_AceptarConexion(SckServidor* server){
	//(bloqueante) retorna el nuevo cliente conectado
	SckCliente* clienteNuevo = Socket_CrearCliente("", -1);
	int addrlen = sizeof(struct sockaddr_in);
	clienteNuevo->socket->descriptor = accept(server->socket->descriptor, (struct sockaddr*) clienteNuevo->socket->direccion, (void*) &addrlen);
	clienteNuevo->estado = CONECTADO;
	return clienteNuevo;
}

SckPaquete* Socket_Recibir(SckCliente* cliente){
	SckPaquete* buffer = malloc(sizeof(SckPaquete));

	uint32_t longitud;
	longitud = recv(cliente->socket->descriptor, buffer, sizeof(SckHeader), MSG_PEEK);
	if (longitud != sizeof(SckHeader)) goto error;
	longitud = recv(cliente->socket->descriptor, buffer, sizeof(SckHeader) + buffer->header.length, MSG_WAITALL);

	if (longitud == -1) goto error;

	return buffer;

	error:
	free(buffer);
	return NULL;
}

//Privadas de la interfaz:

void Socket_LlenarDireccion(struct sockaddr_in* direccion, char* ip, uint32_t puerto) {
	direccion->sin_family = AF_INET;
	if (ip != NULL) {
		direccion->sin_addr.s_addr = inet_addr(ip);
	} else {
		direccion->sin_addr.s_addr = INADDR_ANY;
	}
	direccion->sin_port = htons(puerto);
}

void Socket_SetFlags(int descriptor) {
	int optval = 1;
	setsockopt(descriptor, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	setsockopt(descriptor, IPPROTO_TCP, TCP_NODELAY, &optval, sizeof(optval));
}
