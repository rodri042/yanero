#include "cache_if.h"

memcached_st* Cache_CrearServidor(void);
memcached_return_t Cache_ConectarAServidor(memcached_st* memc, memcached_server_st** servers, char* ipServidor);
void Cache_CerrarConexion(memcached_st** memc, memcached_server_st** servers);

memcached_return_t Cache_ObtenerValor(char* ipServidor, const char* clave, size_t* length, void** valor) {
	memcached_st* memc = Cache_CrearServidor();
	memcached_server_st* servers;
	Cache_ConectarAServidor(memc, &servers, ipServidor);

	size_t length2;
	uint32_t flags;
	memcached_return_t ret;

	*valor = memcached_get(memc, clave, strlen(clave), &length2, &flags, &ret);
	if (length != NULL) *length = length2;

	Cache_CerrarConexion(&memc, &servers);
	return ret;
}

memcached_return_t Cache_InsertarClaveNueva(char* ipServidor, char* clave, size_t length, char* valor) {
	memcached_st* memc = Cache_CrearServidor();
	memcached_server_st* servers;
	Cache_ConectarAServidor(memc, &servers, ipServidor);

	memcached_return_t ret = memcached_set(memc, clave, strlen(clave), valor, length, (time_t) 0, (uint32_t) 0);

	Cache_CerrarConexion(&memc, &servers);
	return ret;
}

memcached_return_t Cache_EliminarDato(char* ipServidor, char* clave) {
	memcached_st* memc = Cache_CrearServidor();
	memcached_server_st* servers;
	Cache_ConectarAServidor(memc, &servers, ipServidor);

	memcached_return_t rc = memcached_delete(memc, clave, strlen(clave), 0);

	Cache_CerrarConexion(&memc, &servers);
	return rc;
}

//Privadas de la interfaz:

memcached_st* Cache_CrearServidor(void) {
	memcached_st* memc = memcached_create(NULL);
	memc->flags.binary_protocol = 1;
	return memc;
}

memcached_return_t Cache_ConectarAServidor(memcached_st* memc, memcached_server_st** servers, char* ipServidor) {
	memcached_return_t ret;
	*servers = memcached_server_list_append(NULL, ipServidor, 11212, &ret);
	ret = memcached_server_push(memc, *servers);
	return ret;
}

void Cache_CerrarConexion(memcached_st** memc, memcached_server_st** servers) {
	memcached_server_free(*servers);
	memcached_free(*memc);
}
