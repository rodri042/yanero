#ifndef SOCKET_IF_H_
#define SOCKET_IF_H_

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>

#define MAX_CONEXIONES 100
#define MAX_BUFFER 1024

//return -1: error

typedef enum {
	CONECTADO,
	DESCONECTADO
} SckEstado;

typedef struct {
	int descriptor;
	struct sockaddr_in* direccion;
} Sck;

typedef struct{
	Sck* socket;
	struct sockaddr_in* direcServer;
	SckEstado estado;
	bool handShaked;
	bool usando;
} SckCliente;

typedef struct{
	Sck* socket;
	uint maxConexiones;
} SckServidor;

typedef struct{
	char type;
	uint16_t length;
} SckHeader;

typedef struct{
	SckHeader header;
	char data[ MAX_BUFFER ];
} SckPaquete;

SckServidor* Socket_CrearServidor(char* ip, uint32_t puerto);
SckCliente*  Socket_CrearCliente(char* ip, int32_t puerto);
void Socket_CerrarServidor(SckServidor* server);
void Socket_CerrarCliente(SckCliente* cliente);

int Socket_Conectar(SckCliente* cliente, char* server_ip, uint32_t server_puerto);
int Socket_Enviar(SckCliente* cliente, SckPaquete* buffer);
int Socket_EscucharConexiones(SckServidor* server);
SckCliente* Socket_AceptarConexion(SckServidor* server);
SckPaquete* Socket_Recibir(SckCliente* cliente);

#endif /* SOCKET_IF_H_ */

/*
>>Ejemplo de uso<<

>>>> CLIENTE:
	int resultado;

	printf("Creo cliente\n");
	SckCliente* cliente = Socket_CrearCliente("127.0.0.1", 6400);
	printf("Me estoy por conectar\n");
	resultado = Socket_Conectar(cliente, "127.0.0.1", 6401);
	printf("Resultado de la conexion: %d\n", resultado);

	printf("Le voy a mandar algo... TOMAAA\n");
	SckPaquete* pkg = malloc(sizeof(SckPaquete));
	char* jojo = "Datoss!";
	pkg->header.type = 1;
	pkg->header.lenght = 8;
	memcpy(pkg->data, jojo, 8);
	resultado = Socket_Enviar(cliente, pkg);
	printf("Bytes enviados: %d", resultado);
	free(pkg);

	Socket_CerrarCliente(cliente);
>>>> SERVIDOR:
	int resultado;

	SckServidor* serv = Socket_CrearServidor("127.0.0.1", 6401); //192.168.1.141 es la mia
	printf("A ver, escucho...\n");
	resultado = Socket_EscucharConexiones(serv);
	printf("Resultado de la escucha: %d \n", resultado);

	printf("A ver si entra alguien...\n");
	SckCliente* cliente = Socket_AceptarConexion(serv);
	printf("Ahi ta\n");

	printf("Toy por recibir la data...\n");
	SckPaquete* pkg = Socket_Recibir(cliente);
	printf("Lo recibido: %s", pkg->data);
	free(pkg);

	Socket_CerrarCliente(cliente, true);
	Socket_CerrarServidor(serv, true);
*/
