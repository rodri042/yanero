#ifndef LIBMEMCACHED_H_
#define LIBMEMCACHED_H_

#include <libmemcached/memcached.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

memcached_return_t Cache_ObtenerValor(char* ipServidor, const char* clave, size_t* length, void** valor);
memcached_return_t Cache_InsertarClaveNueva(char* ipServidor, char* clave, size_t length, char* valor);
memcached_return_t Cache_EliminarDato(char* ipServidor, char* clave);

#endif /* LIBMEMCACHED_H_ */
