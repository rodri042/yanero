#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include <stdint.h>
#include <sys/types.h>

//Código operación:       Payload:
#define OP_HANDSHAKE 0
#define OP_CREATEFILE 1   //Ruta
#define OP_OPENFILE 2     //Ruta
#define OP_READFILE 3     //RutaLeerEscribir
#define OP_WRITEFILE 4    //RutaLeerEscribir
#define OP_DELETEFILE 5   //Ruta
#define OP_TRUNCATEFILE 6 //RutaTruncate
#define OP_CLOSEFILE 7    //Ruta
#define OP_CREATEDIR 8    //Ruta
#define OP_READDIR 9      //Ruta
#define OP_DELETEDIR 10   //Ruta
#define OP_GETATTR 11     //Ruta

//Respuestas:
//El payload de las respuestas (si no se aclara) es 0 o código de error (en uint32_t)
#define RSP_CREATEFILE 10
#define RSP_OPENFILE 20
#define RSP_READFILE 30 //Datos y un paquete EOF
#define RSP_WRITEFILE 40 //Un paquete vacío para que empieze a mandar, y otro al final con el código de respuesta
//|--- Paquete vacío (server->client) para que empieze a mandar
//|--- Datos y un paquete EOF (client->server)
//|--- Bytes enviados,exactamente los que pidió (server->client)
#define RSP_DELETEFILE 50
#define RSP_TRUNCATEFILE 60
#define RSP_CLOSEFILE 70 //No responde nada (no existe este código)
#define RSP_CREATEDIR 80
#define RSP_READDIR 90 //Por cada paquete: ( Lenght(1byte) + Nombre(con \0) ) x n archivos... Paquete EOF al final
#define RSP_DELETEDIR 100
#define RSP_GETATTR 110 //EntradaAtributos (si .atime=0 => inodo no encontrado)
#define RSP_EOF 99 //Indica que se terminó de mandar el archivo: viene con payload vacío

//Estructura para write y read del archivo
typedef struct {
	char path[41];
	uint32_t cant;
	off_t offset;
} RutaLeerEscribir;

//Estructura con la ruta del archivo
typedef struct {
	char path[41];
} Ruta;

//Estructura para truncar el archivo
typedef struct {
	char path[41];
	uint32_t longitud;
} RutaTruncate;

//Estructura para obtener los atributos de un archivo
typedef struct {
    uint32_t atime;
    uint32_t ctime;
    uint16_t gid;
    uint16_t mode;
    uint32_t mtime;
    uint16_t links;
    uint32_t size;
    uint16_t uid;
} EntradaAtributos;

char* NombreOP(uint8_t codigo); //devuelve los nombres de operaciones en formato string

#endif /* PROTOCOL_H_ */
