#include "protocol.h"

char* NombreOP(uint8_t codigo) {
	if (codigo == 0) {
		return "HandShake";
	} else if (codigo == 1) {
		return "CreateFile";
	} else if (codigo == 2) {
		return "OpenFile";
	} else if (codigo == 3) {
		return "ReadFile";
	} else if (codigo == 4) {
		return "WriteFile";
	} else if (codigo == 5) {
		return "DeleteFile";
	} else if (codigo == 6) {
		return "TruncateFile";
	} else if (codigo == 7) {
		return "CloseFile";
	} else if (codigo == 8) {
		return "CreateDir";
	} else if (codigo == 9) {
		return "ReadDir";
	} else if (codigo == 10) {
		return "DeleteDir";
	} else if (codigo == 11) {
		return "Getattr";
	} else {
		return "";
	}
}
