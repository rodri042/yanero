#ifndef EXT2_INT_BITMAPL_H_
#define EXT2_INT_BITMAP_H_

	#include "ext2_internal.h"

	void LiberarInodo(uint32_t inodo, SuperDatos* SD);
	void LiberarBloque(uint32_t bloque, SuperDatos* SD);
	uint32_t InodoLibre(bool reservar, SuperDatos* SD);
	uint32_t BloqueLibre(bool reservar, SuperDatos* SD);
	//void BloquesLibres(uint32_t bytesNecesarios, t_list** bloques, bool reservar, SuperDatos* SD);
	bool EstaOcupado(uint32_t numero, uint32_t grupo, bool inodo, SuperDatos* SD);
	bool ValorBit(char byte, uint8_t bit);

#endif /* EXT2_INT_BITMAP_H_ */
