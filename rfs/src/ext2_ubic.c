#include "ext2_ubic.h"

//Los números de inodo están basados en 1. Los de bloques en SD->superbloque->first_data_block (1)
//(los punteros como SD->superbloque->inode_bitmap ya lo tienen incorporado ese offset)
//Los grupos están basados en 0...

bool EsCarpeta(Inode* inodo) {
	//devuelve si un inodo es una carpeta o un archivo
	return (inodo != NULL) ? ((inodo->mode & 0170000) == 0040000) : false;
}

uint32_t BloquesNecesariosPara(uint32_t cant, SuperDatos* SD) {
	//devuelve la cantidad de bloques necesarios (directos e indirectos)
	//para poder almacenar un archivo de cant bytes
	uint32_t bloquesNecesarios = 0;
	uint32_t bloquesDatos = 0;
	uint32_t blockSize = BlockSize(SD);
	uint32_t punterosPorBloque = blockSize / sizeof(uint32_t);
	int i;
	for (i=0; i < PUNTEROS_DIRECTOS; i++) {
		bloquesNecesarios++;
		bloquesDatos++;
		if (bloquesDatos * blockSize >= cant) break;
	}

	void _contarIndirectos(uint8_t nivelIndirec) {
		for (i=0; i < punterosPorBloque; i++) {
			bloquesNecesarios++;
			if (nivelIndirec == 0) bloquesDatos++;
			if (nivelIndirec > 0) _contarIndirectos(nivelIndirec - 1);
			if (bloquesDatos * blockSize >= cant) break;
		}
	}

	uint8_t nivelIndirec = 0;
	while (bloquesDatos * blockSize < cant) {
		bloquesNecesarios++;
		_contarIndirectos(nivelIndirec);
		nivelIndirec++;
	}

	return bloquesNecesarios;
}

double MaxDireccion(int8_t nivelIndireccion, SuperDatos* SD) {
	//devuelve la cantidad máxima de bytes direccionables hasta ese nivel de indirección inclusive
	//el nivel de indirección está basado en 0
	uint32_t punterosPorBloque = BlockSize(SD) / sizeof(uint32_t);

	if (nivelIndireccion == -1) {
		return PUNTEROS_DIRECTOS * BlockSize(SD);
	} else {
		return MaxDireccion(nivelIndireccion - 1, SD) + pow(punterosPorBloque, nivelIndireccion + 1) * BlockSize(SD);
	}
}

uint32_t ByteInicialBloqueIndirec(uint8_t nivelIndireccion, uint32_t bloque, SuperDatos* SD) {
	//devuelve el byte inicial (relativo al archivo) de un bloque determinado,
	//en un nivel de indirección
	uint32_t punterosPorBloque = BlockSize(SD) / sizeof(uint32_t);
	nivelIndireccion++; //lo quiero basado en 1 para usar pow

	uint32_t punterosDirectos = PUNTEROS_DIRECTOS * BlockSize(SD);
	uint32_t indireccionesAnteriores = (nivelIndireccion==1) ? 0 : pow(punterosPorBloque, nivelIndireccion - 1) * BlockSize(SD);
	return punterosDirectos + indireccionesAnteriores + bloque * pow(punterosPorBloque, nivelIndireccion - 1) * BlockSize(SD);
}

uint32_t GrupoDeInodo(uint32_t numero, SuperDatos* SD) {
	if (numero == 0) return 0;
	return (numero - 1) / SD->superbloque->inodes_per_group;
}

uint32_t InodoRelativo(uint32_t numero, SuperDatos* SD) {
	if (numero == 0) return 0;
	return numero - GrupoDeInodo(numero, SD) * SD->superbloque->inodes_per_group - 1;
}

uint32_t InodoAbsoluto(uint32_t numeroRelativo, uint32_t grupo, SuperDatos* SD) {
	if (numeroRelativo == 0) return 0;
	return 1 + grupo * SD->superbloque->inodes_per_group + numeroRelativo;
}

uint32_t GrupoDeBloque(uint32_t bloque, SuperDatos* SD) {
	if (bloque == 0) return 0;
	return (bloque - SD->superbloque->first_data_block) / SD->superbloque->blocks_per_group;
}

uint32_t BloqueRelativo(uint32_t bloque, SuperDatos* SD) {
	if (bloque == 0) return 0;
	return bloque - GrupoDeBloque(bloque, SD) * SD->superbloque->blocks_per_group - SD->superbloque->first_data_block;
}

uint32_t BloqueAbsoluto(uint32_t bloqueRelativo, uint32_t grupo, SuperDatos* SD) {
	if (bloqueRelativo == 0) return 0;
	return SD->superbloque->first_data_block + grupo * SD->superbloque->blocks_per_group + bloqueRelativo;
}

uint32_t CantBloquesDatos(uint32_t grupo, SuperDatos* SD) {
	//devuelve la cantidad de bloques de datos de un grupo
	uint32_t posfinalGrupo = SD->superbloque->first_data_block + SD->superbloque->blocks_per_group * (grupo + 1) - 1;
	//(último byte del grupo)
	return posfinalGrupo - PosBloquesDatos(grupo, SD) + 1;
	//(sumo 1 porque al restar tengo en cuenta a los dos)
}

uint32_t PosBloquesDatos(uint32_t grupo, SuperDatos* SD) {
	//devuelve el número de bloque donde empiezan los bloques de datos de un grupo
	return SD->descriptoresGB[grupo].inode_table + InodeTableSize(SD) / BlockSize(SD);
}

uint32_t InodeTableSize(SuperDatos* SD) {
	//devuelve el tamaño de la tabla de inodos
	return SD->superbloque->inodes_per_group * sizeof(Inode);
}

uint32_t CantGrupos(SuperDatos* SD) {
	//divide inodes por inodes_per_group y redondea para arriba
	int cantidad = SD->superbloque->inodes / SD->superbloque->inodes_per_group;
	if (SD->superbloque->inodes % SD->superbloque->inodes_per_group != 0) cantidad++;
	return cantidad;
}

uint32_t Byte2Block(uint32_t bytes, SuperDatos* SD) {
	//convierte de cantidad de byte a cantidad de bloques
	uint32_t bloques = bytes / BlockSize(SD);
	if (bytes % BlockSize(SD) != 0) bloques++;
	return bloques;
}

uint32_t BlockSize(SuperDatos* SD) {
	//log_block_size es el logaritmo base 2 del tamaño de bloque, - 10.
	//BlockSize devuelve la inversa, para obtener el tamaño del bloque verdadero
	uint32_t potencia = SD->superbloque->log_block_size + 10;
	return pow(2, potencia);
}

bool NumEsPotenciaDe(double num, double potenciaDe) {
	double div = log(num) / log(potenciaDe);
	double parteDecimal = modf(div, &div);
	return parteDecimal == 0;
}
