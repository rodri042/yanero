#include "ext2_int_direntry.h"

uint32_t DirLast(Inode* carpeta, void* archivos, uint32_t pag, SuperDatos* SD);
uint32_t EntryLen(DirEntry* entry);
uint32_t EntryPadding(DirEntry* entry);
Inode* InodoDeCarpeta(Inode* carpeta, uint32_t carpetaNum, char* nombre, uint32_t* numSalida, SuperDatos* SD);

//Se preasigna espacio para las entradas de a bloques enteros (páginas). En el último DirEntry,
//siempre figura que esa entrada pesa lo que le falta para llegar al final de la página.
//Al agregar una entrada, se buscan huecos libres al final de las páginas. Al borrar, se corren
//todos los bytes de la página para atrás y se actualizan las referencias. Si una página queda vacía,
//se corren todas las páginas siguientes para atrás para "borrarla", y se trunca el archivo.
//- Los entry_len tienen deben ser múltiplos de 4. Al padding de . y .. se le suma 4.
//- Un DirEntry no puede estar entre medio de dos páginas distintas, si no entra se deja espacio.

void DirAdd(Inode* carpeta, uint32_t carpetaNum, char* nombre, uint32_t inodoNum, SuperDatos* SD) {
	//agrega un DirEntry a un directorio
	if (carpetaNum != 0) pthread_rwlock_wrlock(&SD->locks.inodos[carpetaNum]);
	void* archivos = LeerInodo(carpeta, 0, SD); //le paso 0 porque si LeerInodo hace otro lock al inodo entramos en deadlock!

	DirEntry entry; //creo la nueva entrada
	strcpy(entry.name, nombre);
	entry.inode = inodoNum;
	entry.type = 0;
	entry.name_len = strlen(nombre);
	uint32_t entryLenReal = EntryLen(&entry); //para el momento de escribir
	uint32_t entryPadding = EntryPadding(&entry);
	if (strcmp(nombre, ".") == 0 || strcmp(nombre, "..") == 0) entryPadding += 4;
	uint32_t entryLen = entryLenReal + entryPadding;

	if (archivos == NULL) { //si es el primer DirEntry del directorio...
		TruncarInodo(carpeta, 0, BlockSize(SD), SD);
		entry.entry_len = BlockSize(SD);
		char buffer[entryLenReal];
		memcpy(buffer, &entry, entryLenReal);
		Ubic ubic; ubic.offset = 0; ubic.cant = entryLenReal;
		EscribirInodo(carpeta, 0, buffer, &ubic, SD);
		return;
	}

	uint32_t entryOffset; //donde se escribirá el dato
	uint32_t ultimoEntryLenReal;
	uint32_t ultimoEntryPadding;
	uint32_t ultimoEntryLen;
	uint32_t ultimoEntryOffset;
	DirEntry* ultimoEntry;

	uint32_t pagOffset = 0;
	uint32_t pag = 0;
	int32_t pagDefinitiva = -1;
	//...para cada página:
	for (pagOffset = 0; pagOffset < carpeta->size; pagOffset += BlockSize(SD)) {
		//calculo el tamaño real de la última entrada hasta ahora, y el espacio que queda
		ultimoEntryOffset = DirLast(carpeta, archivos, pag, SD);
		ultimoEntry = archivos + ultimoEntryOffset;
		ultimoEntryLenReal = EntryLen(ultimoEntry);;
		ultimoEntryPadding = EntryPadding(ultimoEntry);
		ultimoEntryLen = ultimoEntryLenReal + ultimoEntryPadding;
		uint32_t espacioLibre = ultimoEntry->entry_len - ultimoEntryLen;

		if (espacioLibre > entryLen) { //si entra
			log_trace(SD->logger, "Se usa la pág %d en la carpeta %d para el i%d", pag, carpetaNum, inodoNum);
			ultimoEntry->entry_len = ultimoEntryLen; //el último entry (hasta ahora) pesa lo que tiene que pesar...
			//y el nuevo último:
			entryOffset = ultimoEntryOffset + ultimoEntryLen; //empieza donde termina el otro
			entry.entry_len = espacioLibre; //y pesa: el espacio que queda hasta el final del bloque
			pagDefinitiva = pag;
			break;
		}
		pag++;
	}

	bool nuevaPag = false;
	if (pagDefinitiva == -1) { //si no encontramos lugar en ninguna página
		log_trace(SD->logger, "Se crea la pág %d en la carpeta %d para el i%d", pag, carpetaNum, inodoNum);
		nuevaPag = true;
		ultimoEntryLen = 0;
		entryOffset = carpeta->size;
		entry.entry_len = BlockSize(SD);
		pagDefinitiva = pag; //en pag ya me quedó el número de la nueva página
		TruncarInodo(carpeta, 0, carpeta->size + BlockSize(SD), SD);
	}

	char* buffer; //guardo los cambios en un buffer y escribo
	Ubic ubic;
	buffer = malloc(ultimoEntryLen + entryLenReal);
	if (!nuevaPag) { //si no es la primer entrada del bloque
		memcpy(buffer, ultimoEntry, ultimoEntryLenReal);
		ubic.offset = ultimoEntryOffset;
		int i;
		for(i=ultimoEntryLenReal; i < ultimoEntryLen; i++) buffer[i] = '\0'; //limpio el padding
	} else ubic.offset = entryOffset;
	memcpy(buffer + ultimoEntryLen, &entry, entryLenReal);
	ubic.cant = ultimoEntryLen + entryLenReal;

	EscribirInodo(carpeta, 0, buffer, &ubic, SD);
	free(buffer);
	free(archivos);
	if (carpetaNum != 0) pthread_rwlock_unlock(&SD->locks.inodos[carpetaNum]);
}

void DirDelete(Inode* carpeta, uint32_t carpetaNum, uint32_t inodoABorrar, SuperDatos* SD) {
	//borra un DirEntry de un directorio
	if (carpetaNum != 0) pthread_rwlock_wrlock(&SD->locks.inodos[carpetaNum]);
	void* archivos = LeerInodo(carpeta, 0, SD);
	if (archivos == NULL) return;

	DirEntry* dir;
	uint32_t offset = 0;
	while (offset < carpeta->size) {
		dir = archivos + offset;

		if (inodoABorrar == dir->inode) { //encontré el registro a borrar
			uint32_t pag = offset / BlockSize(SD);
			bool esLaUltimaPag = offset + BlockSize(SD) == carpeta->size;
			uint32_t bytesABorrar = dir->entry_len;

			uint32_t ultimoEntryOffset = DirLast(carpeta, archivos, pag, SD);
			DirEntry* ultimoEntry = archivos + ultimoEntryOffset;
			bool borrePrimero = offset == pag * BlockSize(SD);
			bool borreUltimo = offset == ultimoEntryOffset;
			bool hayQueGuardar = true;
			Ubic ubic;

			if (borreUltimo) {
				if (borrePrimero) {
					//si borré el primero y el último (o sea, el único) hay que borrar la página
					log_trace(SD->logger, "Se borra la pág %d en la carpeta %d por el i%d", pag, carpetaNum, inodoABorrar);
					if (esLaUltimaPag) hayQueGuardar = false;
					uint32_t copiarDesde = offset + BlockSize(SD);
					memcpy(archivos + offset, archivos + copiarDesde, carpeta->size - copiarDesde);
					ubic.offset = offset;
					ubic.cant = carpeta->size - copiarDesde;
					TruncarInodo(carpeta, 0, carpeta->size - BlockSize(SD), SD);
				} else {
					//actualizo el entry_len de la entrada anterior
					log_trace(SD->logger, "Se borra de la pág %d en la carpeta %d el i%d (último)", pag, carpetaNum, inodoABorrar);
					uint32_t sizeOriginal = carpeta->size;
					carpeta->size = offset;
					uint32_t entryAnteriorOffset = DirLast(carpeta, archivos, pag, SD);
					carpeta->size = sizeOriginal;

					DirEntry* entryAnterior = archivos + entryAnteriorOffset;
					entryAnterior->entry_len = (pag+1) * BlockSize(SD) - entryAnteriorOffset;

					ubic.offset = entryAnteriorOffset; //guardo los cambios desde el offset de mi entrada anterior
					ubic.cant = EntryLen(entryAnterior);
				}
			} else {
				//corro el resto de la página para atrás:
				log_trace(SD->logger, "Se borra de la pág %d en la carpeta %d el i%d", pag, carpetaNum, inodoABorrar);
				uint32_t copiarDesde = offset + dir->entry_len;
				memcpy(archivos + offset, archivos + copiarDesde, (pag+1) * BlockSize(SD) - copiarDesde);

				//actualizo la última entrada al tamaño nuevo:
				ultimoEntryOffset -= bytesABorrar;
				ultimoEntry = archivos + ultimoEntryOffset;
				//ahora va a estar más lejos del final, asi que lo incremento para compensar...
				ultimoEntry->entry_len += bytesABorrar;

				ubic.offset = offset; //guardo los cambios desde mi offset
				ubic.cant = (pag+1) * BlockSize(SD) - copiarDesde; //los bytes que me faltan hasta el final de la página
			}

			//escribo los cambios al directorio:
			if (hayQueGuardar) EscribirInodo(carpeta, 0, archivos + ubic.offset, &ubic, SD);
			break;
		}
		offset += dir->entry_len;
	}
	free(archivos);
	if (carpetaNum != 0) pthread_rwlock_unlock(&SD->locks.inodos[carpetaNum]);
}

uint32_t DirCount(Inode* carpeta, uint32_t carpetaNum, SuperDatos* SD) {
	//devuelve la cantidad de entradas de una carpeta
	uint32_t cant = 0;
	void _contar(char* nombre, uint32_t inodo, bool* parar) {
		cant++;
	}
	DirRecorrer(carpeta, carpetaNum, _contar, SD);
	return cant;
}
uint32_t DirLast(Inode* carpeta, void* archivos, uint32_t pag, SuperDatos* SD) {
	//lee el directorio y devuelve el offset del último DirEntry de una página
	DirEntry* dir;
	uint32_t offset = pag * BlockSize(SD);
	uint32_t elOffset = 0;
	while (offset < (pag+1) * BlockSize(SD) && offset < carpeta->size) {
		dir = archivos + offset;
		elOffset = offset;
		offset += dir->entry_len;
	}
	return elOffset;
}

uint32_t EntryLen(DirEntry* entry) {
	return EXT2_DIRENTRY_SIZE + entry->name_len;
}

uint32_t EntryPadding(DirEntry* entry) {
	uint32_t entryLenReal = EntryLen(entry);
	uint32_t entryPadding = 0;
	while((entryLenReal + entryPadding) % 4 != 0) entryPadding++;
	return entryPadding;
}

void DirRecorrer(Inode* carpeta, uint32_t carpetaNum, void(*func)(char*, uint32_t, bool*), SuperDatos* SD) {
	//aplica la función func a cada DirEntry de la carpeta
	void* archivos = LeerInodo(carpeta, carpetaNum, SD);
	if (archivos == NULL) return;

	DirEntry* dir;
	uint32_t offset = 0;
	bool parar = false;
	while (offset < carpeta->size ) {
		dir = archivos + offset;

		char* name = malloc(dir->name_len + 1);
		memcpy(name, dir->name, dir->name_len);
		name[dir->name_len] = '\0';
		parar = false;
		func(name, dir->inode, &parar); //aplica la función
		free(name);
		if (parar) break;
		offset += dir->entry_len;
	}
	free(archivos);
}

char* ListadoDePath(char* path, uint32_t* length, SuperDatos* SD) {
	//devuelve una cadena (serializada) que se enviará por sockets al cliente
	//para listar un directorio, y el length total de la cadena
	uint32_t carpetaNum;
	Inode* carpeta = InodoDePath(path, &carpetaNum, SD);
	char* info = malloc(1);
	uint32_t offsetInfo = 0;
	uint32_t sizeInfo = 0;
	void _serializar(char* nombre, uint32_t inodo, bool* parar) {
		uint8_t nameSize = strlen(nombre) + 1;
		sizeInfo += 1 + nameSize;
		info = realloc(info, sizeInfo);
		info[offsetInfo] = nameSize;
		memcpy(info + offsetInfo + 1, nombre, nameSize);
		offsetInfo += 1 + nameSize;
	}
	DirRecorrer(carpeta, carpetaNum, _serializar, SD);
	*length = sizeInfo;

	return info;
}

Inode* InodoDePath(char* path, uint32_t* numSalida, SuperDatos* SD) {
	//de un path parsea el string con / y obtiene el inodo final
	//y su número, si numSalida != NULL
	Inode* inodo = Inodo(2, SD);
	if (strcmp(path, "/") == 0) {
		if (numSalida != NULL) *numSalida = 2;
		return inodo;
	}

	char* ruta = malloc(strlen(path) + 1);
	strcpy(ruta, path);
	char *tmp;
	char *tokens = NULL;
	tokens = strtok_r(ruta, "/", &tmp);
	uint32_t num = 0;
    inodo = InodoDeCarpeta(inodo, 2, tokens, &num, SD);

    while ( (tokens = strtok_r(NULL, "/", &tmp)) != NULL) {
        if (inodo == NULL) break; //ya hay un inodo que no lo encontró => sale
        inodo = InodoDeCarpeta(inodo, num, tokens, &num, SD);
    }
    if (numSalida != NULL) *numSalida = num;

	free(ruta);
	return inodo;
}

Inode* InodoDeCarpeta(Inode* carpeta, uint32_t carpetaNum, char* nombre, uint32_t* numSalida, SuperDatos* SD) {
	//devuelve el inodo de un archivo, dentro de un inodo carpeta determinado
	//y el número, en el parámetro numSalida
	Inode* inodo = NULL;
	if (!EsCarpeta(carpeta)) return NULL; //si no es una carpeta se va

	void _encontrarInodo(char* name, uint32_t inodoNum, bool* parar) {
		if (strcmp(nombre, name) == 0) {
			inodo = Inodo(inodoNum, SD);
			*numSalida = inodoNum;
			*parar = true;
		}
	}
	DirRecorrer(carpeta, carpetaNum, _encontrarInodo, SD);

	return inodo;
}

void DebugDir(uint32_t inodoNum, SuperDatos* SD) {
	Inode* carpeta = Inodo(inodoNum, SD);
	void* archivos = LeerInodo(carpeta, inodoNum, SD);
	if (archivos == NULL) return;

	DirEntry* dir;
	uint32_t offset = 0;
	bool parar = false;
	while (offset < carpeta->size ) {
		dir = archivos + offset;

		char* name = malloc(dir->name_len + 1);
		memcpy(name, dir->name, dir->name_len);
		name[dir->name_len] = '\0';
		log_trace(SD->logger, "RecorrerInodo: %d a %d: %s (ino: %d; type: %d; nameSize: %d; entrySize: %d; padd: %d)", offset, offset+dir->entry_len-1, name, dir->inode, dir->type, dir->name_len, dir->entry_len, dir->entry_len - 8 - dir->name_len);
		parar = false;
		free(name);
		if (parar) break;
		offset += dir->entry_len;
	}
	free(archivos);
}

//Separo el path a la carpeta, del nombre del archivo:
void SepararCarpeta_Nombre(char* path, char** nombre, char** pathCarpeta) {
	//ES UN ASCO ESTE CÓDIGO PERO ASDLKAJSHGLKASSAFWASKFJWF D=
	if (path[0] != '/') return;
	uint32_t _ocurrCaracter(char* str, uint32_t strLen, char c) {
		uint32_t cant = 0; int i; for (i=0; i < strLen; i++) { if (str[i] == c) cant++; } return cant;
	}
	char** splitted = string_split(path, "/");
	uint32_t splittedLast = _ocurrCaracter(path, strlen(path), '/') - 1;
	uint32_t sizePathCarpeta;
	if (path[strlen(path) - 1] != '/') {
		*nombre = splitted[splittedLast];
		sizePathCarpeta = strlen(path) - strlen(*nombre) - 1;
	} else {
		splittedLast--;
		*nombre = malloc(1);
		*nombre = '\0';
		sizePathCarpeta = strlen(path) - 1;
	}
	*pathCarpeta = malloc(strlen(path) + 1);
	memcpy(*pathCarpeta, path, sizePathCarpeta);
	*(*pathCarpeta + sizePathCarpeta) = '\0';
	if (sizePathCarpeta == 0) strcpy(*pathCarpeta, "/");
	int i; for (i=0; i < splittedLast; i++) free(splitted[i]);
	free(splitted);
}
