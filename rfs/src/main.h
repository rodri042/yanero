#ifndef EXT2_DEF_H_
#define EXT2_DEF_H_

	#include "commons/collections/list.h"
	#include "commons/log.h"
	#include "commons/config.h"
	#include "commons/bitarray.h"
	#include "commons/string.h"
	#include "../tads/socket_if.h"
	#include "../tads/cache_if.h"
	#include "../tads/protocol.h"
	#include <sys/socket.h>
	#include <sys/types.h>
	#include <sys/inotify.h>
	#include <sys/mman.h>
	#include <unistd.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <stdint.h>
	#include <stdbool.h>
	#include <string.h>
	#include <pthread.h>
	#include <time.h>
	#include <semaphore.h>

	#define MAX_KEY 43
	#define PUNTEROS_DIRECTOS 12
	#define NIVELES_INDIRECCION 3
	#define PATH_CONFIG "config.txt"
	#define PATH_LOG "log.txt"

	#define INOTIFY_EVENT_SIZE ( sizeof (struct inotify_event) + 50 )
	#define INOTIFY_BUF_LEN ( 5 * INOTIFY_EVENT_SIZE )

	#define EXT2_SUPER_MAGIC 0xEF53

	#define EXT2_ROOT_INODE_INDEX 1

	#define EXT2_DIRENTRY_SIZE 8

	// t_ext2_superblock -> state
	#define EXT2_VALID_FS 1 //Unmounted cleanly
	#define EXT2_ERROR_FS 2 //Errors detected

	// t_ext2_superblock -> errors
	#define EXT2_ERRORS_CONTINUE 1 //continue as if nothing happened
	#define EXT2_ERRORS_RO 2 //remount read-only
	#define EXT2_ERRORS_PANIC 3 //cause a kernel panic

	// t_ext2_superblock -> creator_os
	#define EXT2_OS_LINUX 0 //Linux
	#define EXT2_OS_HURD 1 //GNU HURD
	#define EXT2_OS_MASIX 2 //MASIX
	#define EXT2_OS_FREEBSD 3 //FreeBSD
	#define EXT2_OS_LITES 4 //Lites

	// t_ext2_superblock -> rev_level
	#define EXT2_GOOD_OLD_REV 0 //Revision 0
	#define EXT2_DYNAMIC_REV 1 //Revision 1 with variable inode sizes, extended attributes, etc.

	// t_ext2_inode -> rev_level
	#define EXT2_INODE_HAS_MODE_FLAG(inode, flag) ((inode->mode & 0xF000) == flag)

	//const int8_t EXT2_INODES_INDIRECTION_LEVEL[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3 };

	// -- file format --
	#define EXT2_IFSOCK 0xC000 //socket
	#define EXT2_IFLNK 0xA000 //symbolic link
	#define EXT2_IFREG 0x8000 //regular file
	#define EXT2_IFBLK 0x6000 //block device
	#define EXT2_IFDIR 0x4000 //directory
	#define EXT2_IFCHR 0x2000 //character device
	#define EXT2_IFIFO 0x1000 //fifo
	// -- process execution user/group override --
	#define EXT2_ISUID 0x0800 //Set process User ID
	#define EXT2_ISGID 0x0400 //Set process Group ID
	#define EXT2_ISVTX 0x0200 //sticky bit
	// -- access rights --
	#define EXT2_IRUSR 0x0100 //user read
	#define EXT2_IWUSR 0x0080 //user write
	#define EXT2_IXUSR 0x0040 //user execute
	#define EXT2_IRGRP 0x0020 //group read
	#define EXT2_IWGRP 0x0010 //group write
	#define EXT2_IXGRP 0x0008 //group execute
	#define EXT2_IROTH 0x0004 //others read
	#define EXT2_IWOTH 0x0002 //others write
	#define EXT2_IXOTH 0x0001 //others execute

	//Estructuras:
	typedef struct {
	    uint32_t inodes;
	    uint32_t blocks;
	    uint32_t reserved_blocks;
	    uint32_t free_blocks;
	    uint32_t free_inodes;
	    uint32_t first_data_block;
	    uint32_t log_block_size;
	    uint32_t fragment_size;
	    uint32_t blocks_per_group;
	    uint32_t fragments_per_group;
	    uint32_t inodes_per_group;
	    uint32_t mount_time;
	    uint32_t write_time;
	    uint16_t mount_count;
	    uint16_t max_mount;
	    uint16_t magic;
	    uint16_t state;
	    uint16_t errors;
	    uint16_t minor;
	    uint32_t last_check;
	    uint32_t check_int;
	    uint32_t creator_os;
	    uint32_t rev_level;
	    uint16_t resv_uid;
	    uint16_t resv_gid;
	} Superblock;

	typedef struct {
	    uint32_t block_bitmap;
	    uint32_t inode_bitmap;
	    uint32_t inode_table;
	    uint16_t free_blocks_count;
	    uint16_t free_inodes_count;
	    uint16_t used_dirs_count;
	    uint16_t dummy[7];
	} BlockgroupDesc;

	typedef struct {
	    uint16_t mode;
	    uint16_t uid;
	    uint32_t size;
	    uint32_t atime;
	    uint32_t ctime;
	    uint32_t mtime;
	    uint32_t dtime;
	    uint16_t gid;
	    uint16_t links;
	    uint32_t nr_blocks;
	    uint32_t flags;
	    uint32_t reserved1;
	    uint32_t blocks[PUNTEROS_DIRECTOS];
	    uint32_t iblock[3];
	    uint32_t generation;
	    uint32_t file_acl;
	    uint32_t size_hi;
	    uint32_t reserved2[4];
	} Inode;

	typedef struct {
	    uint32_t inode;
	    uint16_t entry_len;
	    uint8_t name_len;
	    uint8_t type;
	    char name[MAX_KEY];
	} DirEntry;

	typedef struct {
		SckCliente* cliente;
		SckPaquete paquete;
	} Operacion;

	typedef struct {
		pthread_rwlock_t superbloque;
		pthread_rwlock_t* descriptoresGB;
		pthread_rwlock_t bitmapBloques;
		pthread_rwlock_t bitmapInodos;
		pthread_rwlock_t* inodos;
		sem_t freeInodes;
		sem_t freeBlocks;
		sem_t usedDirsCount;
	} Locks;

	typedef struct {
		FILE* partFile;
		void* partMap;
		uint32_t partSize;
		t_config* config;
		Superblock* superbloque;
		BlockgroupDesc* descriptoresGB;
		t_log* logger;
		char ipCache[16];
		bool cacheConectada;
		sem_t semCache;
		SckServidor* server;
		t_list* clientes;
		pthread_t* threads;
		sem_t* threadsSem;
		Operacion* threadsOP;
		sem_t threadsLibresSem;
		bool* threadsLibres;
		Locks locks;
		t_list* selExcluidos;
		sem_t selExcluidosSem;
		int32_t wakeEvent[2];
		sem_t wakeEventSem;
		uint32_t inotifyFD[2];
		uint32_t retardo;
	} SuperDatos;

	typedef struct {
		uint32_t cant;
		off_t offset;
	} Ubic;

	typedef struct {
		uint32_t primero;
		uint32_t ultimo;
	} Fragmento;

#endif /* EXT2_DEF_H_ */
