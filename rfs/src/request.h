#ifndef REQUEST_H_
#define REQUEST_H_

	#include "main.h"
	#include "ext2_operations.h"

	void DIOS(void* sd);
	void ActualizarRetardo(void* sd);
	bool ComprobarConfig(t_config* config);
	uint32_t PrepararSelect (fd_set* descRead, SuperDatos* SD);
	void DespertarSelect(SuperDatos* SD);
	void ExcluirSelect(SckCliente* cliente, SuperDatos* SD);
	void DesexcluirSelect(SckCliente* cliente, SuperDatos* SD);
	void PonerCliente(SuperDatos* SD);
	void SacarCliente (SckCliente* cliente, SuperDatos* SD);
	uint32_t ThreadLibre(SuperDatos* SD);
	void CrearThread(pthread_t* tid, void(*func)(void*), SuperDatos* SD);

#endif /* REQUEST_H_ */
