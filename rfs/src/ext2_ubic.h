#ifndef EXT2_FUNC_H_
#define EXT2_FUNC_H_

	#include "main.h"
	#include <math.h>

	bool EsCarpeta(Inode* inodo);
	uint32_t BloquesNecesariosPara(uint32_t cant, SuperDatos* SD);
	double MaxDireccion(int8_t nivelIndireccion, SuperDatos* SD);
	uint32_t ByteInicialBloqueIndirec(uint8_t nivelIndireccion, uint32_t bloque, SuperDatos* SD);
	uint32_t GrupoDeInodo(uint32_t numero, SuperDatos* SD);
	uint32_t InodoRelativo(uint32_t numero, SuperDatos* SD);
	uint32_t InodoAbsoluto(uint32_t numeroRelativo, uint32_t grupo, SuperDatos* SD);
	uint32_t GrupoDeBloque(uint32_t bloque, SuperDatos* SD);
	uint32_t BloqueRelativo(uint32_t bloque, SuperDatos* SD);
	uint32_t BloqueAbsoluto(uint32_t bloqueRelativo, uint32_t grupo, SuperDatos* SD);
	uint32_t CantBloquesDatos(uint32_t grupo, SuperDatos* SD);
	uint32_t PosBloquesDatos(uint32_t grupo, SuperDatos* SD);
	uint32_t InodeTableSize(SuperDatos* SD);
	uint32_t CantGrupos(SuperDatos* SD);
	uint32_t Byte2Block(uint32_t bytes, SuperDatos* SD);
	uint32_t BlockSize(SuperDatos* SD);
	bool NumEsPotenciaDe(double num, double potenciaDe); //no usado

#endif /* EXT2_FUNC_H_ */
