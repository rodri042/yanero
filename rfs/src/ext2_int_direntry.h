#ifndef EXT2_INT_DIRENTRY_H_
#define EXT2_INT_DIRENTRY_H_

	#include "ext2_internal.h"

	void DirAdd(Inode* carpeta, uint32_t carpetaNum, char* nombre, uint32_t inodoNum, SuperDatos* SD);
	void DirDelete(Inode* carpeta, uint32_t carpetaNum, uint32_t inodoABorrar, SuperDatos* SD);
	uint32_t DirCount(Inode* carpeta, uint32_t carpetaNum, SuperDatos* SD);
	void DirRecorrer(Inode* carpeta, uint32_t carpetaNum, void(*func)(char*, uint32_t, bool*), SuperDatos* SD);
	char* ListadoDePath(char* path, uint32_t* length, SuperDatos* SD);
	Inode* InodoDePath(char* path, uint32_t* numSalida, SuperDatos* SD);
	void DebugDir(uint32_t inodoNum, SuperDatos* SD);
	void SepararCarpeta_Nombre(char* path, char** nombre, char** pathCarpeta);

#endif /* EXT2_INT_DIRENTRY_H_ */
