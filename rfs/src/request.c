#include "request.h"

void DIOS(void* sd) { //(Asignada a cada thread) Atiende operaciones
	SuperDatos* SD = (SuperDatos*) sd;
	uint32_t tid = pthread_self();
	uint16_t indice = 0;
	while (SD->threads[indice] != tid) indice++;
	SckCliente* cliente;
	SckPaquete pkg;

	while(1) {
		SD->threadsLibres[indice] = true;
		sem_wait(&SD->threadsSem[indice]);

		cliente = SD->threadsOP[indice].cliente;
		pkg = SD->threadsOP[indice].paquete;

		if (pkg.header.type == OP_HANDSHAKE && strcmp(pkg.data, "Hola?") == 0) {
			//>>>Operación HandShake<<<
			cliente->handShaked = true;
			log_trace(SD->logger, "Cliente %d confirmado", cliente->socket->descriptor);
			SckPaquete* pkgHandShake = malloc(sizeof(SckPaquete));
			pkgHandShake->header.type = OP_HANDSHAKE;
			pkgHandShake->header.length = 6;
			strcpy(pkgHandShake->data, "Hola!");
			Socket_Enviar(cliente, pkgHandShake);
			free(pkgHandShake);
		} else if(cliente->handShaked) {
			if (pkg.header.type == OP_CREATEFILE) {
				//>>>Operación CreateFile<<<
				Ruta* ruta = (Ruta*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, cliente->socket->descriptor, indice);
				CreateFile(cliente, ruta->path, SD);
			} else if (pkg.header.type == OP_OPENFILE) {
				//>>>Operación OpenFile<<<
				Ruta* ruta = (Ruta*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, cliente->socket->descriptor, indice);
				OpenFile(cliente, ruta->path, SD);
			} else if (pkg.header.type == OP_READFILE) {
				//>>>Operación ReadFile<<<
				RutaLeerEscribir* ruta = (RutaLeerEscribir*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s; Off: %d; Siz: %d - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, (int) ruta->offset, ruta->cant, cliente->socket->descriptor, indice);
				ReadFile(cliente, ruta->path, ruta->offset, ruta->cant, SD);
			} else if (pkg.header.type == OP_WRITEFILE) {
				//>>>Operación WriteFile<<<
				RutaLeerEscribir* ruta = (RutaLeerEscribir*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s; Off: %d; Siz: %d - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, (int) ruta->offset, ruta->cant, cliente->socket->descriptor, indice);
				WriteFile(cliente, ruta->path, ruta->offset, ruta->cant, SD);
			} else if (pkg.header.type == OP_DELETEFILE) {
				//>>>Operación DeleteFile<<<
				Ruta* ruta = (Ruta*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, cliente->socket->descriptor, indice);
				DeleteFile(cliente, ruta->path, SD);
			} else if (pkg.header.type == OP_TRUNCATEFILE) {
				//>>>Operación TruncateFile<<<
				RutaTruncate* ruta = (RutaTruncate*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s; Siz: %d - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, ruta->longitud, cliente->socket->descriptor, indice);
				TruncateFile(cliente, ruta->path, ruta->longitud, SD);
			} else if (pkg.header.type == OP_CLOSEFILE) {
				//>>>Operación CloseFile<<<
				Ruta* ruta = (Ruta*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, cliente->socket->descriptor, indice);
				CloseFile(cliente, ruta->path, SD);
			} else if (pkg.header.type == OP_CREATEDIR) {
				//>>>Operación CreateDir<<<
				Ruta* ruta = (Ruta*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, cliente->socket->descriptor, indice);
				CreateDir(cliente, ruta->path, SD);
			} else if (pkg.header.type == OP_READDIR) {
				//>>>Operación ReadDir<<<
				Ruta* ruta = (Ruta*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, cliente->socket->descriptor, indice);
				ReadDir(cliente, ruta->path, SD);
			} else if (pkg.header.type == OP_DELETEDIR) {
				//>>>Operación DeleteDir<<<
				Ruta* ruta = (Ruta*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, cliente->socket->descriptor, indice);
				DeleteDir(cliente, ruta->path, SD);
			} else if (pkg.header.type == OP_GETATTR) {
				//>>>Operación Getattr<<<
				Ruta* ruta = (Ruta*) pkg.data;
				if (config_get_int_value(SD->config, "LOG_OPERACIONES") == 1)
					log_debug(SD->logger, "[%s] Pth: %s - Sck: %d; Thr: %d", NombreOP(pkg.header.type), ruta->path, cliente->socket->descriptor, indice);
				Getattr(cliente, ruta->path, SD);
			} else {
				log_warning(SD->logger, "Operación desconocida: %d - Sck: %d; Thr: %d", pkg.header.type, cliente->socket->descriptor, indice);
			}
		}

		cliente->usando = false;
		sem_post(&SD->threadsLibresSem);
	}
}

void ActualizarRetardo(void* sd) {
	SuperDatos* SD = (SuperDatos*) sd;

	char buffer[INOTIFY_BUF_LEN];

	while(1) {
		ssize_t res;
		res = read(SD->inotifyFD[0], buffer, INOTIFY_BUF_LEN); //si cambia la config

		if (res >= 0) {
			t_config* tmpConfig = config_create(PATH_CONFIG);
			if (ComprobarConfig(tmpConfig)) {
				uint32_t nuevoRetardo = config_get_int_value(tmpConfig, "RETARDO");
				if (SD->retardo != nuevoRetardo)
					log_info(SD->logger, "Se cambió el retardo a %d", nuevoRetardo);
				SD->retardo = nuevoRetardo;
			}

			config_destroy(tmpConfig);
			inotify_rm_watch(SD->inotifyFD[0], SD->inotifyFD[1]);
			close(SD->inotifyFD[0]);
			SD->inotifyFD[0] = inotify_init();
			SD->inotifyFD[1] = inotify_add_watch(SD->inotifyFD[0], PATH_CONFIG, IN_MODIFY);
		}
	}
}

bool ComprobarConfig(t_config* config) {
	char* parametros[] = {"LOG_CLIENTENUEVO", //0 o 1
						  "LOG_CAIDACLIENTE", //0 o 1
						  "LOG_OPERACIONES", //0 o 1
						  "LOG_MIN_LEVEL", //0 a 4
						  "PUERTO",
						  "CONEX_MAX",
						  "THREADS_MAX",
						  "RETARDO",
						  "IP_CACHE"
						 };
	int i;
	for(i=0; i < 8; i++) {
		if (!config_has_property(config, parametros[i])) {
			return false;
		}
	}
	return true;
}

uint32_t PrepararSelect(fd_set* descRead, SuperDatos* SD) {
	//prepara los fd_set con todos los clientes conectados
	//y devuelve el descriptor máximo
	FD_ZERO(descRead);
	uint32_t descMax = SD->server->socket->descriptor; //agregar listener
	FD_SET(SD->server->socket->descriptor, descRead);
	sem_wait(&SD->wakeEventSem);
	close(SD->wakeEvent[0]); //agregar evento despertador
	close(SD->wakeEvent[1]);
	socketpair(AF_LOCAL, SOCK_STREAM, 0, SD->wakeEvent);
	sem_post(&SD->wakeEventSem);
	FD_SET(SD->wakeEvent[1], descRead);
	if (SD->wakeEvent[1] > descMax) descMax = SD->wakeEvent[1];

	void _agregador(void* cli) { //agregar clientes
		SckCliente* cliente = (SckCliente*) cli;
		bool _esElCliente (void* cli2) {
			SckCliente* cliente2 = (SckCliente*) cli2;
			return (cliente->socket == cliente2->socket);
		}
		sem_wait(&SD->selExcluidosSem);
		if (list_find(SD->selExcluidos, _esElCliente) == NULL) { //si no está excluido lo agrega
			FD_SET(cliente->socket->descriptor, descRead);
			if (cliente->socket->descriptor > descMax)
				descMax = cliente->socket->descriptor;
		}
		sem_post(&SD->selExcluidosSem);
	}
	list_iterate(SD->clientes, _agregador);

	return descMax;
}

void DespertarSelect(SuperDatos* SD) {
	//desbloquea el select escribiendo fruta en el descriptor de eventos
	uint64_t dummy = 64;
	sem_wait(&SD->wakeEventSem);
	send(SD->wakeEvent[0], &dummy, sizeof(uint64_t), 0);
	sem_post(&SD->wakeEventSem);
}

void ExcluirSelect(SckCliente* cliente, SuperDatos* SD) {
	//agrega al cliente a la lista de excluidos
	sem_wait(&SD->selExcluidosSem);
	list_add(SD->selExcluidos, cliente);
	sem_post(&SD->selExcluidosSem);
}

void DesexcluirSelect(SckCliente* cliente, SuperDatos* SD) {
	//saca al cliente de la lista de excluidos
	sem_wait(&SD->selExcluidosSem);
	bool _esElCliente(void* cli) {
		SckCliente* cliente2 = (SckCliente*) cli;
		return (cliente->socket == cliente2->socket);
	}
	list_remove_by_condition(SD->selExcluidos, _esElCliente);
	sem_post(&SD->selExcluidosSem);
}

void PonerCliente(SuperDatos* SD) {
	//acepta y agrega un cliente nuevo a la lista
	void* clienteNuevo = (void*) Socket_AceptarConexion(SD->server);
	list_add(SD->clientes, clienteNuevo);
	if (config_get_int_value(SD->config, "LOG_CLIENTENUEVO") == 1)
		log_info(SD->logger, "Conexión de cliente: %d", ((SckCliente*) clienteNuevo)->socket->descriptor);
}

void SacarCliente(SckCliente* cliente, SuperDatos* SD) {
	//desconecta y borra de la lista al cliente
	if (config_get_int_value(SD->config, "LOG_CAIDACLIENTE") == 1)
		log_info(SD->logger, "Desconexión de cliente: %d", cliente->socket->descriptor);
	bool _esElCliente(void* cli) {
		SckCliente* cliente2 = (SckCliente*) cli;
		return (cliente->socket == cliente2->socket);
	}
	list_remove_by_condition(SD->clientes, _esElCliente);
	Socket_CerrarCliente(cliente);
}

uint32_t ThreadLibre(SuperDatos* SD) {
	//devuelve el indice de un thread libre (bloqueado, haciendo nada)
	uint32_t i = 0;
	while (SD->threadsLibres[i] != true) {
		i++;
		if (i == config_get_int_value(SD->config, "THREADS_MAX")) i = 0;
	}
	return i;
}

void CrearThread(pthread_t* tid, void(*func)(void*), SuperDatos* SD) {
	//crea un hilo pasandole los superdatos como parámetro, asignandole la función func
	pthread_attr_t attr; pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_create(tid, &attr, (void*) func, (void*) SD);
	pthread_attr_destroy(&attr);
}
