#ifndef EXT2_OPERATIONS_H_
#define EXT2_OPERATIONS_H_

	#include "main.h"
	#include "request.h"
	#include "ext2_ubic.h"
	#include "ext2_internal.h"

	bool CreateFile(SckCliente* cliente, char* path, SuperDatos* SD);
	bool OpenFile(SckCliente* cliente, char* path, SuperDatos* SD);
	bool ReadFile(SckCliente* cliente, char* path, off_t offset, uint32_t cant, SuperDatos* SD);
	bool WriteFile(SckCliente* cliente, char* path, off_t offset, uint32_t cant, SuperDatos* SD);
	bool DeleteFile(SckCliente* cliente, char* path, SuperDatos* SD);
	bool TruncateFile(SckCliente* cliente, char* path, uint32_t size, SuperDatos* SD);
	bool CloseFile(SckCliente* cliente, char* path, SuperDatos* SD);
	bool CreateDir(SckCliente* cliente, char* path, SuperDatos* SD);
	bool ReadDir(SckCliente* cliente, char* path, SuperDatos* SD);
	bool DeleteDir(SckCliente* cliente, char* path, SuperDatos* SD);
	bool Getattr(SckCliente* cliente, char* path, SuperDatos* SD);

#endif /* EXT2_OPERATIONS_H_ */
