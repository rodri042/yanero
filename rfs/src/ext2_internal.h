#ifndef EXT2_INTERNAL_H_
#define EXT2_INTERNAL_H_

	#include "main.h"
	#include "ext2_ubic.h"
	#include "ext2_int_bitmap.h"
	#include "ext2_int_direntry.h"

	bool TruncarInodo(Inode* inodo, uint32_t inodoNum, uint32_t size, SuperDatos* SD);
	bool RecibirInodo(Inode* inodo, uint32_t inodoNum, SckCliente* cliente, Ubic* ubic, SuperDatos* SD);
	void EscribirInodo(Inode* inodo, uint32_t inodoNum, void* buf, Ubic* ubic, SuperDatos* SD);
	void MandarInodo(Inode* inodo, uint32_t inodoNum, SckCliente* cliente, Ubic* ubic, SuperDatos* SD);
	void* LeerInodo(Inode* inodo, uint32_t inodoNum, SuperDatos* SD);
	void ReservarBloquesInodo(Inode* inodo, Ubic* ubic, SuperDatos* SD);
	void LiberarBloquesInodo(Inode* inodo, Ubic* ubic, SuperDatos* SD);
	void RecorrerInodo(Inode* inodo, Ubic* ubic, void(*func)(uint32_t, void*, uint32_t, uint32_t), SuperDatos* SD);

	Inode* Inodo(uint32_t numero, SuperDatos* SD);
	void* CacheDameBloque(uint32_t bloque, SuperDatos* SD);
	void CacheTomaBloque(uint32_t bloqueNum, void* bloque, SuperDatos* SD);
	void CacheVolaBloque(uint32_t bloqueNum, SuperDatos* SD);

	void ModUsedDirsCount(uint32_t grupo, int32_t mod, SuperDatos* SD);
	void ModInodosLibres(uint32_t grupo, int32_t mod, SuperDatos* SD);
	void ModBloquesLibres(uint32_t grupo, int32_t mod, SuperDatos* SD);
	void LeerDescriptoresGB(SuperDatos* SD);
	void LeerSuperbloque(SuperDatos* SD);
	BlockgroupDesc* DescriptorGB(uint32_t grupo, SuperDatos* SD);
	Superblock* Superbloque(SuperDatos* SD);
	void* LeerBloque(uint32_t pos, bool delay, SuperDatos* SD);

#endif /* EXT2_INTERNAL_H_ */
