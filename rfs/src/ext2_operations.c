#include "ext2_operations.h"

void MandarRespuesta(SckCliente* cliente, char type, int32_t cod, SuperDatos* SD);
Inode* CrearInodo(char* path, uint32_t* inodoNum, int32_t* resp, SuperDatos* SD);

void MandarRespuesta(SckCliente* cliente, char type, int32_t cod, SuperDatos* SD) {
	SckPaquete* pkg = malloc(sizeof(SckPaquete));
	pkg->header.type = type;
	pkg->header.length = sizeof(int32_t);
	memcpy(pkg->data, &cod, pkg->header.length);
	Socket_Enviar(cliente, pkg);
	free(pkg);
}

bool CreateFile(SckCliente* cliente, char* path, SuperDatos* SD) {
	uint32_t inodoNum; int32_t resp;
	CrearInodo(path, &inodoNum, &resp, SD);
	Inodo(inodoNum, SD)->mode = EXT2_IFREG | 0777; //le dice al campo MODE que es un archivo

	MandarRespuesta(cliente, RSP_CREATEFILE, resp, SD);
	return inodoNum > 0;
}

bool OpenFile(SckCliente* cliente, char* path, SuperDatos* SD) {
	Inode* inodo = InodoDePath(path, NULL, SD);
	int32_t existe = (inodo != NULL) ? 0 : -2; //-ENOENT

	MandarRespuesta(cliente, RSP_OPENFILE, existe, SD);
	return (bool) existe;
}

bool ReadFile(SckCliente* cliente, char* path, off_t offset, uint32_t cant, SuperDatos* SD) {
	Inode* inodo;
	uint32_t inodoNum;

	inodo = InodoDePath(path, &inodoNum, SD);
	if (inodo == NULL) {
		MandarRespuesta(cliente, RSP_EOF, -2, SD); //-ENOENT
		return false;
	}

	Ubic ubic;
	ubic.offset = offset;
	ubic.cant = cant;

	MandarInodo(inodo, inodoNum, cliente, &ubic, SD); //esta función manda el contenido del archivo

	SckPaquete* pkg = malloc(sizeof(SckPaquete));
	pkg->header.type = RSP_EOF; //mando el EOF
	pkg->header.length = 0;
	Socket_Enviar(cliente, pkg);
	free(pkg);

	return true;
}

bool WriteFile(SckCliente* cliente, char* path, off_t offset, uint32_t cant, SuperDatos* SD) {
	Inode* inodo;
	uint32_t inodoNum;

	inodo = InodoDePath(path, &inodoNum, SD);
	if (inodo == NULL) {
		MandarRespuesta(cliente, RSP_EOF, -2, SD); //-ENOENT
		DesexcluirSelect(cliente, SD);
		DespertarSelect(SD);
		return false;
	}

	Ubic ubic;
	ubic.offset = offset;
	ubic.cant = cant;

	MandarRespuesta(cliente, RSP_WRITEFILE, 0, SD); //le doy el OK para que empieze a mandar

	int32_t resp = cant; //resp debe ser igual a la cantidad de bytes escritos
	bool recibi = RecibirInodo(inodo, inodoNum, cliente, &ubic, SD); //esta función actualiza el contenido del archivo
	if (!recibi) resp = -28; //-ENOSCP

	MandarRespuesta(cliente, RSP_WRITEFILE, resp, SD);

	DesexcluirSelect(cliente, SD);
	DespertarSelect(SD);
	return true;
}

bool DeleteFile(SckCliente* cliente, char* path, SuperDatos* SD) {
	uint32_t inodoNum;
	Inode* inodo = InodoDePath(path, &inodoNum, SD);
	int32_t existe = (inodo != NULL) ? 0 : -2; //-2 = -ENOENT

	if (existe == 0) {
		char* nombre; char* pathALaCarpeta;
		SepararCarpeta_Nombre(path, &nombre, &pathALaCarpeta);

		uint32_t carpetaNum;
		Inode* carpeta = InodoDePath(pathALaCarpeta, &carpetaNum, SD);
		if (carpeta == NULL) {
			MandarRespuesta(cliente, RSP_DELETEFILE, -2, SD); //-ENOENT
			return false;
		}
		DirDelete(carpeta, carpetaNum, inodoNum, SD); //borro el registro en el directorio
		TruncarInodo(inodo, inodoNum, 0, SD); //trunco el inodo a 0 bytes
		inodo->links = 0;
		inodo->dtime = 1;
		LiberarInodo(inodoNum, SD); //marco el inodo como libre

		free(nombre); free(pathALaCarpeta);
	}

	MandarRespuesta(cliente, RSP_DELETEFILE, existe, SD);
	return existe == 0;
}

bool TruncateFile(SckCliente* cliente, char* path, uint32_t size, SuperDatos* SD) {
	uint32_t inodoNum;
	Inode* inodo = InodoDePath(path, &inodoNum, SD);
	if (inodo == NULL) {
		MandarRespuesta(cliente, RSP_TRUNCATEFILE, -2, SD); //-ENOENT
		return false;
	}

	int32_t resp = 0;
	bool trunco = TruncarInodo(inodo, inodoNum, size, SD);
	if (!trunco) resp = -28; //-ENOSCP

	MandarRespuesta(cliente, RSP_TRUNCATEFILE, resp, SD);
	return 0;
}

bool CloseFile(SckCliente* cliente, char* path, SuperDatos* SD) { return true; }

bool CreateDir(SckCliente* cliente, char* path, SuperDatos* SD) {
	uint32_t inodoNum; int32_t resp;
	Inode* inodo = CrearInodo(path, &inodoNum, &resp, SD);

	if (inodoNum > 0) {
		Inodo(inodoNum, SD)->mode = EXT2_IFDIR | 0777; //le dice al campo MODE que es un directorio

		char* nombre; char* pathParent;	//separo el nombre del path a la carpeta anterior
		SepararCarpeta_Nombre(path, &nombre, &pathParent);
		uint32_t inodoNumParent; //obtengo el inodo anterior a la carpeta
		InodoDePath(pathParent, &inodoNumParent, SD);

		DirAdd(inodo, inodoNum, ".", inodoNum, SD); inodo->links++;
		DirAdd(inodo, inodoNum, "..", inodoNumParent, SD); Inodo(inodoNumParent, SD)->links++;
		ModUsedDirsCount(GrupoDeInodo(inodoNum, SD), 1, SD);

		free(nombre); free(pathParent);
	}

	MandarRespuesta(cliente, RSP_CREATEDIR, resp, SD);
	return inodoNum > 0;
}

bool ReadDir(SckCliente* cliente, char* path, SuperDatos* SD) {
	uint32_t bytesAMandar;
	char* info = ListadoDePath(path, &bytesAMandar, SD);
	if (info == NULL) return false;

	SckPaquete* pkg;
	char* mandarDesde = info;
	while (bytesAMandar > 0) { //mando los datos verificando que no superen MAX_BUFFER bytes
		pkg = malloc(sizeof(SckPaquete));
		pkg->header.type = RSP_READDIR;
		if (bytesAMandar > MAX_BUFFER)
			pkg->header.length = MAX_BUFFER;
		else
			pkg->header.length = bytesAMandar;
		memcpy(pkg->data, mandarDesde, pkg->header.length);
		Socket_Enviar(cliente, pkg);
		mandarDesde += pkg->header.length;
		bytesAMandar -= pkg->header.length;
		free(pkg);
	}
	free(info);

	pkg = malloc(sizeof(SckPaquete));
	pkg->header.type = RSP_EOF; //mando el EOF
	pkg->header.length = 0;
	Socket_Enviar(cliente, pkg);
	free(pkg);

	return true;
}

bool DeleteDir(SckCliente* cliente, char* path, SuperDatos* SD){
	uint32_t inodoNum;
	Inode* inodo = InodoDePath(path, &inodoNum, SD);
	int32_t existe = (inodo != NULL) ? 0 : -2; //-2 = -ENOENT

	if (existe == 0) {
		if (DirCount(inodo, inodoNum, SD) > 2) {
			//si el directorio no está vacío rompo
			MandarRespuesta(cliente, RSP_DELETEDIR, -ENOTEMPTY, SD);
			return false;
		}

		char* nombre; char* pathALaCarpeta;
		SepararCarpeta_Nombre(path, &nombre, &pathALaCarpeta);

		uint32_t carpetaNum;
		Inode* carpeta = InodoDePath(pathALaCarpeta, &carpetaNum, SD);
		if (carpeta == NULL) {
			MandarRespuesta(cliente, RSP_DELETEDIR, -2, SD); //-ENOENT
			return false;
		}
		DirDelete(carpeta, carpetaNum, inodoNum, SD); //borro el registro en el directorio
		TruncarInodo(inodo, inodoNum, 0, SD); //trunco el inodo a 0 bytes
		inodo->links = 0;
		inodo->dtime = 1;
		LiberarInodo(inodoNum, SD); //marco el inodo como libre
		carpeta->links--; //la carpeta padre tiene un hardlink menos (por el ..)
		ModUsedDirsCount(GrupoDeInodo(inodoNum, SD), -1, SD);

		free(nombre); free(pathALaCarpeta);
	}

	MandarRespuesta(cliente, RSP_DELETEDIR, existe, SD);
	return existe == 0;
}

bool Getattr(SckCliente* cliente, char* path, SuperDatos* SD) {
	Inode* inodo;
	uint32_t inodoNum;

	inodo = InodoDePath(path, &inodoNum, SD);

	EntradaAtributos attr; //si no existe le llega un inodo con atime = 0
	memset(&attr, 0, sizeof(EntradaAtributos));
	if (inodo != NULL) {
		pthread_rwlock_rdlock(&SD->locks.inodos[inodoNum]);
		attr.atime = inodo->atime;
		attr.ctime = inodo->ctime;
		attr.gid = inodo->gid;
		attr.links = inodo->links;
		attr.mode = inodo->mode;
		attr.mtime = inodo->mtime;
		attr.size = inodo->size;
		attr.uid = inodo->uid;
		pthread_rwlock_unlock(&SD->locks.inodos[inodoNum]);
	}

	SckPaquete* pkg = malloc(sizeof(SckPaquete));
	pkg->header.type = RSP_GETATTR;
	pkg->header.length = sizeof(EntradaAtributos);
	memcpy(pkg->data, &attr, pkg->header.length);
	Socket_Enviar(cliente, pkg);
	free(pkg);

	return true;
}

Inode* CrearInodo(char* path, uint32_t* inodoNum, int32_t* resp, SuperDatos* SD) {
	Inode* inodo = NULL;
	*inodoNum = 0;

	char* nombre; char* pathALaCarpeta;	//separo el nombre del path a la carpeta
	SepararCarpeta_Nombre(path, &nombre, &pathALaCarpeta);

	uint32_t carpetaNum; //obtengo el inodo de la carpeta
	Inode* carpeta = InodoDePath(pathALaCarpeta, &carpetaNum, SD);
	if (carpeta != NULL) { //si el path a la carpeta EXISTE
		*inodoNum = InodoLibre(true, SD);
		if (*inodoNum != 0) {
			inodo = Inodo(*inodoNum, SD); //limpio el inodo
			//inodo->mode depende si es un archivo o un directorio
			inodo->uid = 0;
			inodo->size = 0;
			inodo->atime = 1; //por el protocolo (atime=0 => archivo no existe)
			inodo->ctime = 0;
			inodo->mtime = 0;
			inodo->dtime = 0;
			inodo->gid = 0;
			inodo->links = 1;
			inodo->nr_blocks = 0;
			inodo->flags = 0;
			inodo->reserved1 = 0;
			inodo->file_acl = 0;
			inodo->size_hi = 0;
			inodo->generation = 0;
			int i;
			for (i=0; i < PUNTEROS_DIRECTOS; i++) inodo->blocks[i] = 0;
			for (i=0; i < NIVELES_INDIRECCION; i++) inodo->iblock[i] = 0;
			log_trace(SD->logger, "Se creó el inodo %d", *inodoNum);

			DirAdd(carpeta, carpetaNum, nombre, *inodoNum, SD); //agrego la entrada de directorio
			*resp = 0;
		} else *resp = -28; //ENOSPC
	} else *resp = -2; //-ENOENT
	free(nombre); free(pathALaCarpeta);

	return inodo;
}
