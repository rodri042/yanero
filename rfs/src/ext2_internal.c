#include "ext2_internal.h"

void ReservarBloquesIndirectos(uint32_t nivelIndirec, uint32_t* bloque, uint32_t* bloquesLiberados, uint32_t* bloqueActual, Ubic* ubic, Inode* inodo, SuperDatos* SD);
void LiberarBloquesIndirectos(uint32_t nivelIndirec, uint32_t* bloque, uint32_t* bloquesLiberados, uint32_t* bloqueActual, Ubic* ubic, Inode* inodo, SuperDatos* SD);
void RecorrerBloquesIndirectos(uint32_t nivelIndirec, uint32_t bloque, uint32_t* bytesUsados, uint32_t* bytesRecorridos, Ubic* ubic, void(*func)(uint32_t, void*, uint32_t, uint32_t), SuperDatos* SD);
void RecorrerMuchosBloques(uint32_t* orig, uint32_t bloquesMax, uint32_t* bytesUsados, uint32_t* byteActual, Ubic* ubic, void(*func)(uint32_t, void*, uint32_t, uint32_t), SuperDatos* SD);
bool TodosBloquesLibres(uint32_t* orig, uint32_t size);

bool TruncarInodo(Inode* inodo, uint32_t inodoNum, uint32_t size, SuperDatos* SD) {
	//cambia el tamaño del inodo y por lo tanto del archivo/carpeta
	//devuelve falso si no hay bloques libres suficientes
	if (inodo->size == size) return true;
	uint32_t cantBloquesVieja = Byte2Block(inodo->size, SD);
	uint32_t cantBloquesNueva = Byte2Block(size, SD);

	if (inodoNum != 0) pthread_rwlock_wrlock(&SD->locks.inodos[inodoNum]);
	if (size < inodo->size) { //si quiero ACHICAR el archivo
		uint32_t bloquesALiberar = cantBloquesVieja - cantBloquesNueva;
		//me paro en la cantBloquesNueva, y libero de ahí en adelante bloquesALiberar
		Ubic ubic;
		ubic.offset = cantBloquesNueva;
		ubic.cant = bloquesALiberar;
		LiberarBloquesInodo(inodo, &ubic, SD);
	} else {  //si quiero AGRANDAR el archivo
		uint32_t bloquesAReservar = cantBloquesNueva - cantBloquesVieja;
		//total  = "incluyendo bloques indirectos"
		uint32_t cantBloquesViejaTotal = inodo->nr_blocks / (BlockSize(SD) / 512);
		uint32_t cantBloquesNuevaTotal = BloquesNecesariosPara(size, SD);
		if (SD->superbloque->free_blocks < cantBloquesNuevaTotal - cantBloquesViejaTotal)
			return false; //y bue, no hay espacio

		//me paro en la cantBloquesVieja, y reservo de ahí en adelante bloquesAReservar
		Ubic ubic;
		ubic.offset = cantBloquesVieja;
		ubic.cant = bloquesAReservar;
		ReservarBloquesInodo(inodo, &ubic, SD);
	}
	inodo->size = size;
	if (inodoNum != 0) pthread_rwlock_unlock(&SD->locks.inodos[inodoNum]);

	return true;
}

bool RecibirInodo(Inode* inodo, uint32_t inodoNum, SckCliente* cliente, Ubic* ubic, SuperDatos* SD) {
	//recorre el inodo copiando los datos recibidos del cliente
	//devuelve falso si no hay bloques libres
	if (inodoNum != 0) pthread_rwlock_wrlock(&SD->locks.inodos[inodoNum]);
	if (ubic->offset + ubic->cant > inodo->size) { //si quiero escribir más allá del tamaño del inodo
		bool agrando = TruncarInodo(inodo, 0, ubic->offset + ubic->cant, SD); //lo tengo que agrandar

		if (!agrando) {
			if (inodoNum != 0) pthread_rwlock_unlock(&SD->locks.inodos[inodoNum]);
			return false;
		}
	}

	SckPaquete* pkg = Socket_Recibir(cliente);
	if (pkg == NULL) return true;
	if (pkg->header.length == 0) {
		free(pkg);
		return true;
	}
	uint32_t pkgBytesUsados = 0;
	uint32_t pkgBytesRestantes = pkg->header.length;
	bool cortar = false;

	void _recibir(uint32_t bloqueNum, void* bloque, uint32_t bloqueOffset, uint32_t bytesARecibir) {
		if (cortar) return;
		void* recibirDesde = bloque + bloqueOffset;
		usleep(SD->retardo * 1000);

		while (bytesARecibir > 0) {
			if (pkgBytesRestantes == 0) {
				free(pkg);
				pkg = Socket_Recibir(cliente);
				if (pkg == NULL) { cortar = true; log_error(SD->logger, "El cliente %d se desconectó en medio de la operación", cliente->socket->descriptor); return; }
				if (pkg->header.type == RSP_EOF) { //por si lo que me manda no coincide
					free(pkg);                     //con lo que me dijo que me mandaba
					cortar = true;
					log_error(SD->logger, "El cliente %d me mintió, me escribió menos bytes de los que dijo", cliente->socket->descriptor);
					return;
				}
				pkgBytesUsados = 0;
				pkgBytesRestantes = pkg->header.length;
			}
			if (pkgBytesRestantes >= bytesARecibir) {
				memcpy(recibirDesde, pkg->data + pkgBytesUsados, bytesARecibir);
				pkgBytesUsados += bytesARecibir;
				pkgBytesRestantes -= bytesARecibir;
				return;
			} else {
				memcpy(recibirDesde, pkg->data + pkgBytesUsados, pkgBytesRestantes);
				bytesARecibir -= pkgBytesRestantes;
				pkgBytesUsados += pkgBytesRestantes;
				pkgBytesRestantes -= pkgBytesRestantes;
			}
		}

		CacheTomaBloque(bloqueNum, bloque, SD);
	}

	RecorrerInodo(inodo, ubic, _recibir, SD);
	if (!cortar) Socket_Recibir(cliente); //esperar al EOF
	if (inodoNum != 0) pthread_rwlock_unlock(&SD->locks.inodos[inodoNum]);
	return true;
}

void EscribirInodo(Inode* inodo, uint32_t inodoNum, void* buf, Ubic* ubic, SuperDatos* SD) {
	//recorre el inodo copiando los datos de buf
	uint32_t bytesEscritos = 0;

	if (inodoNum != 0) pthread_rwlock_wrlock(&SD->locks.inodos[inodoNum]);
	if (ubic->offset + ubic->cant > inodo->size) { //si quiero escribir más allá del tamaño del inodo
		TruncarInodo(inodo, 0, ubic->offset + ubic->cant, SD); //lo tengo que agrandar
	}

	void _escribir(uint32_t bloqueNum, void* bloque, uint32_t bloqueOffset, uint32_t bytesAEscribir) {
		void* escribirDesde = bloque + bloqueOffset;
		usleep(SD->retardo * 1000);

		memcpy(escribirDesde, buf + bytesEscritos, bytesAEscribir);
		bytesEscritos += bytesAEscribir;
	}

	RecorrerInodo(inodo, ubic, _escribir, SD);
	if (inodoNum != 0) pthread_rwlock_unlock(&SD->locks.inodos[inodoNum]);
}

void MandarInodo(Inode* inodo, uint32_t inodoNum, SckCliente* cliente, Ubic* ubic, SuperDatos* SD) {
	//manda el contenido del inodo por sockets a un cliente
	if (inodoNum != 0) pthread_rwlock_rdlock(&SD->locks.inodos[inodoNum]);
	void _mandar(uint32_t bloqueNum, void* bloque, uint32_t bloqueOffset, uint32_t bytesAMandar) {
		void* mandarDesde = bloque + bloqueOffset;

		void* bloqueCacheado = CacheDameBloque(bloqueNum, SD); //intento obtener el bloque de la cache
		if (bloqueCacheado != NULL) {
			mandarDesde = bloqueCacheado + bloqueOffset;
		} else {
			usleep(SD->retardo * 1000);
		}

		while (bytesAMandar > 0) { //mando los datos verificando que no superen MAX_BUFFER bytes
			SckPaquete* pkg = malloc(sizeof(SckPaquete));
			pkg->header.type = RSP_READFILE;
			if (bytesAMandar > MAX_BUFFER)
				pkg->header.length = MAX_BUFFER;
			else
				pkg->header.length = bytesAMandar;
			memcpy(pkg->data, mandarDesde, pkg->header.length);
			Socket_Enviar(cliente, pkg);
			mandarDesde += pkg->header.length;
			bytesAMandar -= pkg->header.length;
			free(pkg);
		}

		if (bloqueCacheado == NULL) {
			//cacheo el bloque para proximas lecturas
			CacheTomaBloque(bloqueNum, bloque, SD);
		} else {
			free(bloqueCacheado);
		}
	}
	RecorrerInodo(inodo, ubic, _mandar, SD);
	if (inodoNum != 0) pthread_rwlock_unlock(&SD->locks.inodos[inodoNum]);
}

void* LeerInodo(Inode* inodo, uint32_t inodoNum, SuperDatos* SD) {
	//carga el contenido del inodo en memoria
	if ( inodo->size == 0 ) return NULL;
	void* file = malloc(inodo->size);
	uint32_t bytesLeidos = 0;

	if (inodoNum != 0) pthread_rwlock_rdlock(&SD->locks.inodos[inodoNum]);
	void _leer(uint32_t bloqueNum, void* bloque, uint32_t bloqueOffset, uint32_t bytesALeer) {
		void* leerDesde = bloque + bloqueOffset;
		usleep(SD->retardo * 1000);

		memcpy(file + bytesLeidos, leerDesde, bytesALeer);
		bytesLeidos += bytesALeer;
	}

	Ubic ubic;
	ubic.offset = 0;
	ubic.cant = inodo->size;
	RecorrerInodo(inodo, &ubic, _leer, SD);
	if (inodoNum != 0) pthread_rwlock_unlock(&SD->locks.inodos[inodoNum]);
	return file;
}

void ReservarBloquesInodo(Inode* inodo, Ubic* ubic, SuperDatos* SD) {
	//itera por los bloques del inodo reservando bloques del bitmap
	//desde el BLOQUE ubic->offset hasta el BLOQUE ubic->offset+ubic->cant
	uint32_t bloquesReservados = 0;
	if (ubic->cant == 0) return;

	//Bloques directos:
	int i;
	for (i = ubic->offset; i < PUNTEROS_DIRECTOS && bloquesReservados < ubic->cant; i++) {
		inodo->blocks[i] = BloqueLibre(true, SD);
		bloquesReservados++;
		inodo->nr_blocks += BlockSize(SD) / 512;
	}
	uint32_t bloquesRecorridos = PUNTEROS_DIRECTOS;

	//Bloques indirectos:
	uint8_t nivelIndirec = 0;
	while (bloquesReservados < ubic->cant) {
		bloquesRecorridos = MaxDireccion(nivelIndirec - 1, SD) / BlockSize(SD);
		uint32_t maxBloques = MaxDireccion(nivelIndirec, SD) / BlockSize(SD); //máxima cantidad de bloques direccionables con este nivel de indirección
		if (ubic->offset < maxBloques) { //si necesita usar este puntero a indirectos...
			ReservarBloquesIndirectos(nivelIndirec, &inodo->iblock[nivelIndirec], &bloquesReservados, &bloquesRecorridos, ubic, inodo, SD);
		}
		nivelIndirec++;
	}
}

void ReservarBloquesIndirectos(uint32_t nivelIndirec, uint32_t* bloque, uint32_t* bloquesReservados, uint32_t* bloqueActual, Ubic* ubic, Inode* inodo, SuperDatos* SD) {
	//libera bloques de los bloques indirectos recursivamente
	uint32_t blockSize = BlockSize(SD);
	uint32_t punterosPorBloque = blockSize / sizeof(uint32_t);
	int i;

	if (*bloque == 0) { //si el bloque de punteros no está creado lo crea
		*bloque = BloqueLibre(true, SD);
		//y llena todos los punteros del bloque con ceros
		memset(LeerBloque(*bloque, true, SD), 0, BlockSize(SD));
		inodo->nr_blocks += blockSize / 512;
	}
	uint32_t* ipunteros = LeerBloque(*bloque, true, SD);

	if (nivelIndirec == 0) {
		for (i=0; i < punterosPorBloque && *bloquesReservados < ubic->cant; i++) {
			if (ubic->offset <= *bloqueActual) {
				ipunteros[i] = BloqueLibre(true, SD); //reservo el bloque de datos
				memset(LeerBloque(ipunteros[i], true, SD), '\0', blockSize);
				(*bloquesReservados)++;
				inodo->nr_blocks += blockSize / 512;
			}
			(*bloqueActual)++;
		}
	} else {
		int i;
		for (i=0; i < punterosPorBloque; i++) {
			*bloqueActual = ByteInicialBloqueIndirec(nivelIndirec, i, SD) / blockSize;
			uint32_t ultimoBloqueDelPuntero = (ByteInicialBloqueIndirec(nivelIndirec, i+1, SD) / blockSize) - 1; //último bloque al que llega este bloque de punteros
			if (ubic->offset <= ultimoBloqueDelPuntero) {
				ReservarBloquesIndirectos(nivelIndirec - 1, &ipunteros[i], bloquesReservados, bloqueActual, ubic, inodo, SD);
				if (*bloquesReservados == ubic->cant) break;
			}
		}
	}
}

void LiberarBloquesInodo(Inode* inodo, Ubic* ubic, SuperDatos* SD) {
	//itera por los bloques del inodo liberando bloques del bitmap
	//desde el BLOQUE ubic->offset hasta el BLOQUE ubic->offset+ubic->cant
	uint32_t bloquesLiberados = 0;
	if (ubic->cant == 0) return;

	//Bloques directos:
	int i;
	for (i = ubic->offset; i < PUNTEROS_DIRECTOS && bloquesLiberados < ubic->cant; i++) {
		LiberarBloque(inodo->blocks[i], SD);
		inodo->blocks[i] = 0;
		inodo->nr_blocks -= BlockSize(SD) / 512;
		bloquesLiberados++;
	}
	uint32_t bloquesRecorridos = PUNTEROS_DIRECTOS;

	//Bloques indirectos:
	uint8_t nivelIndirec = 0;
	while (bloquesLiberados < ubic->cant) {
		bloquesRecorridos = MaxDireccion(nivelIndirec - 1, SD) / BlockSize(SD);
		uint32_t maxBloques = MaxDireccion(nivelIndirec, SD) / BlockSize(SD); //máxima cantidad de bloques direccionables con este nivel de indirección
		if (ubic->offset < maxBloques) { //si necesita usar este puntero a indirectos...
			LiberarBloquesIndirectos(nivelIndirec, &inodo->iblock[nivelIndirec], &bloquesLiberados, &bloquesRecorridos, ubic, inodo, SD);
		}
		nivelIndirec++;
	}
}

void LiberarBloquesIndirectos(uint32_t nivelIndirec, uint32_t* bloque, uint32_t* bloquesLiberados, uint32_t* bloqueActual, Ubic* ubic, Inode* inodo, SuperDatos* SD) {
	//libera bloques de los bloques indirectos recursivamente
	uint32_t blockSize = BlockSize(SD);
	uint32_t punterosPorBloque = blockSize / sizeof(uint32_t);

	uint32_t* ipunteros = LeerBloque(*bloque, true, SD);

	if (nivelIndirec == 0) {
		int i;
		for (i=0; i < punterosPorBloque && *bloquesLiberados < ubic->cant; i++) {
			if (ubic->offset <= *bloqueActual) {
				LiberarBloque(ipunteros[i], SD); //libero el bloque de datos
				ipunteros[i] = 0;
				inodo->nr_blocks -= blockSize / 512;
				(*bloquesLiberados)++;
			}
			(*bloqueActual)++;
		}
	} else {
		int i;
		for (i=0; i < punterosPorBloque; i++) {
			*bloqueActual = ByteInicialBloqueIndirec(nivelIndirec, i, SD) / blockSize;
			uint32_t ultimoBloqueDelPuntero = (ByteInicialBloqueIndirec(nivelIndirec, i+1, SD) / blockSize) - 1; //último bloque al que llega este bloque de punteros
			if (ubic->offset <= ultimoBloqueDelPuntero) {
				LiberarBloquesIndirectos(nivelIndirec - 1, &ipunteros[i], bloquesLiberados, bloqueActual, ubic, inodo, SD);
				if (*bloquesLiberados == ubic->cant) break;
			}
		}
	}

	if (TodosBloquesLibres(ipunteros, punterosPorBloque)) {
		LiberarBloque(*bloque, SD); //libero el bloque de punteros
		*bloque = 0;
		inodo->nr_blocks -= blockSize / 512;
	}
}

bool TodosBloquesLibres(uint32_t* orig, uint32_t size) {
	//devuelve si todos los punteros están libres en un bloque de punteros (o bloque indirecto)
	int i;
	for(i=0; i < size; i++) {
		if (orig[i] != 0) return false;
	}
	return true;
}

void RecorrerInodo(Inode* inodo, Ubic* ubic, void(*func)(uint32_t, void*, uint32_t, uint32_t), SuperDatos* SD) {
	//itera por los bloques del inodo aplicando la función func,
	//desde el byte ubic->offset hasta ubic->offset+ubic->cant
	uint32_t bytesUsados = 0;
	uint32_t bytesRecorridos = 0;
	if ( inodo->size == 0 ) return; //si el archivo pesa 0 bytes salgo de la funcion

	if (ubic->offset + ubic->cant > inodo->size) {
		//si quiere leer más de lo que pesa el archivo, le marco el límite
		ubic->cant = ubic->cant - (ubic->offset + ubic->cant - inodo->size);
	}

	//Bloques directos:
	if (ubic->offset < MaxDireccion(0, SD)) {
		//si el offset está dentro de los primeros PUNTEROS_DIRECTOS bloques...
		RecorrerMuchosBloques(inodo->blocks, PUNTEROS_DIRECTOS, &bytesUsados, &bytesRecorridos, ubic, func, SD);
	}

	//Bloques indirectos:
	uint8_t nivelIndirec = 0;
	while (bytesUsados < ubic->cant) {
		bytesRecorridos = MaxDireccion(nivelIndirec - 1, SD);
		if (ubic->offset < MaxDireccion(nivelIndirec, SD)) { //si necesita usar este puntero a indirectos...
			RecorrerBloquesIndirectos(nivelIndirec, inodo->iblock[nivelIndirec], &bytesUsados, &bytesRecorridos, ubic, func, SD);
		}
		nivelIndirec++;
	}
}

void RecorrerBloquesIndirectos(uint32_t nivelIndirec, uint32_t bloque, uint32_t* bytesUsados, uint32_t* bytesRecorridos, Ubic* ubic, void(*func)(uint32_t, void*, uint32_t, uint32_t), SuperDatos* SD) {
	//recorre los indireccionamientos recursivamente
	uint32_t blockSize = BlockSize(SD);
	uint32_t punterosPorBloque = blockSize / sizeof(uint32_t);

	uint32_t* ipunteros = LeerBloque(bloque, true, SD);

	if (nivelIndirec == 0) {
		RecorrerMuchosBloques(ipunteros, punterosPorBloque, bytesUsados, bytesRecorridos, ubic, func, SD);
		return;
	}

	int i;
	for (i=0; i < punterosPorBloque; i++) {
		*bytesRecorridos = ByteInicialBloqueIndirec(nivelIndirec, i, SD);
		if (ubic->offset < ByteInicialBloqueIndirec(nivelIndirec, i+1, SD)) {
			RecorrerBloquesIndirectos(nivelIndirec - 1, ipunteros[i], bytesUsados, bytesRecorridos, ubic, func, SD);
			if (*bytesUsados == ubic->cant) break;
		}
	}
}

void RecorrerMuchosBloques(uint32_t* orig, uint32_t bloquesMax, uint32_t* bytesUsados, uint32_t* byteActual, Ubic* ubic, void(*func)(uint32_t, void*, uint32_t, uint32_t), SuperDatos* SD) {
	//recorre bloques de un array de punteros hasta un tope (ubic->cant)
	//o hasta que se acaben los punteros (bloquesMax)
	uint32_t bytesAUsar=0,
			 i;
	uint32_t usarDesde = 0; //<< desde qué parte del bloque empiezo a usar (el offset)
	void* bloque = NULL;

	for (i=0; i < bloquesMax; i++) {
		bloque = LeerBloque(orig[i], false, SD); //no le aplica el retardo, eso lo maneja la función llamante
		bytesAUsar = 0;
		//Estoy parado en *byteActual...
		//(los bytes recorridos +1 -1; +1 porque es el próximo a recorrer; -1 para que quede basado en 0, como el offset)

		//mando en dos casos:
		if (*byteActual <=  ubic->offset && ubic->offset <= *byteActual + BlockSize(SD) - 1) {
			/* offset está en el intervalo cerrado entre:
			- donde estoy parado
			- donde puedo llegar en ésta iteración */
			bytesAUsar = ubic->cant - *bytesUsados;
			//no puedo llegar más lejos que el final del bloque, ni empezar antes del offset:
			if (bytesAUsar > (*byteActual + BlockSize(SD)) - ubic->offset)
				bytesAUsar = (*byteActual + BlockSize(SD)) - ubic->offset;
			usarDesde = ubic->offset - *byteActual;
		} else if (*byteActual > ubic->offset) {
			//estoy parado entre offset y el final del tramo
			bytesAUsar = ubic->cant - *bytesUsados;
			if (bytesAUsar > BlockSize(SD)) bytesAUsar = BlockSize(SD);
			usarDesde = 0;
		}

		*bytesUsados += bytesAUsar; //aumento los bytes leidos y recorridos
		*byteActual += BlockSize(SD);

		if (bytesAUsar > 0) func(orig[i], bloque, usarDesde, bytesAUsar); //delega la operación necesaria a la función

		if (*bytesUsados == ubic->cant) break;
	}
}

Inode* Inodo(uint32_t numero, SuperDatos* SD) {
	//devuelve un puntero al inodo a partir de su número absoluto de inodo
	if (numero > SD->superbloque->inodes || numero == 0) {
		return NULL;
	}
	uint32_t grupo = GrupoDeInodo(numero, SD);
	numero = InodoRelativo(numero, SD);
	uint32_t posicionTablaInodos = SD->descriptoresGB[grupo].inode_table * BlockSize(SD);
	uint32_t posicionInodo = posicionTablaInodos + numero * sizeof(Inode);
	usleep(SD->retardo * 1000);

	return SD->partMap + posicionInodo;
}

void* CacheDameBloque(uint32_t bloqueNum, SuperDatos* SD) {
	//obtiene un bloque de la cache
	if (!SD->cacheConectada) return NULL;

	memcached_return resCache;
	void* bloque = NULL;

	//la clave es: s + número de bloque
	char key[MAX_KEY];
	key[0] = 's';
	int i; for (i=1; i < MAX_KEY; i++) key[i] = '\0';
	sprintf(((char*) key) + 1, "%d", bloqueNum);

	resCache = Cache_ObtenerValor(SD->ipCache, key, NULL, &bloque);
	if (resCache != MEMCACHED_SUCCESS) {
		free(bloque);
		return NULL;
	}

	return bloque;
}

void CacheTomaBloque(uint32_t bloqueNum, void* bloque, SuperDatos* SD) {
	//cachea un bloque
	//la clave es: s + número de bloque
	if (!SD->cacheConectada) return;

	char key[MAX_KEY];
	key[0] = 's';
	int i; for (i=1; i < MAX_KEY; i++) key[i] = '\0';
	sprintf(((char*) key) + 1, "%d", bloqueNum);

	Cache_InsertarClaveNueva(SD->ipCache, key, BlockSize(SD), (char*) bloque);
}

void CacheVolaBloque(uint32_t bloqueNum, SuperDatos* SD) {
	//borra un bloque de la cache
	//la clave es: s + número de bloque
	if (!SD->cacheConectada) return;

	char key[MAX_KEY];
	key[0] = 's';
	int i; for (i=1; i < MAX_KEY; i++) key[i] = '\0';
	sprintf(((char*) key) + 1, "%d", bloqueNum);

	Cache_EliminarDato(SD->ipCache, key);
}

void ModUsedDirsCount(uint32_t grupo, int32_t mod, SuperDatos* SD) {
	//modifica atómicamente el campo used_dirs_count del descriptor de un grupo
	sem_wait(&SD->locks.usedDirsCount);
	SD->descriptoresGB[grupo].used_dirs_count += mod;
	(DescriptorGB(grupo, SD))->used_dirs_count += mod;
	sem_post(&SD->locks.usedDirsCount);
}

void ModInodosLibres(uint32_t grupo, int32_t mod, SuperDatos* SD) {
	//modifica atómicamente los inodos libres
	sem_wait(&SD->locks.freeInodes);
	SD->superbloque->free_inodes += mod;
	SD->descriptoresGB[grupo].free_inodes_count += mod;
	(Superbloque(SD))->free_inodes += mod;
	(DescriptorGB(grupo, SD))->free_inodes_count += mod;
	sem_post(&SD->locks.freeInodes);
}

void ModBloquesLibres(uint32_t grupo, int32_t mod, SuperDatos* SD) {
	//modifica atómicamente los bloques libres
	sem_wait(&SD->locks.freeBlocks);
	SD->superbloque->free_blocks += mod;
	SD->descriptoresGB[grupo].free_blocks_count += mod;
	(Superbloque(SD))->free_blocks += mod;
	(DescriptorGB(grupo, SD))->free_blocks_count += mod;
	sem_post(&SD->locks.freeBlocks);
}

void LeerDescriptoresGB(SuperDatos* SD) {
	//carga el vector de descriptores de grupos en memoria
	SD->descriptoresGB = malloc(sizeof(BlockgroupDesc) * CantGrupos(SD));
	memcpy(SD->descriptoresGB, SD->partMap + 2048, sizeof(BlockgroupDesc) * CantGrupos(SD));
}

void LeerSuperbloque(SuperDatos* SD) {
	//carga el superbloque en memoria
	Superblock* superbloqueReal = Superbloque(SD);
	superbloqueReal->mount_count++; //aumentar contador de mount
	SD->superbloque = malloc(sizeof(Superblock));
	memcpy(SD->superbloque, SD->partMap + 1024, sizeof(Superblock));
}

BlockgroupDesc* DescriptorGB(uint32_t grupo, SuperDatos* SD) { return SD->partMap + 2048 + sizeof(BlockgroupDesc) * grupo; }
Superblock* Superbloque(SuperDatos* SD) { return SD->partMap + 1024; }

void* LeerBloque(uint32_t pos, bool delay, SuperDatos* SD) {
	//con esto podemos devolver la posición de memoria
	//del contenido de un bloque
	if (delay) usleep(SD->retardo * 1000);
	return SD->partMap + pos * BlockSize(SD);
}
