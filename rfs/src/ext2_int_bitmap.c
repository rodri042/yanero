#include "ext2_int_bitmap.h"

void LiberarInodo(uint32_t inodo, SuperDatos* SD) {
	//marca un inodo como libre
	pthread_rwlock_wrlock(&SD->locks.bitmapInodos);
	pthread_rwlock_wrlock(&SD->locks.inodos[inodo]);
	char* bitmapInodos = LeerBloque(SD->descriptoresGB[GrupoDeInodo(inodo, SD)].inode_bitmap, true, SD);
	t_bitarray* bitarray = bitarray_create(bitmapInodos, SD->superbloque->inodes_per_group);
	bitarray_clean_bit(bitarray, InodoRelativo(inodo, SD));
	ModInodosLibres(GrupoDeInodo(inodo, SD), 1, SD);
	bitarray_destroy(bitarray);
	pthread_rwlock_unlock(&SD->locks.inodos[inodo]);
	pthread_rwlock_unlock(&SD->locks.bitmapInodos);
}

void LiberarBloque(uint32_t bloque, SuperDatos* SD) {
	//marca un bloque como libre
	pthread_rwlock_wrlock(&SD->locks.bitmapBloques);
	char* bitmapBloques = LeerBloque(SD->descriptoresGB[GrupoDeBloque(bloque, SD)].block_bitmap, true, SD);
	t_bitarray* bitarray = bitarray_create(bitmapBloques, SD->superbloque->blocks_per_group);
	bitarray_clean_bit(bitarray, BloqueRelativo(bloque, SD));
	ModBloquesLibres(GrupoDeBloque(bloque, SD), 1, SD);
	bitarray_destroy(bitarray);
	CacheVolaBloque(bloque, SD);
	pthread_rwlock_unlock(&SD->locks.bitmapBloques);
}

uint32_t InodoLibre(bool reservar, SuperDatos* SD) {
	//devuelve el primer inodo libre que encuentre (o 0 si no hay)
	//y lo reserva para evitar problemas de concurrencia, si reservar=true
	if (reservar) pthread_rwlock_wrlock(&SD->locks.bitmapInodos);
	if (!reservar) pthread_rwlock_rdlock(&SD->locks.bitmapInodos);
	int grupo;
	for (grupo=0; grupo < CantGrupos(SD); grupo++) {
		char* bitmapInodos = LeerBloque(SD->descriptoresGB[grupo].inode_bitmap, true, SD);
		t_bitarray* bitarray = bitarray_create(bitmapInodos, SD->superbloque->inodes_per_group);
		int i;
		for (i=0; i < SD->superbloque->inodes_per_group; i++) {
			if (!bitarray_test_bit(bitarray, i)) {
				if (reservar) {
					bitarray_set_bit(bitarray, i);
					ModInodosLibres(grupo, -1, SD);
				}
				bitarray_destroy(bitarray);
				pthread_rwlock_unlock(&SD->locks.bitmapInodos);
				return InodoAbsoluto(i, grupo, SD);
			}
		}
		bitarray_destroy(bitarray);
	}
	pthread_rwlock_unlock(&SD->locks.bitmapInodos);
	return 0; //ningún inodo libre
}

uint32_t BloqueLibre(bool reservar, SuperDatos* SD) {
	//devuelve el primer bloque libre que encuentre (o 0 si no hay)
	//y lo reserva para evitar problemas de concurrencia, si reservar=true
	if (reservar) pthread_rwlock_wrlock(&SD->locks.bitmapBloques);
	if (!reservar) pthread_rwlock_rdlock(&SD->locks.bitmapBloques);
	int grupo;
	for (grupo=0; grupo < CantGrupos(SD); grupo++) {
		char* bitmapBloques = LeerBloque(SD->descriptoresGB[grupo].block_bitmap, true, SD);
		t_bitarray* bitarray = bitarray_create(bitmapBloques, SD->superbloque->blocks_per_group);
		int i;
		for (i=0; i < SD->superbloque->blocks_per_group; i++) {
			if (!bitarray_test_bit(bitarray, i)) {
				if (reservar) {
					bitarray_set_bit(bitarray, i);
					ModBloquesLibres(grupo, -1, SD);
				}
				bitarray_destroy(bitarray);
				pthread_rwlock_unlock(&SD->locks.bitmapBloques);
				return BloqueAbsoluto(i, grupo, SD);
			}
		}
		bitarray_destroy(bitarray);
	}
	pthread_rwlock_unlock(&SD->locks.bitmapBloques);
	return 0; //ningún bloque libre
}

/*void BloquesLibres(uint32_t bytesNecesarios, t_list** bloques, bool reservar, SuperDatos* SD) {
	//devuelve una lista de fragmentos que contienen el inicio y el fin de los bloques contiguos
	//en el parámetro bloques (sale en NULL si no hay bloques libres suficientes)
	//y reserva los bloques para evitar problemas de concurrencia, si reservar=true
	*bloques = NULL;
	uint32_t blockSize = BlockSize(SD);
	uint32_t bloquesNecesarios = bytesNecesarios / blockSize;
	if (bytesNecesarios % blockSize != 0) bloquesNecesarios++;
	uint32_t bloquesConseguidos = 0;
	if (bloquesNecesarios > SD->superbloque->free_blocks) return;

	*bloques = list_create();
	int grupo, bloque;
	char* bitmapBloques;
	Fragmento* fragmento;
	uint32_t primerBloqueDatos;
	uint32_t cantBloquesDatos;
	uint32_t ultimoBloqueEncontrado;

	bool guardandoFragmento = false;
	if (reservar) pthread_rwlock_wrlock(&SD->locks.bitmapBloques);
	if (!reservar) pthread_rwlock_rdlock(&SD->locks.bitmapBloques);

	for (grupo=0; grupo < CantGrupos(SD); grupo++) {
		if (guardandoFragmento) {
			//si cambié de blockgroup y estaba guardando un fragmento, corto ahí
			fragmento->ultimo = ultimoBloqueEncontrado;
			list_add(*bloques, fragmento);
			guardandoFragmento = false;
		}
		bitmapBloques = LeerBloque(SD->descriptoresGB[grupo].block_bitmap, SD);
		primerBloqueDatos = BloqueRelativo(PosBloquesDatos(grupo, SD), SD);
		cantBloquesDatos = CantBloquesDatos(grupo, SD);

		t_bitarray* bitarray = bitarray_create(bitmapBloques, SD->superbloque->blocks_per_group);
		for (bloque=primerBloqueDatos; bloque < primerBloqueDatos + cantBloquesDatos; bloque++) {
			if (!bitarray_test_bit(bitarray, bloque)) {
				ultimoBloqueEncontrado = BloqueAbsoluto(bloque, grupo, SD);
				if (!guardandoFragmento) {
					//si encontré uno libre y no estaba guardando, empiezo a guardar
					fragmento = malloc(sizeof(Fragmento));
					fragmento->primero = ultimoBloqueEncontrado;
					guardandoFragmento = true;
				}
				bloquesConseguidos++;
				if (reservar) bitarray_set_bit(bitarray, bloque);
				if (bloquesConseguidos == bloquesNecesarios) {
					//si encontré los bloques que necesito guardo todo y salgo
					fragmento->ultimo = ultimoBloqueEncontrado;
					list_add(*bloques, fragmento);
					guardandoFragmento = false;
					bitarray_destroy(bitarray);
					goto fin;
				}
			} else if (guardandoFragmento) {
				//si encontré un bloque ocupado y estaba guardando, se cortó el fragmento
				fragmento->ultimo = ultimoBloqueEncontrado;
				list_add(*bloques, fragmento);
				guardandoFragmento = false;
			}
		}
		bitarray_destroy(bitarray);
	}

	fin:
	pthread_rwlock_unlock(&SD->locks.bitmapBloques);
}*/

bool EstaOcupado(uint32_t numero, uint32_t grupo, bool inodo, SuperDatos* SD) {
	//devuelve si un bloque o inodo (inodo=true) está ocupado
	char* bitmap;
	if (inodo) {
		pthread_rwlock_rdlock(&SD->locks.bitmapInodos);
		bitmap = LeerBloque(SD->descriptoresGB[grupo].inode_bitmap, true, SD);
	} else {
		pthread_rwlock_rdlock(&SD->locks.bitmapBloques);
		bitmap = LeerBloque(SD->descriptoresGB[grupo].block_bitmap, true, SD);
	}
	uint32_t bytePosicion = numero / 8;
	uint32_t bitEspecifico = numero % 8;
	bool ocupado = ValorBit(bitmap[bytePosicion], bitEspecifico);
	if (inodo) pthread_rwlock_unlock(&SD->locks.bitmapInodos);
	if (!inodo) pthread_rwlock_unlock(&SD->locks.bitmapBloques);
	return ocupado;
}

bool ValorBit(char byte, uint8_t bit) {
	//devuelve un bool (1 o 0) que corresponde
	//al bit pedido, de un byte
	return (byte>>bit)&0b00000001 ? 1 : 0;
}
