#include "main.h" //definiciones del filesystem y generales del rfs
#include "ext2_ubic.h" //funciones geométricas o de ubicación
#include "ext2_internal.h" //funciones internas (manejo de inodos, bitmaps, ...)
#include "ext2_operations.h" //funciones de alto nivel (leer archivos, carpetas...)
#include "request.h" //funciones de manejo de pedidos (threads, etc...) < FUNCIÓN DIOS
#include <sys/mman.h>

#define PROCESS_NAME "RFS"

static SuperDatos* SD;
/*
	FILE* partFile                  [Archivo de partición ext2]
	void* partMap                   [Archivo mapeado a memoria]
	uint32_t partSize               [Tamaño de la partición]
	Superblock* superbloque         [Estructura del superbloque]
	BlockgroupDesc* descriptoresGB  [Vector de descriptores de grupos de bloque]
	t_config* config                [Instancia de la configuración]
	t_log* logger                   [Instancia del logger]
	char ipCache[16]                [IP del servidor de memcached]
	bool cacheConectada             [Si está conectado a la cache o no]
	SckServidor* server             [Socket servidor en escucha]
	t_list* clientes                [Lista de clientes conectados]
	pthread_t* threads              [Vector de thread ID's del pool]
	sem_t* threadsSem               [Vector de semáforos para threads]
	Operacion* threadsOP            [Vector de operaciónes asignadas para cada thread]
	sem_t threadsLibresSem          [Semáforo para manejar los threads libres]
	bool* threadsLibres             [Vector con threads libres]
	Locks locks                     [Estructura para la sincronización]
	t_list* selExcluidos            [Lista de excluídos del select por estar realizando un write]
	sem_t selExcluidosSem           [Semáforo para selExcluidos]
	int32_t wakeEvent[2]            [Socketpair dummy para despertar del select al desexcluir]
	sem_t wakeEventSem              [Semáforo para que no se rompa al escribir en wakeEvent]
	uint32_t inotifyFD[2]           [File descriptors de inotify para config.txt]
	uint32_t retardo                [Retardo en milisegundos de las operaciones]
*/

void CerrarTodo(char* msg);
void LogPruebas(bool masPruebas);

int main (int argc, char** argv) {
	//crear archivo: $mkfs.ext2 -F  -I 128 -O none,sparse_super -b 1024 asa.ext2 81921
	uint32_t i;

	SD = malloc(sizeof(SuperDatos)); //Reservar superdatos

	//Abrir logs:
	SD->config = config_create(PATH_CONFIG); //La config es: VARIABLE=VALOR[enter]VARIABLE=VALOR...
	t_log_level level = LOG_LEVEL_TRACE;
	unlink(PATH_LOG);
	if (config_has_property(SD->config, "LOG_MIN_LEVEL"))
		level = config_get_int_value(SD->config, "LOG_MIN_LEVEL");
	SD->logger = log_create(PATH_LOG, PROCESS_NAME, true, level);

	//Abrir archivo de configuración:
	if (!ComprobarConfig(SD->config)) CerrarTodo("Error en el archivo de configuración");
	strcpy(SD->ipCache, config_get_string_value(SD->config, "IP_CACHE"));
	SD->retardo = config_get_int_value(SD->config, "RETARDO");
	SD->inotifyFD[0] = inotify_init(); //crea un hilo para actualizar cambios en el retardo
	SD->inotifyFD[1] = inotify_add_watch(SD->inotifyFD[0], PATH_CONFIG, IN_MODIFY);
	pthread_t inotify;
	CrearThread(&inotify, ActualizarRetardo, SD);

	//Testear cache:
	void* valor;
	SD->cacheConectada = true;
	memcached_return resCache = Cache_ObtenerValor(SD->ipCache, "test", NULL, (void*) &valor);
	if (resCache != MEMCACHED_NOTFOUND) {
		SD->cacheConectada = false;
		log_warning(SD->logger, "No se pudo conectar con la cache");
	}

	//Abrir particion:
	char* rutaParticion = argv[argc - 1];
	SD->partFile = fopen(rutaParticion, "rb+");
	if (SD->partFile == NULL) CerrarTodo("El archivo partición no existe");
	else log_info(SD->logger, "Partición abierta");

	//Obtener tamaño:
	fseek(SD->partFile, 0L, SEEK_END);
	SD->partSize = ftell(SD->partFile);
	fseek(SD->partFile, 0L, SEEK_SET);
	log_debug(SD->logger, "La partición pesa %d bytes", SD->partSize);

	//Mapear a memoria:
	SD->partMap = mmap(NULL, SD->partSize, PROT_READ | PROT_WRITE, MAP_SHARED, SD->partFile->_fileno, 0);
	if (SD->partMap == MAP_FAILED) CerrarTodo("Error al mapear el archivo");
	posix_madvise(SD->partMap, SD->partSize, POSIX_MADV_RANDOM);

	LeerSuperbloque(SD); //carga en memoria la estructura del superbloque
	LeerDescriptoresGB(SD); //carga en memoria descriptores de grupos de bloques

	//Abrir threads:
	SD->threads = malloc(sizeof(pthread_t) * config_get_int_value(SD->config, "THREADS_MAX"));
	SD->threadsSem = malloc(sizeof(sem_t) * config_get_int_value(SD->config, "THREADS_MAX"));
	SD->threadsLibres = malloc(sizeof(bool) * config_get_int_value(SD->config, "THREADS_MAX"));
	SD->threadsOP = malloc(sizeof(Operacion) * config_get_int_value(SD->config, "THREADS_MAX"));
	for (i=0; i < config_get_int_value(SD->config, "THREADS_MAX"); i++) {
		SD->threadsLibres[i] = false;
		sem_init(&SD->threadsSem[i], 0, 0);
		CrearThread(&SD->threads[i], &DIOS, SD);
	}
	sem_init(&SD->threadsLibresSem, 0, config_get_int_value(SD->config, "THREADS_MAX"));

	//Inicializar locks:
	SD->locks.descriptoresGB = malloc(sizeof(pthread_rwlock_t) * CantGrupos(SD));
	SD->locks.inodos = malloc(sizeof(pthread_rwlock_t) * (SD->superbloque->inodes + 1));
	for (i=0; i < CantGrupos(SD); i++)
		pthread_rwlock_init(&SD->locks.descriptoresGB[i], NULL);
	for (i=0; i < SD->superbloque->inodes + 1; i++)
		pthread_rwlock_init(&SD->locks.inodos[i], NULL);
	pthread_rwlock_init(&SD->locks.superbloque, NULL);
	pthread_rwlock_init(&SD->locks.bitmapBloques, NULL);
	pthread_rwlock_init(&SD->locks.bitmapInodos, NULL);
	sem_init(&SD->locks.freeInodes, 0, 1);
	sem_init(&SD->locks.freeBlocks, 0, 1);
	sem_init(&SD->locks.usedDirsCount, 0, 1);
	sem_init(&SD->selExcluidosSem, 0, 1);
	sem_init(&SD->wakeEventSem, 0, 1);

	LogPruebas(false); //guarda en el log algunas pruebas unitarias

	//Poner socket en escucha:
	SD->server = Socket_CrearServidor(NULL, config_get_int_value(SD->config, "PUERTO"));
	SD->server->maxConexiones = config_get_int_value(SD->config, "CONEX_MAX");
	SD->clientes = list_create();
	SD->selExcluidos = list_create();
	int res = Socket_EscucharConexiones(SD->server);
	if (res == -1) CerrarTodo("No se pudo escuchar conexiones");
	log_info(SD->logger, "Escuchando conexiones...");
	socketpair(AF_LOCAL, SOCK_STREAM, 0, SD->wakeEvent);
	SckCliente* clientesABorrar[SD->server->maxConexiones];
	//(porque no puedo borrarlos mientras recorro la lista)

	//Aceptar conexiones:
	while (1) {
		for (i=0; i < SD->server->maxConexiones; i++) clientesABorrar[i] = NULL;
		uint32_t cantABorrar = 0;

		//Preparar select:
		fd_set descRead;
		uint32_t descMax = PrepararSelect(&descRead, SD);

		//Select:
		select(descMax + 1, &descRead, NULL, NULL, NULL);
		void _manejadorDePedidos(void* cli) {
			SckCliente* cliente = (SckCliente*) cli;
			if (FD_ISSET(cliente->socket->descriptor, &descRead)) {
				//Recibir paquete:
				SckPaquete* pkg;
				pkg = Socket_Recibir(cliente);

				if (pkg == NULL) { //Se desconectó el cliente => Eliminarlo
					if (!cliente->usando) {
						clientesABorrar[cantABorrar] = cliente;
						cantABorrar++;
					}
					return;
				}

				sem_wait(&SD->threadsLibresSem); //espera a que haya threads libres
				cliente->usando = true;
				int threadAsignado = ThreadLibre(SD); //le asigna a uno el cliente y la operación
				SD->threadsLibres[threadAsignado] = false;
				SD->threadsOP[threadAsignado].cliente = cliente;
				SD->threadsOP[threadAsignado].paquete = *pkg;
				if (pkg->header.type == OP_WRITEFILE) ExcluirSelect(cliente, SD);
				sem_post(&SD->threadsSem[threadAsignado]);

				free(pkg);
			}
		}
		list_iterate(SD->clientes, _manejadorDePedidos);

		//Revisar si tengo nuevas conexiones:
		if (FD_ISSET(SD->server->socket->descriptor, &descRead)) {
			PonerCliente(SD);
		}

		//Borrar clientes que esten marcados para borrar:
		for (i=0; i < cantABorrar; i++) SacarCliente(clientesABorrar[i], SD);
	}

	return 0; //nunca se debería llegar acá
}

void CerrarTodo(char* msg) {
	//Cerrar archivo y salir:
	log_error(SD->logger, "%s", msg);
	exit(1);
}

void LogPruebas(bool masPruebas) {
	printf("-----------------------------------------------------\n");
	//Tests iniciales
	log_debug(SD->logger, "Acá tenemos el ext2 (revisión %d):", SD->superbloque->rev_level);
	log_debug(SD->logger, "Cantidad de inodos: %d", SD->superbloque->inodes);
	log_debug(SD->logger, "Inodos por grupo: %d", SD->superbloque->inodes_per_group);
	log_debug(SD->logger, "Cantidad de bloques: %d", SD->superbloque->blocks);
	log_debug(SD->logger, "Bloques por grupo: %d", SD->superbloque->blocks_per_group);
	log_debug(SD->logger, "Inodos libres: %d", SD->superbloque->free_inodes);
	log_debug(SD->logger, "Bloques libres: %d", SD->superbloque->free_blocks);
	log_debug(SD->logger, "Primer bloque datos: %d", SD->superbloque->first_data_block);
	log_debug(SD->logger, "Veces montado: %d", SD->superbloque->mount_count);
	log_debug(SD->logger, "Log2 del tamaño de bloque -10: %d", SD->superbloque->log_block_size);
	log_debug(SD->logger, "Numerito magico: %d", SD->superbloque->magic);
	if (SD->superbloque->magic == EXT2_SUPER_MAGIC)
		log_debug(SD->logger, "EL NÚMERO MÁGICO COINCIDE!!!");
	log_debug(SD->logger, "Tamaño de bloque POSTA: %d", BlockSize(SD));
	log_debug(SD->logger, "Tengo %d blockgroups", CantGrupos(SD));
	int i;
	for (i=0; i < CantGrupos(SD); i++) {
		log_debug(SD->logger, "El blockgroup %d tiene %d inodos libres y %d bloques libres", i, SD->descriptoresGB[i].free_inodes_count, SD->descriptoresGB[i].free_blocks_count);
	}
	log_debug(SD->logger, "Tamaño tabla de inodos: %d", InodeTableSize(SD) / BlockSize(SD));
	printf("-----------------------------------------------------\n");
	if (!masPruebas) return;
	//Probar leer inodo
	Inode* inodo;
	char* contenido;
	/*if (EstaOcupado(11, 0, true, SD)) {
		inodo = Inodo(12, SD);
		contenido = LeerInodo(inodo, 12, SD);
		printf("Inodo (%d bytes, empieza en bloque num %d):\n%s", inodo->size, inodo->blocks[0], contenido);
		free (contenido);
	}*/
	printf("-----------------------------------------------------\n");
	//Prueba de sparse_superblock
	for (i=0; i < CantGrupos(SD); i++) {
		printf("El grupo %d tiene %d bloques de datos y %d libres\n", i, CantBloquesDatos(i, SD), SD->descriptoresGB[i].free_blocks_count);
	}
	printf("-----------------------------------------------------\n");
	//Probar bitmap inodos con el grupo 0
	for (i=0; i < 30; i++) { //o <= 30
		printf("El inodo %d está ocupado: %d.\n", InodoAbsoluto(i, 0, SD), EstaOcupado(i, 0, true, SD));
	}
	printf("-----------------------------------------------------\n");
	//Probar bitmap bloques con el grupo 1
	for (i=BloqueRelativo(PosBloquesDatos(1, SD), SD); i < BloqueRelativo(PosBloquesDatos(1, SD), SD) + 10; i++) {
		printf("El bloque %d que absoluto es %d está ocupado: %d.\n", i, BloqueAbsoluto(i, 1, SD), EstaOcupado(i, 1, false, SD));
	}
	printf("-----------------------------------------------------\n");
	//Buscar primer inodo libre
	printf("El 1er inodo libre es el %d\n", InodoLibre(false, SD));
	printf("-----------------------------------------------------\n");
	//Mostrar bloques libres
	/*printf("El ultimo bloque de datos del grupo 0 es %d\n", PosBloquesDatos(0, SD) + CantBloquesDatos(0, SD) - 1);
	void _mostrarFragmento(void* fragm) {
		Fragmento* fragmento = (Fragmento*) fragm;
		printf("%d-%d\n", fragmento->primero, fragmento->ultimo);
	}
	void _destructor(void* fragmento) { free(fragmento); }
	printf("Los bloques para alocar un archivo de 20000 bytes son: \n");
	t_list* bloquesLibres = NULL;
	BloquesLibres(20000, &bloquesLibres, false, SD);
	list_iterate(bloquesLibres, _mostrarFragmento);
	list_destroy_and_destroy_elements(bloquesLibres, _destructor);
	printf("Los bloques para alocar un archivo de 65500000 bytes son: \n");
	bloquesLibres = NULL;
	BloquesLibres(65500000, &bloquesLibres, false, SD);
	if (bloquesLibres == NULL) {
		printf("No hay espacio \n");
	} else {
		list_iterate(bloquesLibres, _mostrarFragmento);
		list_destroy_and_destroy_elements(bloquesLibres, _destructor);
	}*/
	printf("-----------------------------------------------------\n");
	//Probar leer archivo indirecto PruebaIndirec.txt
	uint32_t inodoNum;
	if ((inodo = InodoDePath("/PruebaIndirec.txt", &inodoNum, SD)) != NULL) {
		contenido = LeerInodo(inodo, inodoNum, SD);
		char* enString = malloc(inodo->size + 1);
		memcpy(enString, contenido, inodo->size);
		enString[inodo->size] = '\0';
		printf("PruebaIndirec.txt (%d bytes, indirecto, empieza en bloque num %d):\n%s", inodo->size, inodo->blocks[0], enString);
		free(contenido);
		free(enString);
	}
	printf("-----------------------------------------------------\n");
	//Probar obtener el inodo de un path
	inodo = InodoDePath("/piola/guacho/aca/jojo.txt", NULL, SD);
	if (inodo != NULL) printf("Lei el inodo del path, atime: %d, size: %d\n", inodo->atime, inodo->size);
	printf("-----------------------------------------------------\n");
	//Probar leer un path un poco más jodido
	inodo = InodoDePath("/sarasa/carpetita1/../carpetita2/archivo.txt", NULL, SD);
	if (inodo != NULL) printf("Encontre el inodo: %d\n", inodo->atime);
	printf("-----------------------------------------------------\n");
	//Probar la función MaxDireccion
	for (i=-1; i < NIVELES_INDIRECCION; i++) {
		printf("Con indirección nivel %d se pueden direccionar hasta %f bytes y %f bloques\n", i, MaxDireccion(i, SD), MaxDireccion(i, SD) / BlockSize(SD));
	}
	printf("-----------------------------------------------------\n");
	//Probar la función ByteInicialBloqueIndirec
	printf("El byte inicial del bloque 1 del nivel 2 es: %d\n", ByteInicialBloqueIndirec(2, 1, SD));
	printf("-----------------------------------------------------\n");
	//Probar separar una carpeta de un nombre
	char* nombre; char* carpeta;
	SepararCarpeta_Nombre("/carpeta/", &nombre, &carpeta);
	printf("El nombre: %s\nLa carpeta: %s\n", nombre, carpeta);
	free(nombre); free(carpeta);
	printf("-----------------------------------------------------\n");
	//Probar función BytesNecesariosPara
	printf("Archivo-BloquesNecesarios:\n");
	uint32_t bytes[] = { MaxDireccion(0, SD) + 1 };
	for (i=0; i < 1; i++) printf("%d bytes: %d\n", bytes[i], BloquesNecesariosPara(bytes[i], SD));
	printf("-----------------------------------------------------\n");
	//Probar recorrer el inodo root
	DebugDir(2, SD);
	printf("-----------------------------------------------------\n");
}
