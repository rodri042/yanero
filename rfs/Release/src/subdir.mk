################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ext2_int_bitmap.c \
../src/ext2_int_direntry.c \
../src/ext2_internal.c \
../src/ext2_operations.c \
../src/ext2_ubic.c \
../src/main.c \
../src/request.c 

OBJS += \
./src/ext2_int_bitmap.o \
./src/ext2_int_direntry.o \
./src/ext2_internal.o \
./src/ext2_operations.o \
./src/ext2_ubic.o \
./src/main.o \
./src/request.o 

C_DEPS += \
./src/ext2_int_bitmap.d \
./src/ext2_int_direntry.d \
./src/ext2_internal.d \
./src/ext2_operations.d \
./src/ext2_ubic.d \
./src/main.d \
./src/request.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"../../commons-library/src" -I"../../tads" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


