#ifndef DUMMY_ENGINE_C_
#define DUMMY_ENGINE_C_

	#include "commons/log.h"
	#include "commons/config.h"
	#include "commons/temporal.h"
	#include <mcheck.h>
	#include <sys/mman.h>
	#include <semaphore.h>
	#include <pthread.h>
 	#include <unistd.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <signal.h>
	#include <stdint.h>
	#include <stdbool.h>
	#include <memcached/engine.h>
	#include <memcached/util.h>
	#include <memcached/visibility.h>

	#define MAX_KEY 43 //1 caracter 'c' o 's' + 1 caracter 'g' o 'r' + 40 para la ruta + 1 para el \0

	//Fecha actual guardada en cada nodo
	 typedef struct {
		uint32_t anomesdia;
		uint32_t horaminutosegundo;
	 } Fecha;

	//Esta es una estructura utilizada para almacenar y respresentar un elemento
    //almacenado en la cache. Cada nodo representa una partición.
	typedef struct Nodo {
	   char key[MAX_KEY]; //Clave (máx MAX_KEY bytes)
	   void *data; //Puntero a la parte de la cache donde empieza la partición (por ende el dato)
	   size_t ndata; //Longitud del dato
	   size_t ntotal; //Longitud total de la partición
	   Fecha ultimoAcceso; //Fecha de último acceso del dato
	   bool ocupado; //Indica si la partición está ocupada o libre
	   bool usado; //Indica si el nodo representa una partición o solo está reservado (pero libre)
	   size_t numeroNodo; //Contador de número de nodo, sirve para el FIFO
	   struct Nodo *sgte; //Puntero al nodo siguiente
	   //Uso interno de memcached:
	   int flags;
	   rel_time_t exptime;
	   struct Nodo *hijo1;
	   struct Nodo *hijo2;
	   struct Nodo *padre;
	} Nodo;

	typedef struct {
		bool algoritmoAloc;
		bool algoritmoSet;
		bool algoritmoVict;
		uint32_t nodosCount;
		uint32_t nodosCant;
		uint32_t nodosMax;
		uint32_t bytesLibres;
		size_t partMinSize;
		size_t partMaxSize;
		int32_t frecCompactacion;
		pthread_rwlock_t rwlock;
		t_config* config;
		t_log* logger;
		bool log_operaciones;
		bool log_compactacion;
		bool log_reemplazo;
		FILE* dump;
	} Global;

	//Esta es una estructura custom que utilizo para almacenar
	//la configuración que me pasa memcached
	typedef struct {
	   size_t cacheMaxSize;
	   size_t partMinSize;
	   size_t partMaxSize;
	} Config;

	/*
	 * Esta es la estructura que utilizo para representar el engine, para que memcached pueda manipularla el
	 * primer campo de esta tiene que ser ENGINE_HANDLE_V1 engine; el resto de los campos pueden ser los que querramos
	 */
	typedef struct {
		ENGINE_HANDLE_V1 engine;
		GET_SERVER_API get_server_api;
		Config config;
	} Motor;

	// Esta funcion es escencial ya que es la que busca memcached para ejecutar cuando levanta la shared library:
	//(es la primera que se ejecuta)
	MEMCACHED_PUBLIC_API ENGINE_ERROR_CODE create_instance(uint64_t interface, GET_SERVER_API get_server_api, ENGINE_HANDLE **handle);

	void CrearPrimerParticion();
	void ArreglarTamanio (Global* global);
	bool EsPotenciaDeDos(size_t valor);
#endif /* DUMMY_ENGINE_C_ */
