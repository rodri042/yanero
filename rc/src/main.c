#include "main.h"
#include "lista.h"
#include <memcached/config_parser.h>

#define PROCESS_NAME "RemoteCache"
//>>>Para guardar el mtrace, MALLOC_TRACE del proyecto memcached-1.6
//>>>tiene que estar seteada en ${workspace_loc:/rc/mtrace.txt}
//>>>(Project -> Run/Debug Settings -> Enviroment)
//>>Argumentos: -vv -p 11212 -E ../rc/Debug/librc.so -n 10 -I 1025
//(min: 10b, max: 1025b)

//Funciones necesarias para el engine:
static ENGINE_ERROR_CODE InicializarCache(ENGINE_HANDLE* , const char* config_str);
static void DestruirEngine(ENGINE_HANDLE*, const bool force);
static ENGINE_ERROR_CODE AlocarItem(ENGINE_HANDLE* , const void* cookie, item **item, const void* key,
											const size_t nkey, const size_t nbytes, const int flags, const rel_time_t exptime);
static bool GetInfo(ENGINE_HANDLE *, const void *cookie, const item* item, item_info *item_info);
static ENGINE_ERROR_CODE Store(ENGINE_HANDLE* , const void *cookie, item* item, uint64_t *cas,
											ENGINE_STORE_OPERATION operation, uint16_t vbucket);
static void DesalocarItem(ENGINE_HANDLE* , const void *cookie, item* item);
static ENGINE_ERROR_CODE Get(ENGINE_HANDLE* , const void* cookie, item** item, const void* key, const int nkey, uint16_t vbucket);
static ENGINE_ERROR_CODE Flush(ENGINE_HANDLE* , const void* cookie, time_t when);
static ENGINE_ERROR_CODE Delete(ENGINE_HANDLE* , const void* cookie, const void* key, const size_t nkey, uint64_t cas, uint16_t vbucket);

//Funciones dummy (innecesarias):
static const engine_info* InfoMotor(ENGINE_HANDLE* );
static ENGINE_ERROR_CODE dummy_ng_get_stats(ENGINE_HANDLE* , const void* cookie, const char* stat_key, int nkey, ADD_STAT add_stat);
static void dummy_ng_reset_stats(ENGINE_HANDLE* , const void *cookie);
static ENGINE_ERROR_CODE dummy_ng_unknown_command(ENGINE_HANDLE* , const void* cookie, protocol_binary_request_header *request, ADD_RESPONSE response);
static void dummy_ng_item_set_cas(ENGINE_HANDLE *, const void *cookie, item* item, uint64_t val);

//Esta función es la que va a ser llamada cuando se reciba una signal:
void DumpCache(int signal);

/*------------------------------------------------------*/
static void* memCache; //Flujo de bytes que forman la cache
static Nodo* memLista; //Memoria para la lista
static Nodo* lista = NULL; //Puntero a la lista
static Global global;
/*  Estructura que contiene:
	bool algoritmoAloc        [0 = Particiones dinámicas; 1 = Buddy System]
	bool algoritmoSet         [0 = First Fit; 1 = Best Fit]
	bool algoritmoVict        [0 = FIFO; 1 = LRU]
	uint32_t nodosCount       [Nodos agregados desde el inicio del proceso]
	uint32_t nodosCant        [Cantidad real de particiones]
	uint32_t nodosMax         [Cantidad máxima de particiones]
	uint32_t bytesLibres      [Bytes libres en la cache]
	size_t partMinSize        [Tamaño mínimo de partición]
	size_t partMaxSize        [Tamaño máximo de partición]
	uint32_t frecCompactacion [Número de intentos fallidos hasta compactar]
	pthread_rwlock_t rwlock   [Read/write lock]
	t_config* config          [Instancia de la configuración]
	t_log* logger             [Instancia del logger]
	FILE* dump                [Archivo del dump]
*/
/*------------------------------------------------------*/

//A esta función la llama memcached para instanciar el motor:
MEMCACHED_PUBLIC_API ENGINE_ERROR_CODE create_instance(uint64_t interface, GET_SERVER_API get_server_api, ENGINE_HANDLE **handle) {
	mtrace();
	if (interface == 0) {
		return ENGINE_ENOTSUP;
	}
	Motor *engine = calloc(1, sizeof(Motor)); //<<<Declaración de engine: instancia del motor
	if (engine == NULL) {
		return ENGINE_ENOMEM;
	}

	//Avisar a memcached que estamos programando la versión 1 de la API (interfaz) del motor:
	engine->engine.interface.interface = 1;

	//Avisar al motor qué función hace cada operación de la cache:
	engine->engine.initialize = InicializarCache;
	engine->engine.destroy = DestruirEngine;
	engine->engine.get_info = InfoMotor;
	engine->engine.allocate = AlocarItem;
	engine->engine.remove = Delete;
	engine->engine.release = DesalocarItem;
	engine->engine.get = Get;
	engine->engine.get_stats = dummy_ng_get_stats;
	engine->engine.reset_stats = dummy_ng_reset_stats;
	engine->engine.store = Store;
	engine->engine.flush = Flush;
	engine->engine.unknown_command = dummy_ng_unknown_command;
	engine->engine.item_set_cas = dummy_ng_item_set_cas;
	engine->engine.get_item_info = GetInfo;
	engine->get_server_api = get_server_api;

	//Magia oscura happens here -.-
	*handle = (ENGINE_HANDLE*) engine;

	return ENGINE_SUCCESS;
}

//Esta función se llama inmediatamente despues del create_instance y sirve para inicializar:
static ENGINE_ERROR_CODE InicializarCache(ENGINE_HANDLE* handle, const char* config_str) {
	//En todas las funciones vamos a recibir un ENGINE_HANDLE* handle, pero en realidad
	//el puntero handler es nuestra estructura Motor:
	Motor* engine = (Motor*)handle;

	//En la variable config_str nos llega la configuración en el formato:
	//cache_size=1024;chunk_size=48; etc ...
	//Esto parsea la configuración y la guarda en una estructura:
	if (config_str != NULL) {
		struct config_item items[] = {
		 { .key = "cache_size",
		   .datatype = DT_SIZE,
		   .value.dt_size = &engine->config.cacheMaxSize}, //items[0] [!]No lo uso
		 { .key = "chunk_size",
		   .datatype = DT_SIZE,
		   .value.dt_size = &engine->config.partMinSize }, //items[1]
		 { .key = "item_size_max",
		   .datatype = DT_SIZE,
		   .value.dt_size = &engine->config.partMaxSize }, //items[2]
		 { .key = NULL}
		};
		parse_config(config_str, items, NULL);
	}

	//Abrir archivos de configuración y logs:
	global.config = config_create("../rc/config.txt"); //La config es: VARIABLE=VALOR[enter]VARIABLE=VALOR...
	char* parametros[] = {"LOG_OPERACION", //0 o 1
						  "LOG_COMPACTACION", //0 o 1
						  "LOG_REEMPLAZO", //0 o 1
						  "LOG_MIN_LEVEL", //0 a 4
						  "ALGORITMO_ALOC", //0 o 1
						  "ALGORITMO_SET", //0 o 1
						  "ALGORITMO_VICT", //0 o 1
						  "FREC_COMPACTACION"
						 };
	t_log_level level = LOG_LEVEL_TRACE;
	unlink("../rc/log.txt");
	if (config_has_property(global.config, "LOG_MIN_LEVEL"))
		level = config_get_int_value(global.config, "LOG_MIN_LEVEL");
	global.logger = log_create("../rc/log.txt", PROCESS_NAME, true, level);
	int i;
	for(i=0; i < 8; i++) {
		if (!config_has_property(global.config, parametros[i])) {
			log_error(global.logger, "Error en el archivo de configuración");
			exit(1);
		}
	}
	global.log_operaciones = config_get_int_value(global.config, "LOG_OPERACION");
	global.log_compactacion = config_get_int_value(global.config, "LOG_COMPACTACION");
	global.log_reemplazo = config_get_int_value(global.config, "LOG_REEMPLAZO");
	global.algoritmoAloc = config_get_int_value(global.config, "ALGORITMO_ALOC");
	global.algoritmoSet = config_get_int_value(global.config, "ALGORITMO_SET");
	global.algoritmoVict = config_get_int_value(global.config, "ALGORITMO_VICT");
	global.frecCompactacion = config_get_int_value(global.config, "FREC_COMPACTACION");

	//Config de los parámetros
	global.partMinSize = engine->config.partMinSize;
	global.partMaxSize = engine->config.partMaxSize;
	//global.partMaxSize = 32; //soo hardcore
	//global.partMinSize = 4;
	if (global.algoritmoAloc == 0) global.nodosMax = global.partMaxSize / global.partMinSize;
	else global.nodosMax = ((global.partMaxSize / global.partMinSize) * 2) - 1;

	if (global.algoritmoAloc == 0) log_info(global.logger, "Cache andando! Particiones dinámicas mode ON");
	else log_info(global.logger, "Cache andando! Buddy System mode ON");

	if (global.algoritmoAloc == 1 && !EsPotenciaDeDos(global.partMaxSize))
		ArreglarTamanio(&global); //si es Buddy el tamaño debe ser potencia de 2

	//Reservar cache memoria para datos y estructuras:
	memCache = malloc(global.partMaxSize);
	memLista = malloc(sizeof(Nodo) * global.nodosMax);
	mlock(memCache, engine->config.partMaxSize);
	mlock(memLista, sizeof(Nodo) * global.nodosMax);
	CrearPrimerParticion();

	//Abrir archivo para el dump:
	global.dump = fopen("../rc/dump.txt", "w+");

	//Inicializo semaforo
	pthread_rwlock_init(&global.rwlock, NULL);
	ImprimirParticiones(lista, &global); //Muestra la cache para comprobar

	//Registro la SIGUSR1. El registro de signals debe ser realizado acá:
	signal(SIGUSR1, DumpCache);

	return ENGINE_SUCCESS;
}

//Crear primer nodo que represente la única partición vacía:
void CrearPrimerParticion() {
	Nodo* primerNodo = NuevoNodo(memLista, &global);
	primerNodo->padre = NULL;
	primerNodo->data = memCache; //la partición libre empieza al principio de la cache
	primerNodo->ntotal = global.partMaxSize; //y mide todo lo que mide la cache
	primerNodo->sgte = NULL;
	primerNodo->numeroNodo = 1;
	lista = primerNodo;
	global.nodosCount = 1;
	global.nodosCant = 1;
	global.bytesLibres = global.partMaxSize;
}

//Esta función es la que se llama cuando el engine es destruido:
static void DestruirEngine(ENGINE_HANDLE* handle, const bool force) {
	/*free(handle); //el mtrace() tira lineas por esto y me asusta T__________T
	free(memCache); //por las dudas no libero nada =D
	free(memLista);
	config_destroy(global.config);
	log_destroy(global.logger);*/
}

//Esto retorna algo de información la cual se muestra en la consola:
static const engine_info* InfoMotor(ENGINE_HANDLE* handle) {
	static engine_info info = {
		.description = "Noentiendo 64 Engine",
		.num_features = 1,
	};
	return &info;
}

//Crea el item en la lista (solo guarda los metadatos):
static ENGINE_ERROR_CODE AlocarItem(ENGINE_HANDLE *handler, const void* cookie, item **item, const void* key,
								const size_t nkey, const size_t nbytes, const int flags, const rel_time_t exptime) {
	char strkey[nkey + 1];
	VoidAStr(strkey, key, nkey);

	if (nbytes > global.partMaxSize) {
		log_debug(global.logger, "No se guardó la clave %s de %d porque es > a %d", strkey, nbytes, global.partMaxSize);
		return ENGINE_ENOMEM;
	}

	if (global.log_operaciones) log_debug(global.logger, "Se agrega la clave %s", strkey);

	pthread_rwlock_wrlock(&global.rwlock);
	if (ExisteClave(lista, strkey)) { //si la key ya existe la borra
		Nodo* nodoABorrar = ListaBuscar(lista, strkey);
		if (nodoABorrar != NULL) {
			if (global.algoritmoAloc == 0)
				InicializNodo(nodoABorrar, &global);
			else
				BorrarBuddy(nodoABorrar, &global);
		}
	}

	Nodo* it = ListaAgregar(&lista, nbytes, strkey, memLista, memCache, &global);
	if (it == NULL) {
		pthread_rwlock_unlock(&global.rwlock);
		return ENGINE_FAILED; //error RARO (no debería pasar)
	}

	it->flags = flags;
	it->exptime = exptime;
	*item = it;
	pthread_rwlock_unlock(&global.rwlock);
	//una vez creada la partición y está todo listo para que venga el dato
	//memcached lo guarda en el campo data, y llama a la función Store

	return ENGINE_SUCCESS;
}

//Esta función lo que hace es mapear el item_info el cual es el tipo que memcached sabe manejar con el tipo de item
//nuestro el cual es el que nosotros manejamos. Se llama antes de llamar a Store.
static bool GetInfo(ENGINE_HANDLE *handler, const void *cookie, const item* item, item_info *item_info) {
	// casteamos de item*, el cual es la forma generica en la cual memcached trata a nuestro tipo de item, al tipo
	// correspondiente que nosotros utilizamos
	Nodo *it = (Nodo*)item;

	if (item_info->nvalue < 1) {
	  return false;
	}

	item_info->cas = 0; 		/* Not supported */
	item_info->clsid = 0; 		/* Not supported */
	item_info->exptime = it->exptime;
	item_info->flags = it->flags;
	item_info->key = it->key;
	item_info->nkey = strlen(it->key) + 1;
	item_info->nbytes = it->ndata; 	/* Total length of the items data */
	item_info->nvalue = 1; 			/* Number of fragments used ( Default ) */
	item_info->value[0].iov_base = it->data; /* Hacemos apuntar item_info al comienzo de la info */
	item_info->value[0].iov_len = it->ndata; /* Le seteamos al item_info el tamaño de la información */

	return true;
}

//Esta función se llama cuando memcached recibe un set. La variable operation nos indica el tipo. Estos deben ser tratados indistintamente:
static ENGINE_ERROR_CODE Store(ENGINE_HANDLE *handle, const void *cookie, item* item, uint64_t *cas, ENGINE_STORE_OPERATION operation, uint16_t vbucket) {
	printf("[Store]\n");

	//Nodo *it = (Nodo*)item;
	*cas = 0;

	//ImprimirParticiones(lista, &global); //Muestra la cache para comprobar

	return ENGINE_SUCCESS;
}

//Esta funcion es invocada cuando memcached recibe el comando get:
static ENGINE_ERROR_CODE Get(ENGINE_HANDLE *handle, const void* cookie, item** item, const void* key, const int nkey, uint16_t vbucket) {
	pthread_rwlock_rdlock(&global.rwlock);

	//transformamos la variable key a string
	char strkey[nkey + 1];
	VoidAStr(strkey, key, nkey);

	if (strcmp(strkey, ">>dump<<") == 0) {
		ImprimirParticiones(lista, &global); //Muestra la cache
		pthread_rwlock_unlock(&global.rwlock);
		return ENGINE_KEY_ENOENT;
	}

	if (global.log_operaciones) log_debug(global.logger, "Se solicita la clave %s", strkey);

	//buscamos y obtenemos el item

	Nodo *it = ListaBuscar(lista, strkey);

	if ( it == NULL ) {
		pthread_rwlock_unlock(&global.rwlock);
		return ENGINE_KEY_ENOENT;
	}

	//retornamos el item
	*item = it;

	//ImprimirParticiones(lista, &global); //Muestra la cache para comprobar
	pthread_rwlock_unlock(&global.rwlock);
	return ENGINE_SUCCESS;
}

//Esta función se llama cuando memcached recibe un flush_all:
static ENGINE_ERROR_CODE Flush(ENGINE_HANDLE* handle, const void* cookie, time_t when) {
	//limpio toda la cache
	pthread_rwlock_wrlock(&global.rwlock);
	ListaVaciar(&lista, &global);
	//ImprimirParticiones(lista, &global); //Muestra la cache para comprobar
	if (global.log_operaciones) log_debug(global.logger, "Se borra todo :(");
	pthread_rwlock_unlock(&global.rwlock);
	return ENGINE_SUCCESS;

}

//Esta función se llama cuando memcached recibe un delete:
static ENGINE_ERROR_CODE Delete(ENGINE_HANDLE* handle, const void* cookie, const void* key, const size_t nkey, uint64_t cas, uint16_t vbucket) {
	pthread_rwlock_wrlock(&global.rwlock);
	char strkey[nkey + 1];
	VoidAStr(strkey, key, nkey);

	Nodo* nodoABorrar = ListaBuscar(lista, strkey);

	if (nodoABorrar != NULL) {
		if (global.log_operaciones) log_debug(global.logger, "Se borra la clave %s", strkey);
		if (global.algoritmoAloc == 0) InicializNodo(nodoABorrar, &global);
		else BorrarBuddy(nodoABorrar, &global);
		//ImprimirParticiones(lista, &global); //Muestra la cache para comprobar
		pthread_rwlock_unlock(&global.rwlock);
		return ENGINE_SUCCESS;
	} else {
		pthread_rwlock_unlock(&global.rwlock);
		return ENGINE_KEY_ENOENT;
	}
}

//Funciones dummy (innecesarias):
static ENGINE_ERROR_CODE dummy_ng_get_stats(ENGINE_HANDLE* handle, const void* cookie, const char* stat_key, int nkey, ADD_STAT add_stat) {
	return ENGINE_SUCCESS; }
static void dummy_ng_reset_stats(ENGINE_HANDLE* handle, const void *cookie) { }
static ENGINE_ERROR_CODE dummy_ng_unknown_command(ENGINE_HANDLE* handle, const void* cookie, protocol_binary_request_header *request, ADD_RESPONSE response) {
	return ENGINE_ENOTSUP; }
static void dummy_ng_item_set_cas(ENGINE_HANDLE *handle, const void *cookie, item* item, uint64_t val) { }
static void DesalocarItem(ENGINE_HANDLE *handler, const void *cookie, item* item) { }

//Handler de la SIGUSR1:
void DumpCache(int signal) {
	log_info(global.logger, "Se realizó un DUMP de la caché");
	DumpDeLaCache(lista, &global);
}

bool EsPotenciaDeDos(size_t valor){
	while (valor > 1) {
		if (valor % 2 > 0) return false;
		valor = valor / 2;
	}
	return true;
}

void ArreglarTamanio (Global* global){
	while (!EsPotenciaDeDos(global->partMaxSize)) {
		global->partMaxSize++;
	}
	log_warning(global->logger, "Se ajustó el tamaño de la caché a %d porque no era potencia de 2", global->partMaxSize);
}
