/*
 * Copyright (C) 2012 Sistemas Operativos - UTN FRBA. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "temporal.h"
#include "error.h"

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/timeb.h>
#include <string.h>

/**
 * @NAME: temporal_get_string_time
 * @DESC: Guarda en el string pasado la hora,
 * con el siguiente formato: hh:mm:ss:mmmm
 */
void temporal_get_string_time(char** output) {
	struct timeval  tv;
	struct tm      *tm;
	struct tm tmp;
	gettimeofday(&tv, NULL);
	tm = localtime_r(&tv.tv_sec, &tmp);
	sprintf(*output, "%02d:%02d:%02d:%04d", tm->tm_hour, tm->tm_min, tm->tm_sec, (int) tv.tv_usec / 1000);
}
