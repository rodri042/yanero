#include "main.h"
#include "lista.h"


/*        LA PROLEÑA?
░░░░░░░░███████████████░░░░░░░░
░░░░░█████████████████████░░░░░
░░░████████████████████████░░░░
░░██████████████████████████░░░
█████████████████████████████░░
████████░░░░░░░░░▀███████████░░
███░░░░░░░░░░░░░░░███████████░░
██░░░░░░░░░░░░░░░░████████████░
██░░░░░▄▄░░░░░░░░░░░███████░░█░
█░░██░░███████▄░░░░░░█████░░░░█
█░██░░░░░░░░▀▀██░░░░░███░░░█░░█
███░░░░░░░▄██▄░░░░░░░░░░░░█░░░█
██░▄█▀▀█░░░░░░░░░░░░░░░░░░█▄░░█
░██░░░░█░░░░░░░░░░░░░░░░░░░░░░█
░░█░░░░█░░░░░░░░░░░░░░░░░░░███░
░█░░░░█░░░░▄ ░░░░░░░░░░░░░░█░█░
░█░░░██▄░▄▀██░░░░░█░░░░░░░█░█░░
░█░░░░░░░░░░░░░░░░░█░░░░░░█░█░░
░░█░░░░░░▄▄▄▄▄▄░░░░█░░░░░░██░░░
░░░█░░██▀▀░▄▄▄▄█░░█░░░░░░░██░░░
░░░░█░░████████▀░░█░░░░░░░██░░░
░░░░░█░░██▄▄▄▄▀░░█░░░░░░░█░░█░░
░░░░░░█░░░░░░░░░█░░░░░░░█░░░█░░
░░░░░░█░░░░░░░░░░░░░░░░█░░░░░█░
░░░░░░█░░░░░░░░█░░░░░░█░░░░░░░░
░░░░░░░████████░░░░░░░░░░░░░░░░
 */


Nodo* ListaAgregar(Nodo** listaPtr, size_t ndata, char* key, Nodo* memLista, void* memCache, Global* global) {
	if (*listaPtr == NULL) return NULL;
	Nodo* lista = *listaPtr;
	Nodo* definitivo; //partición en la que se va a guardar el dato
	size_t fixedSize; //tamaño ajustado al mínimo de la partición
	if (ndata < global->partMinSize) {
		fixedSize = global->partMinSize;
	} else {
		fixedSize = ndata;
	}

	busqueda:
	if (global->algoritmoAloc == 0) { //Particiones dinámicas con compactación
		uint32_t intentosLlevados = 0;

		if (global->frecCompactacion == 0) {
			Compactar(listaPtr, memLista, global);
			lista = *listaPtr;
			intentosLlevados = 0;
		}

		//Asignar particion a usar y devolver victimas:
		Nodo* first = NULL; //Victimas
		Nodo* leastRecentlyUsed = NULL; //Victimas
		definitivo = ElegirParticion(lista, fixedSize, &first, &leastRecentlyUsed, global);

		bool nodosLlenos = global->nodosCant == global->nodosMax;
		bool entroJustito;
		if (definitivo != NULL) entroJustito = (definitivo->ntotal - fixedSize) == 0;
		if (definitivo == NULL || (definitivo != NULL && nodosLlenos && !entroJustito)) {
			//búsqueda fallida
			intentosLlevados++;

			bool todasEstanLibres = global->bytesLibres == global->partMaxSize;
			bool ningunaEstaOcupada = PrimerNodoOcupado(lista) == NULL;
			if (intentosLlevados == global->frecCompactacion || (global->frecCompactacion == -1 && todasEstanLibres) || ningunaEstaOcupada) {
				//un poco redundante no? igual, falta poco para la entrega y da miedito cambiarlo >.<
				if (global->log_compactacion) log_info(global->logger, "Se compacta en particiones dinámicas");
				Compactar(listaPtr, memLista, global);
				lista = *listaPtr;
				intentosLlevados = 0;
				goto busqueda;
			}

			BorrarVictima(first, leastRecentlyUsed, global);
			goto busqueda;
		}

		Nodo* nodoSiguiente = NULL;
		entroJustito = (definitivo->ntotal - fixedSize) == 0;
		if (!entroJustito) {
			//Crea una partición al lado de la que se usará para guardar,
			//con el espacio libre que sobre...
			nodoSiguiente = NuevoNodo(memLista, global);
			nodoSiguiente->data = definitivo->data + fixedSize;
			//(la nueva partición empieza donde empezaba la otra + la longitud del nuevo dato)
			nodoSiguiente->ntotal = definitivo->ntotal - fixedSize;
			//(la nueva partición ocupa lo que ocupada la otra - la longitud del nuevo dato)
			nodoSiguiente->numeroNodo = definitivo->numeroNodo;
			nodoSiguiente->sgte = definitivo->sgte;
		}

		//Asigna a la primera partición el nuevo dato:
		strcpy(definitivo->key, key);
		definitivo->ndata = ndata;
		definitivo->ntotal = fixedSize;
		definitivo->ocupado = true;
		definitivo->numeroNodo = ++global->nodosCount;
		definitivo->ultimoAcceso = ObtenerFechaActual();
		if (!entroJustito) {
			definitivo->sgte = nodoSiguiente;
		}
		global->bytesLibres -= fixedSize;
	} else { //Buddy System
		//Asignar particion a usar y devolver victimas:
		Nodo* first = NULL; //Victimas
		Nodo* leastRecentlyUsed = NULL; //Victimas
		definitivo = ElegirParticion(lista, fixedSize, &first, &leastRecentlyUsed, global);
		if (definitivo == NULL){
			//búsqueda fallida
			BorrarVictima(first, leastRecentlyUsed, global);
			goto busqueda;
		} //definitivo puede que se parta en dos

		while (definitivo->ntotal/2 >= ndata && definitivo->ntotal/2 >= global->partMinSize ) {
			//si el dato a ingresar es mayor que la mitad del tamaño de la partición, divide en dos la partición
			Nodo* padre = definitivo;
			Nodo* hijo1 = NuevoNodo(memLista, global);
			InicializNodo(hijo1, global);
			Nodo* hijo2 = NuevoNodo(memLista, global);
			InicializNodo(hijo2, global);

			//el hijo1 tiene de sgte al hijo2
			hijo1->data = padre->data;
			hijo1->sgte = hijo2;
			hijo1->ntotal = padre->ntotal / 2;
			hijo1->numeroNodo = ++global->nodosCount;
			hijo1->hijo1 = NULL;
			hijo1->hijo2 = NULL;
			hijo1->padre = padre;

			//el hijo2 tiene de sgte al sgte del padre
			hijo2->data = padre->data + definitivo->ntotal/2;
			hijo2->sgte = padre->sgte;
			hijo2->ntotal = padre->ntotal / 2;
			hijo2->numeroNodo = ++global->nodosCount;
			hijo2->hijo1 = NULL;
			hijo2->hijo2 = NULL;
			hijo2->padre = padre;

			//el padre tiene de sgte a hijo1
			padre->data = NULL;
			padre->sgte = hijo1;
			padre->ntotal = 0; //es padre => no tiene tamaño
			padre->hijo1 = hijo1;
			padre->hijo2 = hijo2;

			padre->ocupado = true;
			strcpy(padre->key, "");
			padre->ultimoAcceso.anomesdia = 0;
			padre->ultimoAcceso.horaminutosegundo = 0;
			padre->numeroNodo = 0;

			definitivo = hijo1; //sigo mirando qué onda con el hijo1
		}

		strcpy(definitivo->key, key); //definitivo es en el que se le va a ingresar el dato
		definitivo->ndata = ndata;
		definitivo->ocupado = true;
		definitivo->ultimoAcceso = ObtenerFechaActual();
		global->bytesLibres -= definitivo->ntotal;
	}
	return definitivo;
}

Nodo* ElegirParticion(Nodo* lista, size_t fixedSize, Nodo** first, Nodo** leastRecentlyUsed, Global* global) {
	Nodo* recorrido = lista;
	Nodo* definitivo = NULL;
	bool libreYEntra = false; //particion no ocupada, y con espacio para el dato
	*first = PrimerNodoOcupado(lista); //Victimas
	*leastRecentlyUsed = PrimerNodoOcupado(lista); //Victimas
	if (global->algoritmoSet == 0) { //First Fit (definitivo es el primero)
		//Buscar espacio libre:
		while (recorrido != NULL) {
			//Guardar first&lru:
			if (*first != NULL) {
				if (recorrido->ocupado && recorrido->numeroNodo < (*first)->numeroNodo && recorrido->ntotal > 0)
					*first = recorrido;
			}
			if (*leastRecentlyUsed != NULL) {
				if (recorrido->ocupado &&
					recorrido->ultimoAcceso.anomesdia <= (*leastRecentlyUsed)->ultimoAcceso.anomesdia &&
					recorrido->ultimoAcceso.horaminutosegundo < (*leastRecentlyUsed)->ultimoAcceso.horaminutosegundo &&
					recorrido->ntotal > 0)
						*leastRecentlyUsed = recorrido;
			}

			//Comprobar si nos sirve la particion:
			libreYEntra = (!recorrido->ocupado) && (recorrido->ntotal >= fixedSize);
			if (libreYEntra) {
				definitivo = recorrido;
				break;
			}
			recorrido = recorrido->sgte;
		}
	} else { //Best Fit (definitivo es el más pequeño en el que entre)
		//Buscar espacio libre:
		while (recorrido != NULL) {
			//Guardar first&lru:
			if (*first != NULL) {
				if (recorrido->ocupado && recorrido->numeroNodo < (*first)->numeroNodo && recorrido->ntotal > 0)
					*first = recorrido;
			}
			if (*leastRecentlyUsed != NULL) {
				if (recorrido->ocupado &&
					recorrido->ultimoAcceso.anomesdia <= (*leastRecentlyUsed)->ultimoAcceso.anomesdia &&
					recorrido->ultimoAcceso.horaminutosegundo < (*leastRecentlyUsed)->ultimoAcceso.horaminutosegundo &&
					recorrido->ntotal > 0)
						*leastRecentlyUsed = recorrido;
			}

			//Comprobar si nos sirve la particion:
			libreYEntra = (!recorrido->ocupado) && (recorrido->ntotal >= fixedSize);
			if (definitivo == NULL && libreYEntra)
				definitivo = recorrido;
			if (libreYEntra && recorrido->ntotal < definitivo->ntotal)
				definitivo = recorrido;
			recorrido = recorrido->sgte;
		}
	}
	return definitivo;
}

void BorrarVictima(Nodo* first, Nodo* leastRecentlyUsed, Global* global) {
	//hay que volar a uno (FIFO o LRU)
	if (global->algoritmoVict == 0) { //FIFO
		if (global->log_operaciones) log_debug(global->logger, "Se borra por FIFO la clave %s",first->key);
		if (global->algoritmoAloc == 1)
			BorrarBuddy(first, global);
		else
			InicializNodo(first, global);
		first->numeroNodo = ++global->nodosCount;
	} else { //LRU
		if (global->log_operaciones) log_debug(global->logger, "Se borra por LRU la clave %s", leastRecentlyUsed->key);
		if (global->algoritmoAloc == 1)
			BorrarBuddy(leastRecentlyUsed, global);
		else
			InicializNodo(leastRecentlyUsed, global);
		leastRecentlyUsed->numeroNodo = ++global->nodosCount;
	}
}

void BorrarBuddy(struct Nodo* nodo, Global* global) {
	Nodo* padre = nodo->padre;
	if (nodo->ocupado) {
		nodo->ocupado = false;
		global->bytesLibres += nodo->ntotal;
	}

	if (nodo->ntotal != 0) { //si es una hoja...
		if (!(padre == NULL) && !(padre->hijo2 == NULL)) { //si tiene padre y hermana... (igual: TIENE que tener hermana si tiene padre)
			if (!padre->hijo1->ocupado && !padre->hijo2->ocupado) { //si las dos hermanas están libres...
				padre->data = padre->hijo1->data;
				padre->sgte = padre->hijo2->sgte;
				padre->ntotal = padre->hijo1->ntotal * 2;
				padre->ocupado = false;
				padre->hijo1->usado = false;
				padre->hijo2->usado = false;
				padre->hijo1 = NULL;
				padre->hijo2 = NULL;
				global->nodosCant -= 2;
				if (!(padre->padre == NULL)) BorrarBuddy(padre, global); //sigue compactando en los niveles superiores
				if (global->log_operaciones) log_info(global->logger, "Se compactan dos particiones libres");
			}
		}
	}
}

void Compactar(struct Nodo** lista, Nodo* memLista, Global* global) {
	Nodo* recorrido = *lista; //puntero de recorrido
	uint32_t offset = 0;
	Nodo* anterior = NULL;

	while (recorrido != NULL) {
		offset += recorrido->ntotal;
		if (!recorrido->ocupado && recorrido->sgte != NULL) {
			//copia los datos para atrás:
			memcpy(recorrido->data, recorrido->data + recorrido->ntotal, global->partMaxSize - offset);
			AcomodarData(recorrido->sgte, recorrido->ntotal); //acomoda los punteros
			if (anterior != NULL) { //borra el nodo
				anterior->sgte = recorrido->sgte;
			} else {
				*lista = recorrido->sgte;
				anterior = NULL; //acabo de modificar el primero de la lista, NO HAY anterior (*)
			}
			recorrido->usado = false; //lo libera
			global->nodosCant--; //decrementa la cantidad de nodos
		} else if (recorrido->sgte == NULL) {
			//si es el último nodo...
			if (!recorrido->ocupado) { //si está libre
				recorrido->ntotal = global->bytesLibres; //la incrementa en espacio libre
			} else {
				//crea una partición nueva libre al final:
				Nodo* nuevaParticion = NuevoNodo(memLista, global);
				nuevaParticion->data = recorrido->data + recorrido->ntotal;
				nuevaParticion->ntotal = global->bytesLibres;
				nuevaParticion->numeroNodo = ++global->nodosCount;
				nuevaParticion->sgte = NULL;
				recorrido->sgte = nuevaParticion;
			}
			return; //nos vamos! ya terminamos de compactar
		}

		if (*lista != recorrido->sgte) //si HAY un anterior... (no cambié el primero)
			anterior = recorrido; //lo actualizo

		recorrido = recorrido->sgte;
	}
}

void AcomodarData(Nodo* siguiente, uint32_t offset) {
	//acomoda todos los punteros a data
	while (siguiente != NULL) {
		siguiente->data -= offset;
		siguiente = siguiente->sgte;
	}
}

Nodo* PrimerNodoOcupado(Nodo* lista) {
	//devuelve el primer nodo ocupado o NULL si todas vacías
	Nodo* primero = NULL;
	while (lista != NULL) {
		if (lista->ocupado && lista->ntotal > 0) {
			primero = lista;
			break;
		}
		lista = lista->sgte;
	}
	return primero;
}

bool ExisteClave(Nodo* lista, char* clave) {
	Nodo* nodoABuscar = ListaBuscar(lista, clave);
	return nodoABuscar != NULL;
}

Nodo* ListaBuscar(Nodo* lista, char* clave) {
	Nodo* recorrido = lista;

	while (recorrido != NULL) {
		if (recorrido->ocupado && strcmp(recorrido->key, clave) == 0) break;
		recorrido = recorrido->sgte;
	}
	if (recorrido != NULL)
		recorrido->ultimoAcceso = ObtenerFechaActual();
	return recorrido;
}

void ListaVaciar(Nodo** lista, Global* global) {
	while (*lista != NULL) {
		(*lista)->usado = false;
		*lista = (*lista)->sgte;
	}
	CrearPrimerParticion();
}

Nodo* NuevoNodo(Nodo* memLista, Global* global) {
	//si lo encuentra lo reserva
	int i;
	bool encontre = false;
	for (i=0; i < global->nodosMax; i++) {
		if (!memLista[i].usado) {
			memLista[i].usado = true; //reserva
			memLista[i].ocupado = false;
			InicializNodo(&memLista[i], global);
			global->nodosCant++;
			encontre = true;
			break;
		}
	}
	return encontre ? &memLista[i] : NULL;
}

bool InicializNodo(Nodo* nodo, Global* global) {
	if (nodo == NULL) return false;
	if (nodo->ocupado) {
		global->bytesLibres += nodo->ntotal;
	}
	nodo->key[0] = '\0';
	nodo->ndata = 0;
	nodo->ultimoAcceso.anomesdia = 0;
	nodo->ultimoAcceso.horaminutosegundo = 0;
	nodo->ocupado = false;
	nodo->hijo1 = NULL;
	nodo->hijo2 = NULL;
	return true;
}

void ImprimirParticiones(Nodo* lista,  Global* global) {
	Nodo* recorrido = lista;
	uint32_t fecha;
	uint32_t hora;

	printf("-------------------------\n");
	Fecha fechaActual = ObtenerFechaActual();
	printf("Dump: "); //mostrar fecha y hora:
	printf("%.4d/%.2d/%.2d %.2d:%.2d:%.2d:%.3d\n", FAno(fechaActual.anomesdia), FMes(fechaActual.anomesdia), FDia(fechaActual.anomesdia), FHor(fechaActual.horaminutosegundo), FMin(fechaActual.horaminutosegundo), FSeg(fechaActual.horaminutosegundo), FMil(fechaActual.horaminutosegundo));

	while (recorrido != NULL) {
		if (recorrido->ntotal == 0) {
			recorrido = recorrido->sgte;
			continue;
		}
		fecha = recorrido->ultimoAcceso.anomesdia;
		hora = recorrido->ultimoAcceso.horaminutosegundo;
		if (recorrido->ocupado) {
			printf("%u-%u: #%d [X] Size: %d DataSize: %d Fecha: %d Hora: %d Clave: %s", (unsigned int) recorrido->data, (unsigned int) recorrido->data + recorrido->ntotal - 1, recorrido->numeroNodo, recorrido->ntotal, recorrido->ndata, fecha, hora, recorrido->key);
			if (ClaveFirst(lista) == recorrido->key) printf(" (first)");
			if (ClaveLRU(lista) == recorrido->key) printf(" (lru)");
		} else {
			printf("%u-%u: #%d [L] Size: %d", (unsigned int) recorrido->data, (unsigned int) recorrido->data + recorrido->ntotal - 1, recorrido->numeroNodo, recorrido->ntotal);
		}
		if (global->algoritmoAloc == 1) {
			if (recorrido->padre == NULL) {
				printf(" (Nodo raiz)");
			} else {
				Nodo* hermana = (recorrido == recorrido->padre->hijo1) ? recorrido->padre->hijo2 : recorrido->padre->hijo1;
				if (hermana->ntotal > 0) printf(" Hermana: #%d", hermana->numeroNodo);
			}
		}
		printf("\n");
		recorrido = recorrido->sgte;
	}
	printf("Tamaño máx: %d; Tamaño min: %d; Nodos máx: %d; Nodos cant: %d; Bytes libres: %d\n", global->partMaxSize, global->partMinSize, global->nodosCount, global->nodosCant, global->bytesLibres);
	printf("-------------------------\n");
}

void DumpDeLaCache(Nodo* lista,  Global* global) {
	Nodo* recorrido = lista;
	uint32_t fecha;
	uint32_t hora;
	fseek(global->dump, 0, SEEK_SET);

	fprintf(global->dump, "-------------------------\n");
	Fecha fechaActual = ObtenerFechaActual();
	fprintf(global->dump, "Dump: "); //mostrar fecha y hora:
	fprintf(global->dump, "%.4d/%.2d/%.2d %.2d:%.2d:%.2d:%.3d\n", FAno(fechaActual.anomesdia), FMes(fechaActual.anomesdia), FDia(fechaActual.anomesdia), FHor(fechaActual.horaminutosegundo), FMin(fechaActual.horaminutosegundo), FSeg(fechaActual.horaminutosegundo), FMil(fechaActual.horaminutosegundo));

	while (recorrido != NULL) {
		if (recorrido->ntotal == 0) {
			recorrido = recorrido->sgte;
			continue;
		}
		fecha = recorrido->ultimoAcceso.anomesdia;
		hora = recorrido->ultimoAcceso.horaminutosegundo;
		if (recorrido->ocupado) {
			fprintf(global->dump, "%u-%u: #%d [X] Size: %d DataSize: %d Fecha: %d Hora: %d Clave: %s", (unsigned int) recorrido->data, (unsigned int) recorrido->data + recorrido->ntotal - 1, recorrido->numeroNodo, recorrido->ntotal, recorrido->ndata, fecha, hora, recorrido->key);
			if (ClaveFirst(lista) == recorrido->key) fprintf(global->dump, " (first)");
			if (ClaveLRU(lista) == recorrido->key) fprintf(global->dump, " (lru)");
		} else {
			fprintf(global->dump, "%u-%u: #%d [L] Size: %d", (unsigned int) recorrido->data, (unsigned int) recorrido->data + recorrido->ntotal - 1, recorrido->numeroNodo, recorrido->ntotal);
		}
		if (global->algoritmoAloc == 1) {
			if (recorrido->padre == NULL) {
				fprintf(global->dump, " (Nodo raiz)");
			} else {
				Nodo* hermana = (recorrido == recorrido->padre->hijo1) ? recorrido->padre->hijo2 : recorrido->padre->hijo1;
				if (hermana->ntotal > 0) fprintf(global->dump, " Hermana: #%d", hermana->numeroNodo);
			}
		}
		fprintf(global->dump, "\n");
		recorrido = recorrido->sgte;
	}
	fprintf(global->dump, "Tamaño máx: %d; Tamaño min: %d; Nodos máx: %d; Nodos cant: %d; Bytes libres: %d\n", global->partMaxSize, global->partMinSize, global->nodosCount, global->nodosCant, global->bytesLibres);
	fprintf(global->dump, "-------------------------\n");
	fflush(global->dump);
}

char* ClaveFirst(Nodo* lista) {
	Nodo* recorrido = lista;
	Nodo* first = PrimerNodoOcupado(lista);
	if (first == NULL) return "";

	while (recorrido != NULL) {
		if (recorrido->ntotal > 0) {
			if (recorrido->ocupado && recorrido->numeroNodo < first->numeroNodo && recorrido->ntotal > 0)
				first = recorrido;
		}
		recorrido = recorrido->sgte;
	}
	return first->key;
}

char* ClaveLRU(Nodo* lista) {
	Nodo* recorrido = lista;
	Nodo* lru = PrimerNodoOcupado(lista);
	if (lru == NULL) return "";

	while (recorrido != NULL) {
		if (recorrido->ntotal > 0) {
			if (recorrido->ocupado &&
				recorrido->ultimoAcceso.anomesdia <= lru->ultimoAcceso.anomesdia &&
				recorrido->ultimoAcceso.horaminutosegundo < lru->ultimoAcceso.horaminutosegundo &&
				recorrido->ntotal > 0)
					lru = recorrido;
		}
		recorrido = recorrido->sgte;
	}
	return lru->key;
}

void VoidAStr(char* str, const char* dato, uint32_t longitud) {
	//desde el cliente/servidor le mando las claves con \0 (como string)
	//pero desde telnet le llegan como void* sin el \0
	//(esto unificaría las dos cosas)
	int i;
	bool esUnString = false;
	for (i=0; i < longitud; i++) {
		if (dato[i] == '\0') {
			esUnString = true;
			break;
		}
	}
	memcpy(str, dato, longitud);
	if (!esUnString) {
		str[longitud] = '\0';
	}
}

Fecha ObtenerFechaActual() {
	struct tm tmp;
	time_t tiempo = time(0);
	struct timeval  tv;
	gettimeofday(&tv, NULL);
	struct tm *tlocal = localtime_r(&tiempo, &tmp);
	char output[128];
	strftime(output,128,"%Y%m%d",tlocal);
	int numero = atoi(output);
	strftime(output,128,"%H%M%S",tlocal);
	sprintf(((char*) output) + strlen(output), "%.3d", (int) tv.tv_usec / 1000);
	int numero2 = atoi(output);
	Fecha fecha;
	fecha.anomesdia = numero;
	fecha.horaminutosegundo = numero2;
	return fecha;
}

//aammdd:
uint32_t FAno(uint32_t fecha) { return fecha / 10000; }
uint32_t FMes(uint32_t fecha) { return (fecha / 100) % 100; }
uint32_t FDia(uint32_t fecha) { return fecha % 100; }
//hhmmssuuu:
uint32_t FHor(uint32_t tiempo) { return tiempo / 10000000; }
uint32_t FMin(uint32_t tiempo) { return (tiempo / 100000) % 100; }
uint32_t FSeg(uint32_t tiempo) { return (tiempo / 1000) % 100; }
uint32_t FMil(uint32_t tiempo) { return tiempo % 1000; }
