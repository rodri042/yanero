#ifndef LISTA_H_
#define LISTA_H_

	#include "main.h"

	Nodo* ListaAgregar(Nodo** listaPtr, size_t ndata, char* key, Nodo* memLista, void* memCache, Global* global);
	Nodo* ElegirParticion(Nodo* lista, size_t fixedSize, Nodo** first, Nodo** lastRecentlyUsed, Global* global);
	void BorrarVictima(Nodo* first, Nodo* lastRecentlyUsed, Global* global);
	void BorrarBuddy(struct Nodo* nodo, Global* global);
	void Compactar(struct Nodo** lista, Nodo* memLista, Global* global);
	void AcomodarData(Nodo* siguiente, uint32_t offset);
	Nodo* PrimerNodoOcupado(Nodo* lista);
	bool ExisteClave(Nodo* lista, char* clave);
	Nodo* ListaBuscar(Nodo* lista, char* clave);
	void ListaVaciar(Nodo** lista, Global* global);
	Nodo* NuevoNodo(Nodo* lista, Global* global);
	bool InicializNodo(Nodo* nodo, Global* global);

	void ImprimirParticiones(Nodo* lista, Global* global);
	void DumpDeLaCache(Nodo* lista, Global* global);
	char* ClaveFirst(Nodo* lista);
	char* ClaveLRU(Nodo* lista);
	void VoidAStr(char* str, const char* dato, uint32_t longitud);
	Fecha ObtenerFechaActual();
	uint32_t FAno(uint32_t fecha);
	uint32_t FMes(uint32_t fecha);
	uint32_t FDia(uint32_t fecha);
	uint32_t FHor(uint32_t tiempo);
	uint32_t FMin(uint32_t tiempo);
	uint32_t FSeg(uint32_t tiempo);
	uint32_t FMil(uint32_t tiempo);
	#endif /* LISTA_H_ */
