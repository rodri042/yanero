#include "main.h"

#define PROCESS_NAME "FSC"
#define MAX_KEY 43

//Estructura con la posta de la vida:
typedef struct {
	char ipCache[16];                //IP del servidor de memcached
	bool cacheConectada;             //Si está conectado a la cache o no
	SckCliente** clientes;           //Vector de clientes
	sem_t clientesLibresMaxSem;      //Semáforo contador de clientes libres
	sem_t clientesLibresSem;         //Semáforo para controlar el vector clientesLibres
	bool* clientesLibres;            //Vector con clientes libres
	t_config* config;                //Instancia de la configuración
	t_log* logger;                   //Instancia del logger
} SuperDatos;
SuperDatos SD;

//Estructura utilizada para almacenar parámetros que le pasemos al main por línea de comando:
struct t_runtime_options {
	char* welcome_msg;
} runtime_options;

//Esta Macro sirve para definir parámetros propios que queremos que FUSE interprete:
//(después se usa para completar campos como welcome_msg)
#define CUSTOM_FUSE_OPT_KEY(t, p, v) { t, offsetof(struct t_runtime_options, p), v }

/* keys for FUSE_OPT_ options */
enum {
	KEY_VERSION,
	KEY_HELP,
};

//Estructura utilizada para decirle a la biblioteca de FUSE qué
//parametro puede recibir y donde tiene que guardar el valor de estos:
static struct fuse_opt fuse_options[] = {
		//Este es un parametro definido por nosotros:
		CUSTOM_FUSE_OPT_KEY("--welcome-msg %s", welcome_msg, 0),

		//Estos son parametros por defecto que ya tiene FUSE:
		FUSE_OPT_KEY("-V", KEY_VERSION),
		FUSE_OPT_KEY("--version", KEY_VERSION),
		FUSE_OPT_KEY("-h", KEY_HELP),
		FUSE_OPT_KEY("--help", KEY_HELP),
		FUSE_OPT_END,
};

uint32_t ClienteLibre();
bool HandShake(SckCliente* cliente);
void BorrarDirCache(const char* path);
void SepararCarpeta_Nombre(const char* path, char** nombre, char** pathCarpeta);
void MuchoPath(const char* path);
void Boom();
//---------------------------------------------------------------------------------
/*
 * @DESC
 *  Esta función va a ser llamada cuando a la biblioteca de FUSE le llege un pedido
 * para obtener la metadata de un archivo/directorio. Esto puede ser tamaño, tipo,
 * permisos, dueño, etc ...
 *
 * @PARAMETROS
 * 		path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 * 		       encontrar el archivo o directorio que nos solicitan
 * 		stbuf - Esta esta estructura es la que debemos completar
 *
 * 	@RETURN
 * 		O archivo/directorio fue encontrado. -ENOENT archivo/directorio no encontrado
 */
static int Getattr(const char *path, struct stat *stbuf) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	char* atributosBuf;
	EntradaAtributos* attr;
	memcached_return resCache;
	memset(stbuf, 0, sizeof(struct stat));

	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "Gettatr: %s", path);

	char key[MAX_KEY];
	key[0] = 'c'; //el registro en la cache pertenece al cliente
	key[1] = 'g'; //el registro corresponde a la operación Getattr
	strcpy(key + 2, path);

	resCache = MEMCACHED_FAILURE;
	if (SD.cacheConectada) resCache = Cache_ObtenerValor(SD.ipCache, key, NULL, (void*) &atributosBuf);
	if (resCache != MEMCACHED_SUCCESS) {
		if (SD.cacheConectada && config_get_int_value(SD.config, "LOG_CONSULTACACHE") == 1) {
			log_debug(SD.logger, "Consulta a cache fallida - Getattr: %s", path);
		}

		sem_wait(&SD.clientesLibresMaxSem);
		uint32_t clienteNum = ClienteLibre();
		SckCliente* cliente = SD.clientes[clienteNum];

		SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
		pkg->header.type = OP_GETATTR;
		pkg->header.length = strlen(path) + 1;

		Ruta ruta; //(lleno el paquete)
		strcpy(ruta.path, path);
		memcpy(pkg->data, &ruta, sizeof(Ruta));
		Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
		free(pkg);

		pkg = Socket_Recibir(cliente); //(recibo la respuesta)
		if (pkg == NULL) Boom();

		//copio los atributos y libero el cliente:
		atributosBuf = malloc(pkg->header.length);
		memcpy(atributosBuf, pkg->data, pkg->header.length);
		attr = (EntradaAtributos*) atributosBuf;
		if (attr->atime == 0) {
			free(atributosBuf);
			free(pkg);
			SD.clientesLibres[clienteNum] = true;
			sem_post(&SD.clientesLibresMaxSem);
			return -ENOENT;
		}
		free(pkg);
		SD.clientesLibres[clienteNum] = true;
		sem_post(&SD.clientesLibresMaxSem);

		//como no estaba, meto el dato en la cache: (pasado a un vector de bytes)
		if (SD.cacheConectada) {
			Cache_InsertarClaveNueva(SD.ipCache, key, sizeof(EntradaAtributos), atributosBuf);
			if (config_get_int_value(SD.config, "LOG_ALMACENARCACHE") == 1) {
				log_debug(SD.logger, "Almacenamiento en cache - Getattr: %s", path);
			}
		}
	} else if (config_get_int_value(SD.config, "LOG_CONSULTACACHE") == 1) {
		log_debug(SD.logger, "Consulta a cache OK - Getattr: %s", path);
	}

	attr = (EntradaAtributos*) atributosBuf;
	stbuf->st_gid = attr->gid;
	stbuf->st_nlink = attr->links;
	stbuf->st_mode = attr->mode;
	stbuf->st_size = attr->size;
	stbuf->st_uid = attr->uid;
	free(atributosBuf);

	return 0;
}

/*
 * @DESC
 *  Esta función va a ser llamada cuando a la biblioteca de FUSE le llege un pedido
 * para obtener la lista de archivos o directorios que se encuentra dentro de un directorio
 *
 * @PARAMETROS
 * 		path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 * 		       encontrar el archivo o directorio que nos solicitan
 * 		buf - Este es un buffer donde se colocaran los nombres de los archivos y directorios
 * 		      que esten dentro del directorio indicado por el path
 * 		filler - Este es un puntero a una función, la cual sabe como guardar una cadena dentro
 * 		         del campo buf
 *
 * 	@RETURN
 * 		O directorio fue encontrado. -ENOENT directorio no encontrado
 */
static int ReadDir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	(void) offset;
	(void) fi;
	char* carpetas = NULL;
	size_t carpetasSize;
	memcached_return resCache;

	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "ReadDir: %s", path);

	char key[MAX_KEY];
	key[0] = 'c'; //el registro en la cache pertenece al cliente
	key[1] = 'r'; //el registro corresponde a la operación ReadDir
	strcpy(key + 2, path);

	resCache = MEMCACHED_FAILURE;
	if (SD.cacheConectada) resCache = Cache_ObtenerValor(SD.ipCache, key, &carpetasSize, (void*) &carpetas);
	if (resCache != MEMCACHED_SUCCESS) {
		if (SD.cacheConectada && config_get_int_value(SD.config, "LOG_CONSULTACACHE") == 1) {
			log_debug(SD.logger, "Consulta a cache fallida - ReadDir: %s", path);
		}
		sem_wait(&SD.clientesLibresMaxSem);
		uint32_t clienteNum = ClienteLibre();
		SckCliente* cliente = SD.clientes[clienteNum];

		SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
		pkg->header.type = OP_READDIR;
		pkg->header.length = strlen(path) + 1;

		Ruta ruta; //(lleno el paquete)
		strcpy(ruta.path, path);
		memcpy(pkg->data, &ruta, sizeof(Ruta));

		Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
		free(pkg);

		carpetas = malloc(1); //en carpetas guardo el contenido de los paquetes
		carpetasSize = 0; //para almacenarlo después en la cache...
		uint32_t offset = 0;
		pkg = Socket_Recibir(cliente);
		if (pkg == NULL) Boom();
		while(pkg->header.type != RSP_EOF) {
			carpetasSize += pkg->header.length;
			carpetas = realloc(carpetas, carpetasSize);
			memcpy(carpetas + offset, pkg->data, pkg->header.length);
			offset += pkg->header.length;
			free(pkg);
			pkg = Socket_Recibir(cliente);
			if (pkg == NULL) Boom();
		} free(pkg);
		SD.clientesLibres[clienteNum] = true;
		sem_post(&SD.clientesLibresMaxSem);

		//como no estaba, meto el dato en la cache:
		if (SD.cacheConectada) {
			Cache_InsertarClaveNueva(SD.ipCache, key, carpetasSize, (char*) carpetas);
			if (config_get_int_value(SD.config, "LOG_ALMACENARCACHE") == 1) {
				log_debug(SD.logger, "Almacenamiento en cache - Readdir: %s", path);
			}
		}
	} else if (config_get_int_value(SD.config, "LOG_CONSULTACACHE") == 1) {
		log_debug(SD.logger, "Consulta a cache OK - ReadDir: %s", path);
	}

	uint32_t leido = 0;
	while (leido < carpetasSize) {
		filler(buf, carpetas + (leido+1), NULL, 0);
		leido = leido + carpetas[leido] + 1;
	}
	free(carpetas);

	return 0;
}

/*
 * @DESC
 *  Esta función va a ser llamada cuando a la biblioteca de FUSE le llege un pedido
 * para tratar de abrir un archivo
 *
 * @PARAMETROS
 * 		path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 * 		       encontrar el archivo o directorio que nos solicitan
 * 		fi - es una estructura que contiene la metadata del archivo indicado en el path
 *
 * 	@RETURN
 * 		O archivo fue encontrado. -EACCES archivo no es accesible
 */
static int OpenFile(const char *path, struct fuse_file_info *fi) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "OpenFile: %s", path);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_OPENFILE;
	pkg->header.length = sizeof(Ruta);

	Ruta ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	memcpy(pkg->data, &ruta, sizeof(Ruta));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);

	pkg = Socket_Recibir(cliente); //(recibo la respuesta)
	if (pkg == NULL) Boom();
	int32_t resp;
	if (pkg->header.type != RSP_OPENFILE)
		resp = -EIO;
	else
		resp = *((int32_t*) pkg->data); //creo un puntero a entero, con los datos recibidos
		//y accedo a su contenido (el código de respuesta) para asignarlo a resp
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	return resp;
}

/*
 * @DESC
 *  Esta función va a ser llamada cuando a la biblioteca de FUSE le llege un pedido
 * para obtener el contenido de un archivo
 *
 * @PARAMETROS
 * 		path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 * 		       encontrar el archivo o directorio que nos solicitan
 * 		buf - Este es el buffer donde se va a guardar el contenido solicitado
 * 		size - Nos indica cuanto tenemos que leer
 * 		offset - A partir de que posicion del archivo tenemos que leer
 *
 * 	@RETURN
 * 		Si se usa el parametro direct_io los valores de retorno son 0 si  elarchivo fue encontrado
 * 		o -ENOENT si ocurrio un error. Si el parametro direct_io no esta presente se retorna
 * 		la cantidad de bytes leidos o -ENOENT si ocurrio un error. ( Este comportamiento es igual
 * 		para la funcion write )
 */
static int ReadFile(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	(void) fi;

	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "ReadFile: %s; Offset: %d; Size: %d", path, (int) offset, size);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_READFILE;
	pkg->header.length = sizeof(RutaLeerEscribir);

	RutaLeerEscribir ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	ruta.offset = offset;
	ruta.cant = size;
	memcpy(pkg->data, &ruta, sizeof(RutaLeerEscribir));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);

	//Recibir los bytes del archivo:
	uint32_t leido = 0;
	pkg = Socket_Recibir(cliente);
	if (pkg == NULL) Boom();
	while(pkg->header.type != RSP_EOF) {
		memcpy(buf + leido, pkg->data, pkg->header.length);
		leido += pkg->header.length;
		free(pkg);
		pkg = Socket_Recibir(cliente);
		if (pkg == NULL) Boom();
	}
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	return leido;
}

static int CreateFile(const char *path, mode_t mode, struct fuse_file_info *fi) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "CreateFile: %s", path);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_CREATEFILE;
	pkg->header.length = sizeof(Ruta);

	Ruta ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	memcpy(pkg->data, &ruta, sizeof(Ruta));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);

	pkg = Socket_Recibir(cliente); //(recibo la respuesta)
	if (pkg == NULL) Boom();
	int32_t resp;
	if (pkg->header.type != RSP_CREATEFILE)
		resp = -EIO;
	else
		resp = *((int32_t*) pkg->data);
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	//Borro de la cache para mantener coherencia:
	if (SD.cacheConectada) {
		char* nombre; char* pathALaCarpeta;
		SepararCarpeta_Nombre(path, &nombre, &pathALaCarpeta);
		char key[MAX_KEY];
		key[0] = 'c';
		key[1] = 'r';
		strcpy(key + 2, pathALaCarpeta);
		Cache_EliminarDato(SD.ipCache, key);
		key[1] = 'g';
		Cache_EliminarDato(SD.ipCache, key);
		free(nombre); free(pathALaCarpeta);
	}

	return resp;
}

static int WriteFile(const char *path, const char *buf, size_t size,
                 off_t offset, struct fuse_file_info *fi) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	(void) fi;

	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "WriteFile: %s; Offset: %d; Size: %d", path, (int) offset, size);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_WRITEFILE;
	pkg->header.length = sizeof(RutaLeerEscribir);

	RutaLeerEscribir ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	ruta.offset = offset;
	ruta.cant = size;
	memcpy(pkg->data, &ruta, sizeof(RutaLeerEscribir));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);

	pkg = Socket_Recibir(cliente); //(recibo la confirmación)
	if (pkg == NULL) Boom();
	if (pkg->header.type != RSP_WRITEFILE) {
		free(pkg);
		return -EIO;
	}
	free(pkg);

	//Mando los bytes del archivo:
	uint32_t bytesAMandar = size;
	uint32_t bytesMandados = 0;
	while (bytesAMandar > 0) { //mando los datos verificando que no superen MAX_BUFFER bytes
		pkg = malloc(sizeof(SckPaquete));
		pkg->header.type = RSP_WRITEFILE;
		if (bytesAMandar > MAX_BUFFER)
			pkg->header.length = MAX_BUFFER;
		else
			pkg->header.length = bytesAMandar;
		memcpy(pkg->data, buf + bytesMandados, pkg->header.length);
		Socket_Enviar(cliente, pkg);
		bytesMandados += pkg->header.length;
		bytesAMandar -= pkg->header.length;
		free(pkg);
	}
	pkg = malloc(sizeof(SckPaquete));
	pkg->header.type = RSP_EOF; //mando el EOF
	pkg->header.length = 0;
	Socket_Enviar(cliente, pkg);
	free(pkg);

	pkg = Socket_Recibir(cliente); //(recibo la respuesta)
	if (pkg == NULL) Boom();
	int32_t resp;
	if (pkg->header.type != RSP_WRITEFILE)
		resp = -EIO;
	else
		resp = *((int32_t*) pkg->data);
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	//Borro de la cache para mantener coherencia:
	if (SD.cacheConectada) {
		char key[MAX_KEY];
		key[0] = 'c';
		key[1] = 'g';
		strcpy(key + 2, path);
		Cache_EliminarDato(SD.ipCache, key);
	}

	return resp;
}

static int DeleteFile(const char *path) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "DeleteFile: %s", path);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_DELETEFILE;
	pkg->header.length = sizeof(Ruta);

	Ruta ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	memcpy(pkg->data, &ruta, sizeof(Ruta));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);

	pkg = Socket_Recibir(cliente); //(recibo la respuesta)
	if (pkg == NULL) Boom();
	int32_t resp;
	if (pkg->header.type != RSP_DELETEFILE)
		resp = -EIO;
	else
		resp = *((int32_t*) pkg->data);
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	//Borro de la cache para mantener coherencia:
	if (SD.cacheConectada) {
		char* nombre; char* pathALaCarpeta;	//separo el nombre del path a la carpeta
		SepararCarpeta_Nombre(path, &nombre, &pathALaCarpeta);
		char key[MAX_KEY];
		key[0] = 'c';
		key[1] = 'r';
		strcpy(key + 2, pathALaCarpeta);
		Cache_EliminarDato(SD.ipCache, key);
		key[1] = 'g';
		Cache_EliminarDato(SD.ipCache, key);
		key[1] = 'g';
		strcpy(key + 2, path);
		Cache_EliminarDato(SD.ipCache, key);
		free(nombre); free(pathALaCarpeta);
	}

	return resp;
}

static int TruncateFile(const char *path, off_t size) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "TruncateFile: %s; Size: %d", path, size);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_TRUNCATEFILE;
	pkg->header.length = sizeof(RutaTruncate);

	RutaTruncate ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	ruta.longitud = size;
	memcpy(pkg->data, &ruta, sizeof(RutaTruncate));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);

	pkg = Socket_Recibir(cliente); //(recibo la respuesta)
	if (pkg == NULL) Boom();
	int32_t resp;
	if (pkg->header.type != RSP_TRUNCATEFILE)
		resp = -EIO;
	else
		resp = *((int32_t*) pkg->data);
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	//Borro de la cache para mantener coherencia:
	if (SD.cacheConectada) {
		char key[MAX_KEY];
		key[0] = 'c';
		key[1] = 'g';
		strcpy(key + 2, path);
		Cache_EliminarDato(SD.ipCache, key);
	}

	return resp;
}

static int CloseFile(const char *path, struct fuse_file_info *fi) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	(void) path;

	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "CloseFile: %s", path);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_CLOSEFILE;
	pkg->header.length = sizeof(Ruta);

	Ruta ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	memcpy(pkg->data, &ruta, sizeof(Ruta));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	return 0;
}

static int CreateDir(const char *path, mode_t mode) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "CreateDir: %s", path);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_CREATEDIR;
	pkg->header.length = sizeof(Ruta);

	Ruta ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	memcpy(pkg->data, &ruta, sizeof(Ruta));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);

	pkg = Socket_Recibir(cliente); //(recibo la respuesta)
	if (pkg == NULL) Boom();
	int32_t resp;
	if (pkg->header.type != RSP_CREATEDIR)
		resp = -EIO;
	else
		resp = *((int32_t*) pkg->data);
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	//Borro de la cache para mantener coherencia:
	if (SD.cacheConectada) {
		char* nombre; char* pathALaCarpeta;
		SepararCarpeta_Nombre(path, &nombre, &pathALaCarpeta);
		char key[MAX_KEY];
		key[0] = 'c';
		key[1] = 'r';
		strcpy(key + 2, pathALaCarpeta);
		Cache_EliminarDato(SD.ipCache, key);
		key[1] = 'g';
		Cache_EliminarDato(SD.ipCache, key);
		free(nombre); free(pathALaCarpeta);
	}

	return resp;
}

static int DeleteDir(const char *path) {
	if (strlen(path) > MAX_KEY - 3) { MuchoPath(path); return -ENOENT; }
	if (config_get_int_value(SD.config, "LOG_OPERACIONES") == 1)
		log_debug(SD.logger, "DeleteDir: %s", path);

	sem_wait(&SD.clientesLibresMaxSem);
	uint32_t clienteNum = ClienteLibre();
	SckCliente* cliente = SD.clientes[clienteNum];

	SckPaquete* pkg = malloc(sizeof(SckPaquete)); //(creo el paquete)
	pkg->header.type = OP_DELETEDIR;
	pkg->header.length = sizeof(Ruta);

	Ruta ruta; //(lleno el paquete)
	strcpy(ruta.path, path);
	memcpy(pkg->data, &ruta, sizeof(Ruta));

	Socket_Enviar(cliente, pkg); //(mando y borro el paquete)
	free(pkg);

	pkg = Socket_Recibir(cliente); //(recibo la respuesta)
	if (pkg == NULL) Boom();
	int32_t resp;
	if (pkg->header.type != RSP_DELETEDIR)
		resp = -EIO;
	else
		resp = *((int32_t*) pkg->data);
	free(pkg);
	SD.clientesLibres[clienteNum] = true;
	sem_post(&SD.clientesLibresMaxSem);

	//Borrar ReadDir, Getattr, y ReadDir&Getattr de todos los hijos:
	if (SD.cacheConectada) BorrarDirCache(path);

	return resp;
}

//-------------------------------------------------------------------
//Estructura principal de FUSE, con punteros a funciones. Con esto le
//decimos al motor qué función invocar para cada operación...
static struct fuse_operations operaciones = {
		.getattr = Getattr,
		.readdir = ReadDir,
		.open = OpenFile,
		.read = ReadFile,
		.create = CreateFile,
		.write = WriteFile,
		.unlink = DeleteFile,
		.truncate = TruncateFile,
		.release = CloseFile,
		.mkdir = CreateDir,
		.rmdir = DeleteDir,
};
//-------------------------------------------------------------------

//Dentro de los argumentos que recibe nuestro programa obligatoriamente
//debe estar el path al directorio donde vamos a montar nuestro FS
int main(int argc, char *argv[]) {
	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
	uint16_t i;

	//Limpio la estructura que va a contener los parametros:
	memset(&runtime_options, 0, sizeof(struct t_runtime_options));

	//Esta funcion de FUSE lee los parametros recibidos y los intepreta:
	if (fuse_opt_parse(&args, &runtime_options, fuse_options, NULL) == -1){
		perror("Invalid arguments!");
		return EXIT_FAILURE;
	}

	// Si se paso el parametro --welcome-msg
	// el campo welcome_msg deberia tener el
	// valor pasado
	if( runtime_options.welcome_msg != NULL ){
		printf("%s\n", runtime_options.welcome_msg);
	}

	//Abrir archivo de logs:
	unlink("log.txt");
	SD.logger = log_create("log.txt", PROCESS_NAME, true, LOG_LEVEL_TRACE);

	//Abrir archivos de configuración y logs:
	SD.config = config_create("config.txt"); //La config es: VARIABLE=VALOR[enter]VARIABLE=VALOR...
	char* parametros[] = {"LOG_ALMACENARCACHE", //0 o 1
						  "LOG_CONSULTACACHE", //0 o 1
						  "LOG_OPERACIONES", //0 o 1
						  "LOG_MIN_LEVEL", //0 a 4
						  "CONEX_MAX",
						  "IP_SERVIDOR",
						  "PUERTO_SERVIDOR",
						  "IP_CACHE"
						 };
	t_log_level level = LOG_LEVEL_TRACE;
	unlink("log.txt");
	if (config_has_property(SD.config, "LOG_MIN_LEVEL"))
		level = config_get_int_value(SD.config, "LOG_MIN_LEVEL");
	SD.logger = log_create("log.txt", PROCESS_NAME, true, level);
	for(i=0; i < 8; i++) {
		if (!config_has_property(SD.config, parametros[i])) {
			log_error(SD.logger, "Error en el archivo de configuración");
			return 1;
		}
	}

	strcpy(SD.ipCache, config_get_string_value(SD.config, "IP_CACHE"));

	//Crear pool de conexiones:
	SD.clientes = malloc(sizeof(SckCliente) * config_get_int_value(SD.config, "CONEX_MAX"));
	SD.clientesLibres = malloc(sizeof(bool) * config_get_int_value(SD.config, "CONEX_MAX"));
	uint16_t libres = 0;
	for (i=0; i < config_get_int_value(SD.config, "CONEX_MAX"); i++) {
		SD.clientesLibres[i] = true;
		SD.clientes[i] = Socket_CrearCliente(NULL, 0);
		if (Socket_Conectar(SD.clientes[i], config_get_string_value(SD.config, "IP_SERVIDOR"), config_get_int_value(SD.config, "PUERTO_SERVIDOR")) == -1) {
			//Conecta, y si no se puede, lo marca como ocupado para siempre
			SD.clientesLibres[i] = false;
		} else {
			if (!HandShake(SD.clientes[i])) {
				close(SD.clientes[i]->socket->descriptor);
				SD.clientesLibres[i] = false;
			} else {
				libres++;
			}
		}
	}
	if (libres == 0) { log_error(SD.logger, "No se pudo conectar al servidor"); return 1; }
	if (libres < config_get_int_value(SD.config, "CONEX_MAX")) {
		log_error(SD.logger, "No todos los clientes del pool se pudieron conectar");
	}
	sem_init(&SD.clientesLibresMaxSem, 0, libres);
	sem_init(&SD.clientesLibresSem, 0, 1);

	//Testear cache:
	void* valor;
	SD.cacheConectada = true;
	memcached_return resCache = Cache_ObtenerValor(SD.ipCache, "test", NULL, (void*) &valor);
	//printf("el memcached_return es %d\n", resCache);
	if (resCache != MEMCACHED_NOTFOUND) {
		SD.cacheConectada = false;
		log_warning(SD.logger, "No se pudo conectar con la cache");
	}

	// Esta es la funcion principal de FUSE, es la que se encarga
	// de realizar el montaje, comuniscarse con el kernel, delegar operaciones
	// en varios threads
	log_info(SD.logger, "FUSE levantado");
	return fuse_main(args.argc, args.argv, &operaciones, NULL);
}

uint32_t ClienteLibre() {
	//devuelve el indice de un cliente libre
	//al encontrarlo lo reserva
	uint32_t i = 0;
	sem_wait(&SD.clientesLibresSem);
	while (SD.clientesLibres[i] != true) {
		i++;
		if (i == config_get_int_value(SD.config, "CONEX_MAX")) i = 0;
	}
	SD.clientesLibres[i] = false;
	sem_post(&SD.clientesLibresSem);
	return i;
}

bool HandShake(SckCliente* cliente) {
	//realiza el handshake con el servidor
	cliente->handShaked = false;
	SckPaquete* pkgHandShake = malloc(sizeof(SckPaquete));
	pkgHandShake->header.type = OP_HANDSHAKE;
	pkgHandShake->header.length = 6;
	strcpy(pkgHandShake->data, "Hola?");
	Socket_Enviar(cliente, pkgHandShake);
	free(pkgHandShake);
	pkgHandShake = Socket_Recibir(cliente);
	if (pkgHandShake != NULL && pkgHandShake->header.type == 0 && strcmp(pkgHandShake->data, "Hola!") == 0)
		cliente->handShaked = true;
	free(pkgHandShake);
	return cliente->handShaked;
}

void BorrarDirCache(const char* path) {
	//lee de la cache una carpeta y borra todos los registros relacionados con ella
	char* carpetas = NULL;
	size_t carpetasSize;
	memcached_return resCache;

	char key[MAX_KEY];
	key[0] = 'c'; //el registro en la cache pertenece al cliente
	key[1] = 'r'; //el registro corresponde a la operación ReadDir
	strcpy(key + 2, path);

	resCache = Cache_ObtenerValor(SD.ipCache, key, &carpetasSize, (void*) &carpetas);
	if (resCache == MEMCACHED_SUCCESS) {
		uint32_t leido = 0;
		char keyChild[MAX_KEY];
		keyChild[0] = 'c';
		while (leido < carpetasSize) {
			keyChild[1] = 'r';
			strcpy(keyChild + 2, key + 2);
			strcat(keyChild, "/");
			strcat(keyChild, carpetas + (leido+1));

			//si es una carpeta me llamo a mi mismo, sino simplemente borro los registros
			char* hijos = NULL;
			memcached_return esDirectorio;
			esDirectorio = Cache_ObtenerValor(SD.ipCache, keyChild, NULL, (void*) &hijos);
			if (esDirectorio == MEMCACHED_SUCCESS) {
				BorrarDirCache(keyChild + 2);
			} else {
				Cache_EliminarDato(SD.ipCache, keyChild);
				keyChild[1] = 'g';
				Cache_EliminarDato(SD.ipCache, keyChild);
				leido = leido + carpetas[leido] + 1;
			}
			free(hijos);
		}
	}
	free(carpetas);

	//borro el readdir&getattr de la carpeta:
	Cache_EliminarDato(SD.ipCache, key);
	key[1] = 'g';
	Cache_EliminarDato(SD.ipCache, key);
}

void SepararCarpeta_Nombre(const char* path_cnst, char** nombre, char** pathCarpeta) {
	//ES UN ASCO ESTE CÓDIGO PERO ASDLKAJSHGLKASSAFWASKFJWF D=
	char path[MAX_KEY];
	strcpy(path, path_cnst);
	if (path[0] != '/') return;
	uint32_t _ocurrCaracter(const char* str, uint32_t strLen, char c) {
		uint32_t cant = 0; int i; for (i=0; i < strLen; i++) { if (str[i] == c) cant++; } return cant;
	}
	char** splitted = string_split(path, "/");
	uint32_t splittedLast = _ocurrCaracter(path, strlen(path), '/') - 1;
	uint32_t sizePathCarpeta;
	if (path[strlen(path) - 1] != '/') {
		*nombre = splitted[splittedLast];
		sizePathCarpeta = strlen(path) - strlen(*nombre) - 1;
	} else {
		splittedLast--;
		*nombre = malloc(1);
		*nombre = '\0';
		sizePathCarpeta = strlen(path) - 1;
	}
	*pathCarpeta = malloc(strlen(path) + 1);
	memcpy(*pathCarpeta, path, sizePathCarpeta);
	*(*pathCarpeta + sizePathCarpeta) = '\0';
	if (sizePathCarpeta == 0) strcpy(*pathCarpeta, "/");
	int i; for (i=0; i < splittedLast; i++) free(splitted[i]);
	free(splitted);
}

void MuchoPath(const char* path) {
	log_warning(SD.logger, "Se omitió op. por superar path máximo: %s", path);
}

void Boom() {
	log_error(SD.logger, "El servidor se desconectó: Todo se ha roto");
	exit(1);
}

/*
-f: Desactiva la ejecución en modo background
-s: La biblioteca de FUSE se ejecuta en modo single thread
-d: Imprime información de debug
*/
